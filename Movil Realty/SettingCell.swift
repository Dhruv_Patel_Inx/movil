//
//  SettingCell.swift
//  Movil Realty
//
//  Created by BAPS on 4/4/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var `switch`: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        
        /*-------- Switch View Layout --------*/
        
        `switch`.transform = CGAffineTransform(scaleX: 0.80, y: 0.80)
        
        
        
        
        
        // Configure the view for the selected state
    }

}
