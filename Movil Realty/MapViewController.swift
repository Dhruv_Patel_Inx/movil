//
//  MapViewController.swift
//  Movil Realty
//
//  Created by Apple on 17/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import GoogleMaps

let keylat = "Latitude"
let keylong = "Langitude"
let keyAdd = "Address"
let keyStatus = "status"
let keyPrise = "Prise"
let keyType = "Type"
let keyMapType = "map_agent"
let keyname = "name"
let keycontact = "contact_number"
let keyimage = "path"
let keybath = "totalbath"
let keybed = "totalbed"
let keysqft = "size"
let keynewold = "propnewold"
let keyId = "Id"


class MapViewController: UIViewController,GMSMapViewDelegate {
    
    @IBOutlet weak var lblMapSearch: UILabel!
    
    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet var gmap: GMSMapView!
    
    @IBOutlet var viewAnno:UIView!
    @IBOutlet var agentviewAnno: UIView!
    @IBOutlet var labelPrise :UILabel!
    @IBOutlet var labelstatus :UILabel!
    @IBOutlet var labeladdress :UILabel!
    @IBOutlet var labeltype :UILabel!
    @IBOutlet var imageIcon :UIImageView!
    
    @IBOutlet var viewmenu:UIView!
    @IBOutlet var btnProperties:UIButton!
    @IBOutlet var btnagent:UIButton!
    @IBOutlet var btnReset:UIButton!
    @IBOutlet var lblResult:UILabel!
    @IBOutlet var agentname: UILabel!
    @IBOutlet var agentcontact: UILabel!
    
    @IBOutlet weak var barItemMap: UITabBarItem!
    
    
    var arrayAllMarkers = NSMutableArray()
    var p_id = String()
    var a_id = String()
    
    // MARK: - View Life Cycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
     // self.navigationController?.navigationBarHidden = true
//        viewAnno.layer.cornerRadius = 5.0;
//        viewAnno.layer.borderColor = UIColor.orange.cgColor
//        viewAnno.layer.borderWidth = 1.0
//        
//        agentviewAnno.layer.cornerRadius = 5.0
//        agentviewAnno.layer.borderColor = UIColor.orange.cgColor
//        agentviewAnno.layer.borderWidth = 1.0
        
        
        lblResult.layer.cornerRadius = 5.0;
        lblResult.layer.borderColor = DarkOrangeColor.cgColor
        lblResult.layer.borderWidth = 0.50
        lblResult.clipsToBounds = true
        
        btnReset.layer.cornerRadius = 5.0;
        btnReset.layer.borderColor = DarkOrangeColor.cgColor
        btnReset.layer.borderWidth = 0.50
        btnReset.clipsToBounds = true
        
        
        
        viewmenu.layer.cornerRadius = 5.0;
        viewmenu.layer.borderColor = DarkOrangeColor.cgColor
        viewmenu.layer.borderWidth = 0.50
        viewmenu.clipsToBounds = true
        
         self.navigationController?.isNavigationBarHidden = true
        
        gmap.delegate = self
        gmap.isMyLocationEnabled = true
        gmap.settings.myLocationButton = true
        
        
        
        btnagent.accessibilityHint = "Y"
        btnProperties.accessibilityHint = "Y"
        
        prepareForAddMarkers()
        
        
        /*---------- Call SetUpValue ---------*/
        
        self.SetUpValue()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - All Actions

    
    @IBAction func btnOptionsClick(_ sender: UIButton)
    {
        if sender.tag == 1 // property button click
        {
            if sender.accessibilityHint == "N" {
                sender.accessibilityHint = "Y"
                sender.backgroundColor = OrangeThemeColor
                sender.setTitleColor(UIColor.white, for: UIControlState.normal)
            }
            else{
                sender.accessibilityHint = "N"
                sender.backgroundColor = UIColor.white
                sender.setTitleColor(OrangeThemeColor, for: UIControlState.normal)
            }
        }
        else // agent button click
        {
            if sender.accessibilityHint == "N" {
                sender.accessibilityHint = "Y"
                sender.backgroundColor = OrangeThemeColor
                sender.setTitleColor(UIColor.white, for: UIControlState.normal)
            }
            else{
                sender.accessibilityHint = "N"
                sender.backgroundColor = UIColor.white
                sender.setTitleColor(OrangeThemeColor, for: UIControlState.normal)
            }
        }
        prepareForAddMarkers()
    }

    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
    @IBAction func selRightSlideAct(_ sender: Any) {
        
        //        SlideNavigationController.sharedInstance().righttMenuSelected(sender)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "RIghtMenuViewControllerVC")
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func selInfoAct(_ sender: AnyObject)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MoreinfoVC")
        self.present(controller, animated: true, completion: nil)
        
    }
    
    
    

    
    
    
    // MARK: - Custom Methods

    
    func prepareForAddMarkers()  {
        
        arrayAllMarkers.removeAllObjects()
        
        if btnProperties.accessibilityHint == "Y"
        {
            for index in 0..<appdel.arrayProparty.count
            {
                let lat = (appdel.arrayProparty.object(at: index) as! NSDictionary).object(forKey: "LMD_MP_Latitude")!
                
                let long = (appdel.arrayProparty.object(at: index) as! NSDictionary).object(forKey: "LMD_MP_Longitude")!
                
                print(appdel.arrayProparty)
                
                let currentDict = appdel.arrayProparty.object(at: index) as! NSDictionary
                let address = "\(currentDict.object(forKey: "L_Address")!)"
                let status = "\(currentDict.object(forKey: "L_Status")!)"
                let newold = "\(currentDict.object(forKey: "LM_Char1_7"))"
                let prise  = "\(currentDict.object(forKey: "L_AskingPrice")!)"
                let type = "\(currentDict.object(forKey: "L_Type_")!)"
                let prop_image = "\(currentDict.object(forKey: "path")!)"
                
                var totalbath = ""
                var totalbed = ""
                var sqft = ""
                var id = ""
                
                
                if currentDict.object(forKey: "LM_Int1_14") != nil
                {
                    totalbath = "\(currentDict.object(forKey: "LM_Int1_14")!)"
                    print(totalbath)
                    
                }
                else
                {
                    totalbath = ""
                }
                
                
                
                if currentDict.object(forKey: "LM_Int1_1") != nil
                {
                    
                    totalbed = "\(currentDict.object(forKey: "LM_Int1_1")!)"
                    print(totalbed)
                    
                }
                else
                {
                    totalbed = ""
                    
                }
                
                
                if currentDict.object(forKey: "LM_Dec_1") != nil
                {
                    
                    sqft = "\(currentDict.object(forKey: "LM_Dec_1")!)"
                    print(sqft)
                    
                }
                else
                {
                    sqft = ""
                    
                }
                
                
                if currentDict.object(forKey: "L_ListingID") != nil
                {
                    
                    id = "\(currentDict.object(forKey: "L_ListingID")!)"
                    print(id)
                    
                }
                else
                {
                    id = ""
                    
                }

//                
//                let totalbath = currentDict.object(forKey: "LM_Int1_14")
//                print(totalbath as Any)
//                let totalbed = "\(currentDict.object(forKey: "LM_Int1_1"))"
//                print(totalbed)
//
//                let sqft = "\(currentDict.object(forKey: "LM_Dec_1"))"
//                let id = "\(currentDict.object(forKey: "id"))"
                
                 let maptype = "1"
//                labeladdress.text = "\(address)"
//                labelstatus.text = "\(status)"
//                labelPrise.text = "\(prise)"
//                labeltype.text = "\(type)"
                arrayAllMarkers.add([keylat:lat,keylong:long,keyAdd:address,keyStatus:status,keyPrise:prise,keyType:type, keyId:id, keyMapType:maptype,keyimage:prop_image,keybath:totalbath,keybed:totalbed,keysqft:sqft,keynewold:newold])
            }
        }
        
        if btnagent.accessibilityHint == "Y"
        {
            for index in 0..<appdel.arrayAgents.count
            {
                
                print(appdel.arrayAgents)
                let lat = (appdel.arrayAgents.object(at: index) as! NSDictionary).object(forKey: "lat")!
                let long = (appdel.arrayAgents.object(at: index) as! NSDictionary).object(forKey: "long")!
                let currentDict = appdel.arrayAgents.object(at: index) as! NSDictionary
                let address = "\(currentDict.object(forKey: "address")!)"
                let status = ""
                let prise  = ""
                let type = ""
                let agent_name = "\(currentDict.object(forKey: "name")!)"
                let agent_cont = "\(currentDict.object(forKey: "contact_number")!)"
                let id = "\(currentDict.object(forKey: "id")!)"
//                let name = "\(currentDict.object(forKey: "name")!)"
//                let contact = "(\(currentDict.object(forKey: "contact_number")!)"
                let maptype = "2"
//                agentname.text = "\(name)"
//                agentcontact.text = "\(contact)"
                arrayAllMarkers.add([keylat:lat,keylong:long,keyAdd:address,keyStatus:status,keyPrise:prise,keyType:type, keyId:id, keyMapType:maptype, keyname:agent_name,keycontact:agent_cont])
            }
        }
        addMarkerstoMap()
    }
    
    func addMarkerstoMap()  {
        
        gmap.clear()
        var bounds = GMSCoordinateBounds()
        
        
        
        for index in 0..<arrayAllMarkers.count
        {
            let marker = GMSMarker()
            
            let lat = (arrayAllMarkers.object(at: index) as! NSDictionary).object(forKey: keylat)!
            
            let long = (arrayAllMarkers.object(at: index) as! NSDictionary).object(forKey: keylong)!
            
            marker.position = CLLocationCoordinate2D(latitude:  CLLocationDegrees((lat as! NSString).floatValue) ,  longitude: CLLocationDegrees((long as! NSString).floatValue))
            
            bounds  = bounds.includingCoordinate(marker.position)
            marker.map = gmap
            marker.iconView.tag = index
            marker.accessibilityHint = "\(index)"
            let mapType = (arrayAllMarkers.object(at: index) as! NSDictionary).object(forKey: keyMapType)!
            let imageIcon:UIImage!
            if (mapType as AnyObject) .isEqual("1")
            {
                
                let status = (arrayAllMarkers.object(at: index) as! NSDictionary).object(forKey: keyStatus)!
                
                
                
                if (status as AnyObject) .isEqual("1")
                {
                    imageIcon = UIImage(named: "greenProPin")
                }
                else if (status as AnyObject) .isEqual("2")
                {
                    
                    imageIcon = UIImage(named: "orangeProPin")
                }
                else if (status as AnyObject) .isEqual("3")
                {
                    imageIcon = UIImage(named: "yellowProPin")

                }
                else
                {
                    imageIcon = UIImage(named: "redProPin")
                }
                
               
                let viewobj = UIView(frame: CGRect(x: 0, y: 0, width: 53, height: 55))
                
                let label = UILabel(frame: CGRect(x: 1, y: 6, width: 51, height: 25))
                
                let price = "$\((arrayAllMarkers.object(at: index) as! NSDictionary).object(forKey: keyPrise)!)"
                

                
                label.text = (price as AnyObject) as? String
                
                label.textAlignment = NSTextAlignment.center
                
                label.font = UIFont.optimaRegular(Size: 13)
                label.adjustsFontSizeToFitWidth = true
                label.textColor = UIColor.white
                
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 53, height: 55))
                imageView.image = imageIcon
                viewobj.addSubview(imageView)
                viewobj.addSubview(label)
                
                marker.icon = viewobj.getSnapImage()

            }
            else
            {
                
                imageIcon = UIImage(named: "car")

                marker.icon = imageIcon
            }
        
        }
        gmap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 40))
    }
    
    
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
    
        let customtag = Int(marker.accessibilityHint!)
        let mapType = (arrayAllMarkers.object(at: customtag!) as! NSDictionary).object(forKey: keyMapType)!
        if (mapType as AnyObject) .isEqual("1")
        {
            let currentDict = arrayAllMarkers.object(at: customtag!) as! NSDictionary
            
            
            print(currentDict)
            
            let price = "$\(currentDict.object(forKey: keyPrise)!)"
//            let type = "(\(currentDict.object(forKey: keyType)!)"
            let address = "\(currentDict.object(forKey: keyAdd)!)"
//            let status = "\(currentDict.object(forKey: keyStatus)!)"
            let imageurl = "\(currentDict.object(forKey: keyimage)!)"
            let totalbath = "\(currentDict.object(forKey: keybath)!)"
            let totalbed = "\(currentDict.object(forKey:keybed)!)"
            let sqft = "\(currentDict.object(forKey: keysqft)!)"
            let newold = "\(currentDict.object(forKey: keynewold))"
            let detail = "\(totalbed) bd | \(totalbath) ba | \(sqft) sf"
            p_id = "\(currentDict.object(forKey: keyId)!)"
            print(p_id)
            
            
            if newold.isEqual("No") {
                labelstatus.text = "Old"
            }
            else{
                labelstatus.text = "New"
            }
            
            
            
            labeladdress.text = "\(address)"
            labelPrise.text = "\(price)"
            labeltype.text = "\(detail)"
            
            let url = URL(string: imageurl )
            
            let placeHolderImage = "propertyList"
            let placeimage = UIImage(named: placeHolderImage)
            
            imageIcon.sd_setImage(with: url, placeholderImage: placeimage)
            
            marker.tracksInfoWindowChanges = true
            return viewAnno
            
        }
        else
        {
            
            let currentDict = arrayAllMarkers.object(at: customtag!) as! NSDictionary
            let name = "\(currentDict.object(forKey: keyname)!)"
            let contact = "\(currentDict.object(forKey: keycontact)!)"
            agentname.text = "\(name)"
            agentcontact.text = "\(contact)"
            a_id = "\(currentDict.object(forKey: keyId)!)"
            marker.tracksInfoWindowChanges = true
            return agentviewAnno
            
        }
    }
        
    
   
    
    /*------ METHOD TO MAKE MARKER STEADY ON ITS POSITION ON THE TAP ------*/
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        mapView.selectedMarker = marker
        
        return true
    }
    
    
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        
        
        let customtag = Int(marker.accessibilityHint!)
        let mapType = (arrayAllMarkers.object(at: customtag!) as! NSDictionary).object(forKey: keyMapType)!
        if (mapType as AnyObject) .isEqual("1")
        {
            print(p_id)
            
            let residentVC = self.storyboard?.instantiateViewController(withIdentifier: "ResidentActiveVC") as! ResidentActiveVC
            
            residentVC.propertyId = p_id
            
            self.navigationController?.pushViewController(residentVC, animated: true)
        }
        else
        {
           
            
            
            print(a_id)
            
            let count = appdel.arrayAgents.count - 1
            
            for index in 0...count {
                
                let currentDict = appdel.arrayAgents.object(at: index) as! NSDictionary
                print(currentDict)
                
                let id = "\(currentDict.object(forKey: "id")!)"
                
                if id == a_id
                {
                    
                    
                    let dict = (currentDict).mutableCopy() as! NSMutableDictionary
                    print(dict)
                    let agentdetail = self.storyboard?.instantiateViewController(withIdentifier:"AgentDetailsViewControllerVC") as! AgentDetailsViewController
                    agentdetail.agentsDict = dict
                    self.navigationController?.pushViewController(agentdetail, animated: true)

                    
                }
                
                
            }
            
        }
        
    }
    
    
    
    
    
    // MARK: - SetUpvalue Method

    func SetUpValue()
    {
        barItemMap.title = strMap
        lblMapSearch.text = strMapSearch
        lblResult.text = strResults
        
        btnagent.setTitle(strAgents, for: UIControlState.normal)
        btnProperties.setTitle(strProperties, for: UIControlState.normal)
        btnReset.setTitle(strReset, for: UIControlState.normal)
        
        lblResult.text = "\(arrayAllMarkers.count) Results"
        
    }
    

}
extension UIFont{
    
    
    class func optimaBold(Size:CGFloat) -> UIFont {
        return UIFont(name: "Optima-Bold", size: Size)!
    }
    
    class func optimaRegular(Size:CGFloat) -> UIFont {
        return UIFont(name: "Optima-Regular", size: Size)!
    }
    
    class func optimaExtraBlack (Size:CGFloat) -> UIFont {
        return UIFont(name: "Optima-ExtraBlack", size: Size)!
    }
    
    class func optimaItalic(Size:CGFloat) -> UIFont {
        return UIFont(name: "Optima-Italic", size: Size)!
    }
    
    class func optimaBoldItalic(Size:CGFloat) -> UIFont {
        return UIFont(name: "Optima-BoldItalic", size: Size)!
    }
}


extension UIView {
    
    func getSnapImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
}
