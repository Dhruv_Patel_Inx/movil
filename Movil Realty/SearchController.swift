//
//  SearchController.swift
//  Movil Realty
//
//  Created by Apple on 09/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import GoogleMaps

class SearchController: UIViewController,UITextFieldDelegate,PlaceSearchTextFieldDelegate {

  
   
    @IBOutlet var txtSearch1: MVPlaceSearchTextField!
    
    @IBOutlet var ObjMapview: GMSMapView!
    
    @IBOutlet weak var lblMyHome: UILabel!
    
    @IBOutlet weak var btnAddMyHome: UIButton!
    
    var strAddress : NSString!
    
    var locationManager:CLLocationManager!
    
    var userCurrentLocation:CLLocationCoordinate2D!
    
    let marker = GMSMarker()
    
    // MARK: - UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        ObjMapview.isMyLocationEnabled = true
    
        setupView()
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        txtSearch1.autoCompleteRegularFontName =  "Optima-Bold";
        txtSearch1.autoCompleteBoldFontName = "Optima-Regular";
        txtSearch1.autoCompleteTableCornerRadius = 0.0
        txtSearch1.autoCompleteRowHeight = 35
        txtSearch1.autoCompleteTableCellTextColor = UIColor(white: 0.131, alpha: 1.000)
        txtSearch1.autoCompleteFontSize = 14;
        txtSearch1.autoCompleteTableBorderWidth = 1.0
        txtSearch1.showTextFieldDropShadowWhenAutoCompleteTableIsOpen = true
        txtSearch1.autoCompleteShouldHideOnSelection = true
        txtSearch1.autoCompleteShouldHideClosingKeyboard = true
        txtSearch1.autoCompleteShouldSelectOnExactMatchAutomatically = true
        txtSearch1.autoCompleteTableCornerRadius = 1.0
        txtSearch1.autoCompleteTableBorderColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0)
        txtSearch1.autoCompleteTableFrame = CGRect(x: txtSearch1.frame.origin.x, y: txtSearch1.frame.origin.y + txtSearch1.frame.size.height, width: txtSearch1.frame.size.width, height: 50)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - MVPlaceSearchTextField Delegate Methods
    @available(iOS 2.0, *)
    public func placeSearch(_ textField: MVPlaceSearchTextField!, resultCell cell: UITableViewCell!, with placeObject: PlaceObject!, at index: Int) {
        
        
        
        
    }
    
    public func placeSearchWillHideResult(_ textField: MVPlaceSearchTextField!) {
        
        
    }
    
    public func placeSearchWillShowResult(_ textField: MVPlaceSearchTextField!) {
        
    }
    
    public func placeSearch(_ textField: MVPlaceSearchTextField!, responseForSelectedPlace responseDict: GMSPlace!) {
        
        userCurrentLocation = responseDict.coordinate;
        
        strAddress = responseDict.formattedAddress as NSString!;
        
       self.moveMarkerOn(markerPosition: userCurrentLocation)

    }
    
    
    // MARK: - UITextField Delegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        txtSearch1.resignFirstResponder()
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == txtSearch1
        {
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.view.endEditing(true)
            })
        }
        
        txtSearch1.resignFirstResponder()
        return true
    }

   
    // MARK: - UIView Action
    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
    @IBAction func addMyLocationAct(_ sender: Any)
    {
        
        if userCurrentLocation != nil
        {
            
            print(strAddress)
            
            
            //First Convert it to NSNumber.
            let lat : NSNumber = NSNumber(value: userCurrentLocation.latitude)
            let lng : NSNumber = NSNumber(value: userCurrentLocation.longitude)
            
            let dictParams: NSMutableDictionary? = [kMyHomeLat : lat,kMyHomeLong : lng,kMyHomeAddress : strAddress]
            
            self.saveMyHomeApi(latitude: "\(lat)" as NSString, longitude: "\(lng)" as NSString, add: strAddress)
           
            
            
        
            if UserDefaults.standard.object(forKey: kMyLocationDict) != nil
            {
                //Remove object
                let userDefaults = Foundation.UserDefaults.standard
                userDefaults.removeObject(forKey: kMyLocationDict)
                userDefaults.synchronize()
                
                //To save the string
                
                let userDefaults1 = Foundation.UserDefaults.standard
                  userDefaults1.set(dictParams, forKey: kMyLocationDict)
                userDefaults1.synchronize()
                
            }
            else
            {
                //To save the string
                
                let userDefaults1 = Foundation.UserDefaults.standard
                userDefaults1.set(dictParams, forKey: kMyLocationDict)
                userDefaults1.synchronize()

            }

        }
    
    }
    // MARK: - WebServices Call
    func saveMyHomeApi(latitude:NSString,longitude :NSString,add:NSString)
    {
        
        SVProgressHUD.show()
        
        var agentid = NSString()
        var homeid = NSString()
        
        if UserDefaults.standard.object(forKey: kHomeId) != nil
        {
            let userDefaults = Foundation.UserDefaults.standard
            userDefaults.object(forKey: kHomeId)
            print("\(userDefaults.object(forKey: kHomeId))")
            homeid = userDefaults.object(forKey: kHomeId) as! NSString
        }
        
        
        if UserDefaults.standard.object(forKey: kLoginUserDic) != nil
        {
            
            let userDefaults = Foundation.UserDefaults.standard
            userDefaults.object(forKey: kLoginUserDic)
            print("\(userDefaults.object(forKey: kLoginUserDic))")
            let data_dict = userDefaults.object(forKey: kLoginUserDic) as! NSDictionary
            
             agentid = data_dict.object(forKey: "id") as! String as NSString
        }
        
        let global = GlobalMethods()
        let param =  [GlobalMethods.METHOD_NAME: "saveMyHome","agent_id":"\(agentid)","user_id":"\(agentid)","latitude":"\(latitude)","longitude":"\(longitude)","address":"\(add)"] as [String : Any]
        
        global.callWebService(parameter: param as AnyObject!) {
            (Response:AnyObject, error:NSError?) in
            
            
            if error != nil
            {
                SVProgressHUD.showInfo(withStatus: error?.description as String!)
            }
            else
            {
                
                let dictResponse = Response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0
                {
                   // SVProgressHUD.dismiss()
                    let dataDict = dictResponse.object(forKey: "data") as! NSDictionary
                    
                    
                    
                    if UserDefaults.standard.object(forKey: kHomeId) == nil
                    {
                        //To save the string
                        let userDefaults = Foundation.UserDefaults.standard
                        var homeIDS = NSString()
                        
                        homeIDS = String(describing: dataDict.value(forKey: "home_id") as! NSNumber as NSNumber) as NSString
                        
                        print(homeIDS)
                        
                        userDefaults.set(homeIDS, forKey: kHomeId)
                        
                        SVProgressHUD.showSuccess(withStatus: "Add my home Success")
                    }
                    else
                    {
                        SVProgressHUD.showSuccess(withStatus: "Update my home Success")
                    }
                    
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                }
            }
        }
    }
    // MARK: - UIView Functions
    func setupView() {
        
        
        /*----- MVPlacesearchTextField properties -----*/
        txtSearch1.placeSearchDelegate = self
        txtSearch1.strApiKey = "AIzaSyCODzz1jvUh3c8oZGSHjGhOUM8BcPs8YWo"
        txtSearch1.superViewOfList = self.view;  // View, on which Autocompletion list should be appeared.
        txtSearch1.autoCompleteShouldHideOnSelection = true
        txtSearch1.maximumNumberOfAutoCompleteRows = 4
        
        txtSearch1.delegate = self
        
        
        /*--- Set left search icon---*/
        let leftView = UIView(frame: CGRect(x: 0, y: 5, width: 25, height: 20))
        
        var imgSearch1 = UIImageView(frame: CGRect(x: 5, y: 0, width: 20, height: 20))
        //
        let image : UIImage = UIImage(named:"menu-search")!
        imgSearch1 = UIImageView(image: image)
        
        leftView.addSubview(imgSearch1)
        txtSearch1.leftView = leftView;
        
        // txtSearch1.leftViewMode = .always
        
        
        
        /*--- Set right closeButton---*/
        
        let closeButton = UIButton(frame: CGRect(x: 0, y: 5, width: 20, height: 20))
        
        closeButton.backgroundColor = UIColor.black
        
        closeButton.addTarget(self, action: #selector(clearTextAction(_:)), for: .touchUpInside)
        
        let closeImage : UIImage = UIImage(named: "close_icon")!
        
        closeButton.setImage(closeImage, for: .normal)
        
        closeButton.layer.cornerRadius = 10
        closeButton.layer.masksToBounds = true
        
        txtSearch1.rightView = closeButton;
        
        txtSearch1.rightViewMode = .always
        
        
        /*--- Get My Location ---*/
        
        if UserDefaults.standard.object(forKey: kMyLocationDict) != nil
        {
            let dict = UserDefaults.standard.object(forKey: kMyLocationDict) as! NSDictionary
            
            
            let latD = dict.object(forKey: kMyHomeLat) as! CLLocationDegrees
            
            let LongD = dict.object(forKey: kMyHomeLong)  as! CLLocationDegrees
            
            
            userCurrentLocation = CLLocationCoordinate2D(latitude: latD, longitude: LongD)
           
           
    
            strAddress = dict.object(forKey: kMyHomeAddress)as! String? as NSString!
            
            
            
            let camera = GMSCameraPosition.camera(withLatitude: userCurrentLocation.latitude, longitude: userCurrentLocation.longitude, zoom: 10)
            
            ObjMapview.camera = camera
            
            marker.position = CLLocationCoordinate2D(latitude:  CLLocationDegrees(userCurrentLocation.latitude),longitude: CLLocationDegrees(userCurrentLocation.longitude))
            
            txtSearch1.text = strAddress as String?
            
        }
        else
        {
                let camera = GMSCameraPosition.camera(withLatitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!, zoom: 10)
             
             ObjMapview.camera = camera
             
             marker.position = CLLocationCoordinate2D(latitude:  CLLocationDegrees((locationManager.location?.coordinate.latitude)!),longitude: CLLocationDegrees((locationManager.location?.coordinate.longitude)!))
            
           
        }
        
         marker.map = ObjMapview
        
    }
    func clearTextAction(_ sender: UIButton!) {
        
        txtSearch1.text = ""
    }

    func moveMarkerOn(markerPosition: CLLocationCoordinate2D)
    {
        CATransaction.begin()
        
        CATransaction.setAnimationDuration(1.0)
        marker.position = markerPosition;
        CATransaction.commit()
        
       
        marker.title = "My Location"
        marker.appearAnimation = kGMSMarkerAnimationPop
        
        marker.map = self.ObjMapview
        
        self.ObjMapview.animate(toLocation: markerPosition)
      
    }
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        
        txtSearch1.placeholder = strPlaceholderLocationSearch
        btnAddMyHome.setTitle(strAddMyHome, for: UIControlState.normal)
    }

    
}
