//
//  ChangePasswordVC.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 22/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    
    
    
    @IBOutlet weak var lblChangePassword: UILabel!
    @IBOutlet var txtOldPassword: MRTextField!
    @IBOutlet var txtNewPassword: MRTextField!
    @IBOutlet var txtConfirmPassword: MRTextField!
    @IBOutlet var btnSubmit: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         btnSubmit.layer.cornerRadius = 5.0
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()
        

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backAct(_ sender: Any)
    {
        self.view.endEditing(true)
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func submitAct(_ sender: Any)
    {
        self.view.endEditing(true)
        
        if (txtOldPassword.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: "Enter old password.")
        }
        else if (txtNewPassword.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: "Enter new password.")
        }
        else if (txtConfirmPassword.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: "Enter confirm password.")
        }
        else if (txtNewPassword.text != txtConfirmPassword.text){
            SVProgressHUD.showInfo(withStatus: "New password and confirm password mismatch.")
        }
        else
        {
            self.changePasswordApiCall()
        }
        
    }
    
    func changePasswordApiCall()
    {
        self.view.endEditing(true)
        SVProgressHUD.show()
        
        let dict = UserDefaults.standard.object(forKey: kLoginUserDic) as! NSDictionary
        
        
        let userId = dict.object(forKey: "id") as! String?
        
                
        let global = GlobalMethods()
        let param =  [GlobalMethods.METHOD_NAME: "changePassword","agent_id":userId!,"old_password":"\(txtOldPassword.text!)","new_password":"\(txtConfirmPassword.text!)"] as [String : Any]
        
        global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
            
            
            if error != nil
            {
                SVProgressHUD.showInfo(withStatus: error?.description as String!)
            }
            else
            {
                
                let dictResponse = Response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0
                {
                 
                    
                    SVProgressHUD.showSuccess(withStatus: dictResponse.object(forKey: "message") as! String!)
                    
                let _ = self.navigationController?.popViewController(animated: true)
                    
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                }
            }
        }
    }
    
    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        txtNewPassword.placeholder = strPlaeHolderNewPassword
        txtOldPassword.placeholder = strPlaceholderOldPassword
        txtConfirmPassword.placeholder = strPlaceholderConfirmPassword
        lblChangePassword.text = strChangePassword
        
        btnSubmit.setTitle(strSubmit, for: UIControlState.normal)
    }
    

    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
