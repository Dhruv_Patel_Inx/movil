//
//  ReminderCell.swift
//  Movil Realty
//
//  Created by BAPS on 4/3/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class ReminderCell: UITableViewCell {

    @IBOutlet weak var LBLAppointment: UILabel!
    @IBOutlet weak var LBLAgentname: UILabel!
    @IBOutlet weak var LBLStartTime: UILabel!
    @IBOutlet weak var LBLEndTime: UILabel!
    @IBOutlet weak var LBLLocation: UILabel!
    @IBOutlet weak var lblAppStatus: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblEndtime: UILabel!
    @IBOutlet weak var lblStarttime: UILabel!
    @IBOutlet weak var lblAgentname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
