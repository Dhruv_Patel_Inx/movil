//
//  CreateSuggestionVC.swift
//  Movil Realty
//
//  Created by BAPS on 4/11/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class CreateSuggestionVC: UIViewController , UITextFieldDelegate, UITextViewDelegate{

    
    
    @IBOutlet weak var lblCreateTopic: UILabel!
    @IBOutlet weak var txtTitle: UITextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    
    @IBOutlet weak var txtViewDescription: UITextView!
    // MARK: - View Life Cycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /*---------- Text Field Delegate/ Layout ----------*/
        txtTitle.delegate = self
        txtTitle.returnKeyType = .done

//        txtDescription.delegate = self
        
//        txtDescription.layer.borderWidth = 1.0
//        txtDescription.layer.borderColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1.0).cgColor
        
        
        /*---------- Text Field Delegate/ Layout ----------*/

        txtViewDescription.delegate = self
        txtViewDescription.layer.borderWidth = 1.0
        txtViewDescription.textColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1.0)
        txtViewDescription.text = "Description"
        txtViewDescription.returnKeyType = .done
        txtViewDescription.layer.borderWidth = 1.0
        txtViewDescription.layer.borderColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1.0).cgColor

        
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    
    @IBAction func selBackAct(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selSubmitAct(_ sender: Any) {
        
        self.createSuggestionApi()
        
    }
    
    
    
    
    // MARK: - Text Field Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        print("textFieldDidBeginEditing")
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        if txtTitle == textField {
            textField.resignFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return true
        
          
    }
    
    
    
    // MARK: - Text View Delegate
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        txtViewDescription.text = ""
        txtViewDescription.textColor = UIColor.black
        return true
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            view.endEditing(true)
            return false
        }
        else
        {
            return true
        }
    }
    
    
    // MARK: - Web Service Call
    
    func createSuggestionApi()
    {
        
        
        self.view.endEditing(true)
        
        
        if txtViewDescription.text.isEqual("Description") {
            
            txtViewDescription.text = ""
            
        }
        
        
        if (txtTitle.text?.characters.count)! <= 0
        {
            SVProgressHUD.showError(withStatus: "Enter Title")
        }
        else if (txtViewDescription.text?.characters.count)! <= 0
        {
            SVProgressHUD.showError(withStatus: "Enter Description")
        }
        else if UserDefaults.standard.object(forKey: kLoginUserDic) == nil
        {
            SVProgressHUD.showError(withStatus: "Login with Agent Account")
            
        }
        else
        {
            
            
            self.view.endEditing(true)
            SVProgressHUD.show()
            
            let global = GlobalMethods()
            
            
            let userDefaults = Foundation.UserDefaults.standard
            let dict = userDefaults.object(forKey: kLoginUserDic)as! NSDictionary
            print("DICT IS: \(dict)")
            let agent_id = "\(dict.object(forKey: "id")!)"
            print("\(agent_id)")
            
            
            
            let param = [GlobalMethods.METHOD_NAME: "createForum","user_id":"\(agent_id)","title":"\(txtTitle.text!)","description":"\(txtViewDescription.text!)"] as [String : Any]
            
            
            global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
                
                
                if error != nil
                {
                    SVProgressHUD.showInfo(withStatus: error?.description as String!)
                }
                else
                {
                    
                    print("\(Response)")
                    
                    let dictResponse = Response as! NSDictionary
                    let status = dictResponse.object(forKey: "status") as! Int
                    
                    if status != 0
                    {
                        
                        
                        print(Response)
                        
                        
                        

                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            
                            SVProgressHUD.showInfo(withStatus: "Topic Created Successfully")
                            
                            
                        })


                        self.txtTitle.text = ""
                        self.txtViewDescription.text = "Description"
                        self.txtViewDescription.textColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1.0)
                        

                        
                    }
                    else
                    {
                        SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                    }
                }
            }
            
            
        }
        
    }

    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        
        txtTitle.placeholder = strTitle
        lblCreateTopic.text = strCreateTopic
        btnSubmit.setTitle(strSubmit, for: UIControlState.normal)
        
    }

    
    
    
    // MARK: - Custom Methods

    func dismissKeyboard() {
        
        
        if txtTitle.isFirstResponder == true
        {
            txtTitle.resignFirstResponder()

        }
        else
        {
            txtViewDescription.resignFirstResponder()
            
        }
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
