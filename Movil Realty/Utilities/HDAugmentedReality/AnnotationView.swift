/*
 * Copyright (c) 2016 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

protocol AnnotationViewDelegate {
  func didTouch(annotationView: AnnotationView)
}


class AnnotationView: ARAnnotationView {
    var titleLabel: UILabel?
    var distanceLabel: UILabel?
    var detailLabel: UILabel?
    
    var delegate: AnnotationViewDelegate?
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        loadUI()
    }
    
    func loadUI() {
        self.backgroundColor = UIColor.init(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.50)
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1.5
        self.layer.cornerRadius = 3.0
        
        titleLabel?.removeFromSuperview()
        distanceLabel?.removeFromSuperview()
        detailLabel?.removeFromSuperview()
        
        self.titleLabel = UILabel(frame: CGRect(x: 10, y: 10, width: self.frame.size.width, height: 14))
        self.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        self.titleLabel?.numberOfLines = 0
        self.titleLabel?.backgroundColor = UIColor.clear
        self.titleLabel?.textColor = UIColor.white
        self.addSubview(self.titleLabel!)
        
        distanceLabel = UILabel(frame: CGRect(x: 10, y: 25, width: self.frame.size.width, height: 14))
        distanceLabel?.backgroundColor = UIColor.clear
        distanceLabel?.textColor = UIColor.white
        distanceLabel?.font = UIFont.systemFont(ofSize: 12)
        self.addSubview(distanceLabel!)
        
        detailLabel = UILabel(frame: CGRect(x: 10, y: 39, width: self.frame.size.width, height: 14))
        detailLabel?.backgroundColor = UIColor.clear
        detailLabel?.textColor = UIColor.white
        detailLabel?.font = UIFont.systemFont(ofSize: 12)
        self.addSubview(detailLabel!)
        
        if let annotation = annotation as? Place {
            titleLabel?.text = annotation.reference
            distanceLabel?.text = annotation.placeName
            detailLabel?.text = annotation.address
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel?.frame = CGRect(x: 8, y: 10, width: self.frame.size.width-16, height: 14)
        distanceLabel?.frame = CGRect(x: 8, y: 25, width: self.frame.size.width-16, height: 14)
        detailLabel?.frame = CGRect(x: 8, y: 39, width: self.frame.size.width-16, height: 14)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        delegate?.didTouch(annotationView: self)
    }
}
