//
//  MR_StringFile.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 28/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

let kLoginUserDic = "UserLoginDetail"
let kAppointmentDic = "UserCreatedAppointment"
let kHomeId = "UserHomeId"

let kMyLocationDict = "UserHomeLocation"
let kMyHomeAddress = "UserSavedAddress"

let kMyHomeLat = "UserSavedLatitude"
let kMyHomeLong = "UserSavedLongitude"


let kMyFavouriteDic = "MyFavouriteDetail"

let kSearchResult = "SearchedItem"

let kNavigation = "ForNavigationFromSignIn"


/*---- Settings Parameters ----*/
let kMapType = "MapType"
let kSavedSearch = "SavedSearch"
let kSavedHomes = "SavedHomes"
let kAppFeatures = "AppFeaturesUpdate"

/*---- Search Homes Parameters ----*/

let kSearchDict = "SearchHomeDict"
let kBedrooms = "Bedrooms"
let kBathrooms = "Bathrooms"
let kGarage = "Garage"
let kFrontwater = "FrontWater"
let kReducedDay = "ReducedLast_7_Days"
let kOpenHouse = "OpenHousesOnly"


/*---- My Profile/SignIn/SignUp Screen Strings ----*/

let msgUserNameRequire = "Username is required."
let msgPasswordRequire = "Password is required."
let msgPassworConfirm = "Enter confirm password"
let msgPasswordMatchConfirmPassword = "Password and confirm password must match"
let msgCompanyNameRequire = "Company Name is required."
let msgEmaileRequire = "Email is required."
let msgInvalidEmail = "The email you entered is not valid email. Please retype valid email."
let msgLiceanceNumberRequire = "Licence Number is required."
let msgPhoneNumberRequire = "Phone Number is required."
let msgInvalidPhone = "The phone number you entered is not valid. Please retype valid phone"


/*---- Appointment Screen Strings ----*/

let msgDateRequire = "Date is required."
let msgStartTimeRequire = "Start time is required."
let msgEndTimeRequire = "End time is required."
let msgContactNumerRequire = "Contact number  is required."
let msgLocationRequire = "Location is required."


/*---- Appointment/Property Screen Strings ----*/

let strProperties = "Properties"
let strReset = "Reset"


let strMap = "Map"
let strMapSearch = "Map Search"
let strResults = "Results"
let strAgents = "Agents"

/*---- Agent View Screen Strings ----*/

let strOurAgents = "Our Agents"
let strBecomeMovilAgent = "Become a Movil Agent"


/*---- Chat View Screen Strings ----*/

let strChat = "Chat"

/*---- My Profile/Change Password  View Screen Strings ----*/


let strPlaceholderUsername = "Username"
let strPlaceholderPassword = "Password"
let strPlaceholderConfirmPassword = "Confirm Password"
let strPlaceholderEmail = "Email ID"
let strPlaceholderPhone = "Phone number"
let strPlaceholderCompany = "Company name"
let strPlaceholderLicense = "License number"
let strPlaeHolderNewPassword = "New Password"
let strPlaceholderOldPassword = "Old Password"

let strSubmit = "Submit"
let strEdit = "Edit"
let strSignOut = "Sign Out"
let strChangePassword = "Change Password"




/*----  View Screen Strings ----*/

let strLoading = "Loading..."




/*----  Agent Detail Screen Strings ----*/

let strWeb = "Web"
let strCompany = "Company"
let strAddress = "Address"





/*----  Sign Up Screen Strings ----*/


let strRemember = "Remember Me"
let strDontHaveAccount = "Don't have an account yet?"
let strSignIn = "Sign In"
let strSignup = "Sign up"





/*----  More Info Screen Strings ----*/

let strActive = "Active"
let strContigent = "Contigent"
let strPending = "Pending"
let strClose = "Close"
let strMapLegend = "Map Legend"





/*----  Share My App Screen Strings ----*/

let strFacebook = "Facebook"
let strGoogle = "Google"
let strEmail = "Email"
let strTwitter = "Twitter"
let strSMS = "SMS"
let strShareMyApp = "Share My App"




/*----  Search Controller Screen Strings ----*/

let strPlaceholderLocationSearch = "Location Search"
let strAddMyHome = "Add as My Home"





/*----  Search Controller Agent Screen Strings ----*/

let strMyHome = "My Home"





/*----  Schedual Cell Screen Strings ----*/


let strAppointment = "Appointment"
let strAgentName = "Agent Name"
let strStartTime = "Start Time"
let strEndTime = "End Time"
let strLocation = "Location"





/*----  Scheduale View Screen Strings ----*/

let strSchedual = "Scheduale"




/*----  Appointment View Screen Strings ----*/

let strDate = "Date"
let strSchedualAppointment = "Schedual an Appointment"
let strContactNumber = "Contact Number"




/*----  Search Near View Screen Strings ----*/


let strSearch = "Search"






/*----  Favourite Screen Strings ----*/

let strFavourite = "Favorite"





/*----  Mortage Screen Strings ----*/



let strPrincipalInterest = "Principal & Interest"
let strHomeIns = "Home Ins."
let strHOADues = "HOA  Dues"
let strPropertyTaxes = "Property Taxes"
let strmo = "/mo"
let strMoreOption = "More Option"





/*----  Resident Active Screen Strings ----*/

let strbath = "bath"
let strbeds = "beds"
let strdayson = "days on"
let strfinsqft = "fin. sqft"
let strResidentialActive = "Residential Active"
let strNEIGHBORHOOD = "NEIGHBORHOOD"
let strAGENTREMARKS = "AGENT REMARKS"
let strChatAboutProperty = "Chat About This Property"
let strDETAILS = "DETAILS"
let strPUBLICREMARKS = "PUBLIC REMARKS"





/*----  Resident Detail Screen Strings ----*/


let strSEEFULLDETAILS = "SEE FULL PROPERTY DETAILS"
let strHIDEFULLDETAILS = "HIDE PROPERTY DETAILS"






/*----  Agent  Forum Screen Strings ----*/

let strAgentForum = "AgentForum"
let strFeedback = "Feedback"
let strpeople = "people"
let strthis = "this"
let strIWantthis = "I Want this!"
let strAddComment = "Add a Comment"






/*----  Agent  Forum  Detail Screen Strings ----*/

let strAgentForum_1 = "Agent Forum"
let strPostanIdea = "Post an Idea"
let strMySuggestionwould = "My Suggestion would be"






/*----  Create Suggestion Screen Strings ----*/

let strTitle = "Title"
let strCreateTopic = "Create a Topic"
//let strFeedback = "Feedback"




/*----  Define Max and Min Value ----*/


let PropertyMax = 300000
let PropertyMin = 0

let DownPaymentMax = 100
let DownPaymentMin = 0

let InterestRateMax = 30
let InterestRateMin = 0

let LoanTermMax = 100
let LoanTermMin = 0

let ProTaxMax = 5000
let ProTaxMin = 0

let HomeInsMax = 500
let HomeInsMin = 0

let HOAMax = 50
let HOAMin = 0




/*----  Define Color  ----*/
let DarkOrangeColor =  UIColor(red:255.0/255.0, green:80/255.0 ,blue:0.0/255.0 , alpha:1.00)

let OrangeThemeColor = UIColor(red:255.0/255.0, green:102/255.0 ,blue:26.0/255.0 , alpha:1.00)
