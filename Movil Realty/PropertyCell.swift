//
//  PropertyCell.swift
//  Movil Realty
//
//  Created by Apple on 24/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class PropertyCell: UITableViewCell {
    @IBOutlet var addresslabel:UILabel!
    @IBOutlet var typelabel:UILabel!
    @IBOutlet var pricelabel:UILabel!
    @IBOutlet var houseimageview:UIImageView!

    @IBOutlet var lblStatus: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        pricelabel.sizeToFit()
        // Configure the view for the selected state
    }

}
