//
//  MoreinfoViewController.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 23/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class MoreinfoViewController: UIViewController {

    @IBOutlet weak var lblMapLegend: UILabel!
    
    @IBOutlet weak var lblActive: UILabel!
    
    @IBOutlet weak var lblContigent: UILabel!
    
    @IBOutlet weak var lblPending: UILabel!
    
    @IBOutlet weak var lblClosed: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeSel(_ sender: AnyObject)
    {
        self.dismiss(animated: true, completion: nil)
    }

    
    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        lblActive.text = strActive
        lblContigent.text = strContigent
        lblPending.text = strPending
        lblClosed.text = strClose
        lblMapLegend.text = strMapLegend
        
    }
    

    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
