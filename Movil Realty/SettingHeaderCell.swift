//
//  SettingHeaderCell.swift
//  Movil Realty
//
//  Created by BAPS on 4/5/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class SettingHeaderCell: UITableViewCell {

    @IBOutlet weak var lblHeader: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
