//
//  SuggestionCell.swift
//  Movil Realty
//
//  Created by BAPS on 4/11/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class SuggestionCell: UITableViewCell {

    @IBOutlet weak var lblSuggestion: UILabel!
    
    @IBOutlet weak var lblLikeNumber: UILabel!
    
    @IBOutlet weak var btnDetail: UIButton!
    
    @IBOutlet weak var imgviewLike: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
