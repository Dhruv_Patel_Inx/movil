//
//  myProfileVC.swift
//  Movil Realty
//
//  Created by Apple on 27/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import Alamofire

class myProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var viewProfPic: UIView!
    @IBOutlet var imgProfilePic: UIImageView!
    @IBOutlet var btnSignOut: UIButton!
    @IBOutlet var btnimage: UIButton!
    
    
    @IBOutlet var txtUsername: MRTextField!
    @IBOutlet var txtcompany: MRTextField!
    @IBOutlet var txtEmail: MRTextField!
    @IBOutlet var txtLicence: MRTextField!
    @IBOutlet var txtPassword: MRTextField!
    @IBOutlet var txtphone: MRTextField!

    @IBOutlet var btnPassword: UIButton!
//    var userDefaults = Foundation.UserDefaults.standard
    var currentImage:UIImage!
    var userDetailOld = NSMutableDictionary()
    var newDict = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSignOut.layer.cornerRadius = 5.0
        
        viewProfPic.layer.borderWidth = 0.5
        viewProfPic.layer.borderColor = UIColor.lightGray.cgColor
        
        imgProfilePic.layer.borderWidth = 0.5
        imgProfilePic.layer.borderColor = UIColor.lightGray.cgColor
        
        btnimage.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
        
        
        
        
        if UserDefaults.standard.object(forKey: kLoginUserDic) != nil
        {
            let userDefaults = Foundation.UserDefaults.standard
            
            let dict = userDefaults.object(forKey: kLoginUserDic) as! NSDictionary
            let temp_isAgent = dict.object(forKey: "is_agent")!
            
            let isAgent = "\(temp_isAgent)"
            
            if isAgent.isEqual("0")

            {
                
                self.imgProfilePic.isHidden = true
                self.viewProfPic.isHidden = true

                self.txtcompany.isHidden = true
                self.txtLicence.isHidden = true
                self.txtphone.isHidden = true

                DispatchQueue.main.async(execute: {() -> Void in

                
                self.txtEmail.frame = CGRect(x: self.txtcompany.frame.origin.x, y: self.txtUsername.frame.origin.y + self.txtUsername.frame.size.height + 22, width: self.txtEmail.frame.size.width, height: self.txtEmail.frame.size.height)
                
                self.txtPassword.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtEmail.frame.origin.y + self.txtEmail.frame.size.height + 22, width: self.txtPassword.frame.size.width, height: self.txtPassword.frame.size.height)
                
                
                self.btnSignOut.frame = CGRect(x: self.btnSignOut.frame.origin.x  , y: self.txtPassword.frame.origin.y + self.txtPassword.frame.size.height + 22, width: self.btnSignOut.frame.size.width, height: self.btnSignOut.frame.size.height)
                })

            }
            else
            {
                
                self.viewProfPic.isHidden = false
                self.imgProfilePic.isHidden = false
                self.txtcompany.isHidden = false
                self.txtLicence.isHidden = false
                self.txtphone.isHidden = false
                
                
                
                DispatchQueue.main.async(execute: {() -> Void in

                
                
                self.txtcompany.frame = CGRect(x: self.txtcompany.frame.origin.x, y: self.txtUsername.frame.origin.y + self.txtUsername.frame.size.height + 22, width: self.txtcompany.frame.size.width, height: self.txtUsername.frame.size.height)
                
                self.txtEmail.frame = CGRect(x: self.txtcompany.frame.origin.x, y: self.txtcompany.frame.origin.y + self.txtcompany.frame.size.height + 22, width: self.txtcompany.frame.size.width, height: self.txtEmail.frame.size.height)
                
                self.txtLicence.frame = CGRect(x: self.txtcompany.frame.origin.x, y: self.txtEmail.frame.origin.y + self.txtEmail.frame.size.height + 22, width: self.txtcompany.frame.size.width, height: self.txtEmail.frame.size.height)
                
                self.txtPassword.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtLicence.frame.origin.y + self.txtEmail.frame.size.height + 22, width: self.txtPassword.frame.size.width, height: self.txtPassword.frame.size.height)
                
                self.txtphone.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtPassword.frame.origin.y + self.txtPassword.frame.size.height + 22, width: self.txtphone.frame.size.width, height: self.txtphone.frame.size.height)
                
                self.btnSignOut.frame = CGRect(x: self.btnSignOut.frame.origin.x  , y: self.txtPassword.frame.origin.y + self.txtPassword.frame.size.height + 22, width: self.btnSignOut.frame.size.width, height: self.btnSignOut.frame.size.height)
                
                })
                
            }

            
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        self.setUpProfile()
        
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()

        
    }

    override func viewDidAppear(_ animated: Bool) {
        
        viewProfPic.layer.cornerRadius = viewProfPic.frame.size.height / 2
        imgProfilePic.layer.cornerRadius = imgProfilePic.frame.size.height / 2
        btnimage.layer.cornerRadius = btnimage.frame.size.height / 2
        
        viewProfPic.clipsToBounds = true
        imgProfilePic.clipsToBounds = true
        
    }
    
    @IBAction func btnimagesel(_ sender: Any)
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        
        let alert = UIAlertController(title: "Options", message: "Select any option to add image", preferredStyle: .actionSheet)
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            alert.addAction(UIAlertAction(title: "Camera roll", style: .default, handler: { (_ : UIAlertAction) in
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Gallary", style: .default, handler: { (_ : UIAlertAction) in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_ : UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)

    }
    @IBAction func btnSignOutClick(_ sender: UIButton)
    {
        // Create the alert controller
        let alertController = UIAlertController(title: "Movil", message: "Are you sure you want to sign out?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.signOutApiCall()
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
            print("NOOOO")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    func signOutApiCall()
    {

        
        self.view.endEditing(true)
        SVProgressHUD.show()
        
        let global = GlobalMethods()
        let param =  [GlobalMethods.METHOD_NAME: "getLogout","agent_id":"\(newDict.object(forKey: "id")!)"] as [String : Any]
        
        global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
            
            
            if error != nil
            {
                SVProgressHUD.showInfo(withStatus: error?.description as String!)
            }
            else
            {
                
                let dictResponse = Response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0
                {
                    SVProgressHUD.dismiss()
                    
                    
                    // To remove the Login Details
                    
                    let defaults = UserDefaults.standard
                    defaults.removeObject(forKey: kLoginUserDic)
                    
                    
                    let Obj_signInViewC = self.storyboard?.instantiateViewController(withIdentifier: "signInViewC")
                    SlideNavigationController.sharedInstance().popToRootAndSwitch(to: Obj_signInViewC, withSlideOutAnimation: false, andCompletion: nil)
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfileDetail"), object: nil)
                    
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                }
            }
        }
    }
    @IBAction func btnEditclick(_ sender: UIButton)
    {
        if  sender.currentTitle == "Edit"
        {
            sender.setTitle("Save", for: .normal)
           
            txtUsername.isUserInteractionEnabled = true
            txtcompany.isUserInteractionEnabled = true
            txtEmail.isUserInteractionEnabled = true
            txtLicence.isUserInteractionEnabled = true
           // txtPassword.isUserInteractionEnabled = true
            txtphone.isUserInteractionEnabled = true
            btnimage.isUserInteractionEnabled = true
            btnPassword.isUserInteractionEnabled = true
            txtUsername.becomeFirstResponder()
        }
        else
        {
            self.checkUserUpdateDetails()
        }
        
        
    }
    
    func disableAllField()
    {
        self.view.endEditing(true)
        btnEdit.setTitle("Edit", for: .normal)
      
        txtUsername.isUserInteractionEnabled = false
        txtcompany.isUserInteractionEnabled = false
        txtEmail.isUserInteractionEnabled = false
        txtLicence.isUserInteractionEnabled = false
        // txtPassword.isUserInteractionEnabled = false
        btnPassword.isUserInteractionEnabled = false
        txtphone.isUserInteractionEnabled = false
        btnimage.isUserInteractionEnabled = false
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            currentImage = image
            imgProfilePic.image = currentImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    func checkUserUpdateDetails()
    {
        if (txtUsername.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: msgUserNameRequire)
        }
        else if (txtcompany.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: msgCompanyNameRequire)
        }
        else if (txtEmail.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: msgEmaileRequire)
        }
        else if (txtLicence.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: msgLiceanceNumberRequire)
        }
        else if (txtphone.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: msgPhoneNumberRequire)
        }
        else
        {
            let mutableDict = newDict.mutableCopy() as! NSMutableDictionary
            
            mutableDict .setObject(txtUsername.text!, forKey: "user_name" as NSCopying)
            mutableDict .setObject(txtEmail.text!, forKey: "email" as NSCopying)
            mutableDict .setObject(txtphone.text!, forKey: "contact_number" as NSCopying)
            mutableDict .setObject(txtLicence.text!, forKey: "lic_number" as NSCopying)
            mutableDict .setObject(txtcompany.text!, forKey: "company" as NSCopying)
            mutableDict .setObject(imgProfilePic.image!, forKey: "pic" as NSCopying)
            
            newDict = mutableDict.mutableCopy() as! NSMutableDictionary
            
            
            if(newDict != userDetailOld)
            {
                self.editProfileApiCall()
            }
            else
            {
                self.disableAllField()
            }
        }
    }
    
    func editProfileApiCall()
    {
        
        self.view.endEditing(true)
        SVProgressHUD.show()
        
        let userDefaults = Foundation.UserDefaults.standard
        
        let dict = userDefaults.object(forKey: kLoginUserDic) as! NSDictionary
        let temp_isAgent = dict.object(forKey: "is_agent")!
        
        let isAgent = "\(temp_isAgent)"
        
        if isAgent.isEqual("0")

        {
            
            //        let param =  [GlobalMethods.METHOD_NAME: "saveForumComment","user_id": SuggestionDetail_dict.object(forKey: "user_id")!,"forum_id":SuggestionDetail_dict.object(forKey: "user_id")!,"comment":"\(txtComment.text!)","comment_date":"\(date_string)"] as [String : Any]
            
            
            let global = GlobalMethods()
            
            let param =  [GlobalMethods.METHOD_NAME: "editProfile","id":"\(newDict.object(forKey: "id")!)","name":"\(newDict.object(forKey: "user_name")!)","email":"\(newDict.object(forKey: "email")!)","is_agent":"0"] as [String : Any]
            
            
            global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
                
                
                if error != nil
                {
                    SVProgressHUD.showInfo(withStatus: error?.description as String!)
                }
                else
                {
                    
                    let dictResponse = Response as! NSDictionary
                    let status = dictResponse.object(forKey: "status") as! Int
                    
                    if status != 0
                    {
                        
                        let dataDict = (dictResponse as AnyObject).object(forKey: "data") as! NSDictionary
                        
                        print(dataDict)
                        //To save the string
                        let userDefaults = Foundation.UserDefaults.standard
                        userDefaults.set( dataDict, forKey: kLoginUserDic)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfileDetail"), object: nil)
                        let view1 = self.storyboard?.instantiateViewController(withIdentifier: "tabBarControllerVC") as! tabBarController
                        self.navigationController?.pushViewController(view1, animated: false)
                    }
                    else
                    {
                        SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                    }
                }
            }
            
            
            
            
        }
        
        
        else
        {

            let param =  [GlobalMethods.METHOD_NAME: "editProfile","id":"\(newDict.object(forKey: "id")!)","name":"\(newDict.object(forKey: "user_name")!)","company_name":"\(newDict.object(forKey: "company")!)","email":"\(newDict.object(forKey: "email")!)","lic_number":"\(newDict.object(forKey: "lic_number")!)","phone":"\(newDict.object(forKey: "contact_number")!)","is_agent":"1"] as [String : Any]
            
            
            
            Alamofire.upload(multipartFormData: { (MultipartFormData) in
                if self.currentImage != nil
                {
                    
                    MultipartFormData.append(UIImageJPEGRepresentation(self.currentImage, 1.0)!, withName: "image", fileName: "file.png", mimeType: "image/png")
                }
                
                for (key, value) in param
                {
                    MultipartFormData.append(((value as! String).data(using: .utf8))!, withName: key)
                }
            }, to: GlobalMethods.WEB_SERVICE_URL, encodingCompletion: { (result:SessionManager.MultipartFormDataEncodingResult) in
                
                
                self.disableAllField()
                
                switch result
                {
                case .success(let upload,_ , _):
                    upload.response
                        {
                            
                            [weak self] response in
                            guard self != nil else
                            {
                                return
                            }
                            
                            do{
                                let dict = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                                let status = (dict as AnyObject).object(forKey: "status") as! Int
                                
                                if status != 0{
                                    
                                    SVProgressHUD.dismiss()
                                    // To remove the Login Details
                                    
                                    let defaults = UserDefaults.standard
                                    defaults.removeObject(forKey: kLoginUserDic)
                                    
                                    
                                    let dataDict = (dict as AnyObject).object(forKey: "data") as! NSDictionary
                                    
                                    print("\(dataDict)")
                                    //To save the string
                                    let userDefaults = Foundation.UserDefaults.standard
                                    
                                    userDefaults.set( dataDict, forKey: kLoginUserDic)
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfileDetail"), object: nil)
                                    
                                    self?.userDetailOld = self!.newDict.mutableCopy() as! NSMutableDictionary
                                    
                                }
                                else
                                {
                                    SVProgressHUD.showInfo(withStatus: (dict as AnyObject).object(forKey: "message") as! String!)
                                }
                                
                            }
                            catch {
                                
                                SVProgressHUD.dismiss()
                                
                                let errorObj = error as NSError
                                print("ERROR Code : ",errorObj.code)
                                print("ERROR Code : ",errorObj.localizedDescription)
                            }
                            
                            
                    }
                case .failure(let encodingError):
                    SVProgressHUD.dismiss()
                    print("error:\(encodingError)")
                }
                
                SVProgressHUD.dismiss()
                print(result)
            })

        
        }
        
//        let param =  [GlobalMethods.METHOD_NAME: "editProfile","id":"\(newDict.object(forKey: "id")!)","name":"\(newDict.object(forKey: "user_name")!)","company_name":"\(newDict.object(forKey: "company")!)","email":"\(newDict.object(forKey: "email")!)","lic_number":"\(newDict.object(forKey: "lic_number")!)","phone":"\(newDict.object(forKey: "contact_number")!)"] as [String : Any]
//        
//        Alamofire.upload(multipartFormData: { (MultipartFormData) in
//            if self.currentImage != nil
//            {
//                               
//                MultipartFormData.append(UIImageJPEGRepresentation(self.currentImage, 1.0)!, withName: "image", fileName: "file.png", mimeType: "image/png")
//            }
//            
//            for (key, value) in param
//            {
//                MultipartFormData.append(((value as! String).data(using: .utf8))!, withName: key)
//            }
//        }, to: GlobalMethods.WEB_SERVICE_URL, encodingCompletion: { (result:SessionManager.MultipartFormDataEncodingResult) in
//            
//            
//            self.disableAllField()
//            
//            switch result
//            {
//            case .success(let upload,_ , _):
//                upload.response
//                    {
//                        
//                        [weak self] response in
//                        guard self != nil else
//                        {
//                            return
//                        }
//                        
//                        do{
//                            let dict = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers)
//                            let status = (dict as AnyObject).object(forKey: "status") as! Int
//                            
//                            if status != 0{
//                                
//                                SVProgressHUD.dismiss()
//                                // To remove the Login Details
//                                
//                                let defaults = UserDefaults.standard
//                                defaults.removeObject(forKey: kLoginUserDic)
//                                
//                                
//                                let dataDict = (dict as AnyObject).object(forKey: "data") as! NSDictionary
//                                
//                                print("\(dataDict)")
//                                //To save the string
//                                let userDefaults = Foundation.UserDefaults.standard
//                                
//                                userDefaults.set( dataDict, forKey: kLoginUserDic)
//                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfileDetail"), object: nil)
//                                
//                                self?.userDetailOld = self!.newDict.mutableCopy() as! NSMutableDictionary
//                                
//                            }
//                            else
//                            {
//                                SVProgressHUD.showInfo(withStatus: (dict as AnyObject).object(forKey: "message") as! String!)
//                            }
//                            
//                        }
//                        catch {
//                            
//                            SVProgressHUD.dismiss()
//                            
//                            let errorObj = error as NSError
//                            print("ERROR Code : ",errorObj.code)
//                            print("ERROR Code : ",errorObj.localizedDescription)
//                        }
//                        
//                        
//                }
//            case .failure(let encodingError):
//                SVProgressHUD.dismiss()
//                print("error:\(encodingError)")
//            }
//            
//            SVProgressHUD.dismiss()
//            print(result)
//        })
        
//        let userDefaults = Foundation.UserDefaults.standard
//        userDetailOld = newDict.mutableCopy() as! NSMutableDictionary
//        userDetailOld = userDefaults.object(forKey: kLoginUserDic) as! NSMutableDictionary
//        
//        print("\(userDetailOld)")
    }
    
    func setUpProfile()
    {
        //To retrieve from the key
        let userDefaults = Foundation.UserDefaults.standard
        let dict = userDefaults.object(forKey: kLoginUserDic) as! NSDictionary
        let temp_isAgent = dict.object(forKey: "is_agent")!
        
        let isAgent = "\(temp_isAgent)"
        
        if isAgent.isEqual("0")

        {
            userDetailOld = userDefaults.object(forKey: kLoginUserDic) as! NSMutableDictionary
            
            
            
            
            newDict = userDefaults.object(forKey: kLoginUserDic) as! NSMutableDictionary
            
            if let value = userDetailOld["email"] as? String {
                txtEmail.text = value;
            }
            
           
            
            if let value = userDetailOld["user_name"] as? String
            {
                txtUsername.text = value;
            }
            
            if let value = userDetailOld["pic"] as? String
            {
                let url = URL(string: value )
                
                let placeHolderImage = "profile_icon"
                let image = UIImage(named: placeHolderImage)
                
                imgProfilePic.sd_setImage(with: url, placeholderImage: image)
                
            }
            

        }
        else
        {
            
            
            
            userDetailOld = userDefaults.object(forKey: kLoginUserDic) as! NSMutableDictionary
            
            
            newDict = userDefaults.object(forKey: kLoginUserDic) as! NSMutableDictionary
            
            if let value = userDetailOld["email"] as? String {
                txtEmail.text = value;
            }
            
            if let value = userDetailOld["contact_number"] as? String
            {
                txtphone.text = value;
            }
            
            if let value = userDetailOld["user_name"] as? String
            {
                txtUsername.text = value;
            }
            
            
            if let value = userDetailOld["lic_number"] as? String
            {
                txtLicence.text = value;
            }
            
            if let value = userDetailOld["company"] as? String
            {
                txtcompany.text = value;
            }
            
            if let value = userDetailOld["pic"] as? String
            {
                let url = URL(string: value )
                
                let placeHolderImage = "profile_icon"
                let image = UIImage(named: placeHolderImage)
                
                imgProfilePic.sd_setImage(with: url, placeholderImage: image)
                
            }
            

        }
        
        
        
            }
    
    
    
    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
    @IBAction func movePasswordViewAct(_ sender: Any)
    {
        self.view.endEditing(true)
        let view1 = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(view1, animated: true)
    }
    //MARK: - UITextField Method -
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if txtUsername == textField {
            textField.resignFirstResponder()
            txtcompany.becomeFirstResponder()
        }
        else if txtcompany == textField {
            textField.resignFirstResponder()
            txtEmail.becomeFirstResponder()
        }
        else if txtEmail == textField {
            textField.resignFirstResponder()
            txtLicence.becomeFirstResponder()
        }
        else if txtLicence == textField {
            textField.resignFirstResponder()
            txtphone.becomeFirstResponder()
        }
//        else if txtPassword == textField {
//            textField.resignFirstResponder()
//            txtphone.becomeFirstResponder()
//        }
        else{
            textField.resignFirstResponder()
        }
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        txtUsername.placeholder = strPlaceholderUsername
        txtPassword.placeholder = strPlaceholderPassword
        txtEmail.placeholder = strPlaceholderEmail
        txtphone.placeholder = strPlaceholderPhone
        txtcompany.placeholder = strPlaceholderCompany
        txtLicence.placeholder = strPlaceholderLicense
        
        
        btnEdit.setTitle(strEdit, for: UIControlState.normal)
        btnSignOut.setTitle(strSignOut, for: UIControlState.normal)
    }
    

    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
