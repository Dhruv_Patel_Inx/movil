//
//  AgentsCell.swift
//  Movil Realty
//
//  Created by Apple on 27/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class AgentsCell: UICollectionViewCell
{
    @IBOutlet var callbutton: UIButton!
    @IBOutlet var emailbuton: UIButton!
    @IBOutlet var chatbutton: UIButton!
    @IBOutlet var agentimage: UIImageView!
       @IBOutlet var agentname: UILabel!
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.makeCorner()
    }
    
    func makeCorner() {
        self.agentimage.layer.masksToBounds = true
        self.agentimage.layer.cornerRadius  = 10
        self.agentimage.layer.borderWidth = 1.0
        self.agentimage.layer.borderColor = OrangeThemeColor.cgColor
    }
 }
