//
//  SchedualCell.swift
//  Movil Realty
//
//  Created by Apple on 23/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class SchedualCell: UITableViewCell {

    @IBOutlet var LBLAppointment: UILabel!
    @IBOutlet var LBLAgentname: UILabel!
    @IBOutlet var LBLStarttime: UILabel!
    @IBOutlet var LBLEndtime: UILabel!
    @IBOutlet var LBLLocation: UILabel!
    @IBOutlet var lblAgentname: UILabel!
    @IBOutlet var lblStartime: UILabel!
    @IBOutlet var lblEndtime: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblAppointment: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        LBLAppointment.text = strAppointment
        LBLAgentname.text = strAgentName
        LBLStarttime.text = strStartTime
        LBLEndtime.text = strEndTime
        LBLLocation.text = "Location"

        
        // Configure the view for the selected state
    }

}
