//
//  AgentUnassignedVC.swift
//  Movil Realty
//
//  Created by Apple on 18/05/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class AgentUnassignedVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBAction func BacktoViewController(_ sender: AnyObject) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet var labellocationvalueObj: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblContact: UILabel!
    @IBOutlet var statusButtonObj: UIButton!
    
    @IBOutlet var segmentObj: UISegmentedControl!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var labelstatustextObj: UILabel!
    var statusPickerView : UIPickerView = UIPickerView()
    var statustoolbar : UIToolbar = UIToolbar()
    var strstatus : String = ""
    
    var pickerData = ["Assigned","Unassigned"]
    
    var homeDict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {

    }

    func setUpView()
    {
        print(homeDict)
        
        lblName.text = homeDict.object(forKey: "fu_name") as! String?
        labellocationvalueObj.text = homeDict.object(forKey: "mh_address") as! String?
        
        lblContact.text = homeDict.object(forKey: "mh_contact") as! String?
        
        let Strstatus = homeDict.object(forKey: "mh_is_assign") as! String?
    
        if(Strstatus == "1")
        {
            segmentObj.selectedSegmentIndex = 0
            labelstatustextObj.text = "Assigned"
        }
        else
        {
            segmentObj.selectedSegmentIndex = 1
            labelstatustextObj.text = "Unassigned"
        }
        
        segmentObj.isUserInteractionEnabled = false

        // UIDesign
        statusButtonObj.layer.cornerRadius = 5
        statusButtonObj.layer.borderWidth = 1
        statusButtonObj.layer.borderColor = UIColor.gray.cgColor
        
        btnSubmit.layer.cornerRadius = 5;
        btnSubmit.layer.masksToBounds = true
        
        
        // Add PickerView
        
        statusPickerView = UIPickerView(frame: CGRect(x:0,y: self.view.frame.size.height - 200 ,width: self.view.frame.size.width ,height: 200))
        statusPickerView.delegate = self;
        statusPickerView.dataSource = self;
        
        statusPickerView.backgroundColor = UIColor.white
        
        self.view.addSubview(statusPickerView)
        
        // Add Toolbar
        
        statustoolbar = UIToolbar(frame: CGRect(x:0,y: self.view.frame.size.height - 250 ,width: self.view.frame.size.width ,height: 50))
        
        statustoolbar.barStyle = .blackOpaque
        let donebutton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.selectDone))
        
        let cancelbutton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.selectCancel))
        
        let barButtonFreeSpaace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        statustoolbar.items = [donebutton, barButtonFreeSpaace, cancelbutton]
        statustoolbar.barTintColor = UIColor(red: 255/255, green: 102/255, blue: 26/255, alpha: 1.0)
        donebutton.tintColor = UIColor.white
        cancelbutton.tintColor = UIColor.white
        donebutton.setTitleTextAttributes([NSFontAttributeName:UIFont.optimaBold(Size: 18.0),NSForegroundColorAttributeName : UIColor.white], for: .normal)
        cancelbutton.setTitleTextAttributes([NSFontAttributeName:UIFont.optimaBold(Size: 18.0),NSForegroundColorAttributeName : UIColor.white], for: .normal)
        
        self.view.addSubview(statustoolbar)
        
        statustoolbar.isHidden = true
        statusPickerView.isHidden = true
        
        
        // Set Segement Font
        let font = UIFont.optimaRegular(Size: 15)
    
        
        segmentObj.setTitleTextAttributes([NSFontAttributeName:font],for: UIControlState.normal)
    }
    
    @IBAction func btnStatusObj(_ sender: AnyObject) {
    
        statusPickerView.isHidden = false
        statustoolbar.isHidden = false
        
    }

    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return pickerData.count
    }
    /*---------- To Set the text of Picker ---------*/
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        
        
        
        
        let pickerLabel = UILabel()
        
        
            let titleData = pickerData[row]
            let myTitle = NSAttributedString(string: titleData , attributes: [NSFontAttributeName:UIFont.optimaRegular(Size: 15)])
            pickerLabel.attributedText = myTitle
            pickerLabel.textAlignment = .center
            return pickerLabel
        
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        strstatus = pickerData[row]
        print(strstatus)
    }
    
    func selectDone(_ sender: Any)
    {
        labelstatustextObj.text = strstatus
        statustoolbar.isHidden = true
        statusPickerView.isHidden = true
    }
    
    func selectCancel()
    {
        statustoolbar.isHidden = true
        statusPickerView.isHidden = true

    }
    

    @IBAction func submitAct(_ sender: Any)
    {
        
    }
    func apiCall()
    {
        self.view.endEditing(true)
        SVProgressHUD.show()
        
        let dict = UserDefaults.standard.object(forKey: kLoginUserDic) as! NSDictionary
        
        
        let userId = dict.object(forKey: "id") as! String?
        
        let global = GlobalMethods()
        let param =  [GlobalMethods.METHOD_NAME: "saveMyHome","agent_id":userId!,"id": homeDict.object(forKey: "") as! String ,"is_assign":homeDict.object(forKey: "") as! String] as [String : Any]
        
        global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
            
            
            if error != nil
            {
                SVProgressHUD.showInfo(withStatus: error?.description as String!)
            }
            else
            {
                
                let dictResponse = Response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0
                {
                    
                    
                    SVProgressHUD.showSuccess(withStatus: dictResponse.object(forKey: "message") as! String!)
                    
                    let _ = self.navigationController?.popViewController(animated: true)
                    
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                }
            }
        }
    }
}
