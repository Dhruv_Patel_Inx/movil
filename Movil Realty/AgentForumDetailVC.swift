//
//  AgentForumDetailVC.swift
//  Movil Realty
//
//  Created by BAPS on 4/12/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class AgentForumDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate {
    
    
    
    
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnAddComment: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var tblComment: UITableView!
    @IBOutlet weak var swtLike: UISwitch!
    @IBOutlet weak var lblLike: UILabel!
    
    
    @IBOutlet weak var lblAgentForum: UILabel!
    @IBOutlet weak var lblFeedback: UILabel!
    @IBOutlet weak var lblWantThis: UILabel!
    @IBOutlet weak var lblPeople: UILabel!
    @IBOutlet weak var lblThis: UILabel!
    
    
    
    var comments_array = NSMutableArray()
    var SuggestionDetail_dict = NSDictionary()

    var txtComment = UITextView()
    var ScrollviewObj = UIScrollView()
    var date_string = NSString()
    var switch_tag = Int()
    
    
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        
        
//        swtLike.isOn = false
//        comments_array = ["comment_1","comment_2","comment_3","comment_4","comment_5","comment_6","comment_7","comment_8","comment_9","comment_10"]
        print(SuggestionDetail_dict)
        
        
        /*---------- Text Field Delegate/Layout  ----------*/

        txtComment.frame =  CGRect(x: 0, y: view.frame.height - (64+50), width: view.frame.width, height: 50)
        txtComment.font = UIFont(name: "Optima", size: 15.0)
        txtComment.autocorrectionType = UITextAutocorrectionType.yes
        txtComment.keyboardType = UIKeyboardType.default
        txtComment.returnKeyType = UIReturnKeyType.done
        txtComment.layer.borderWidth = 1.0
        txtComment.layer.cornerRadius = 3.0
        txtComment.textColor = OrangeThemeColor
        txtComment.delegate = self
//        self.view.addSubview(txtComment)
        
        
        /*---------- Scroll View Delegate/Layout  ----------*/
        
        ScrollviewObj.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        ScrollviewObj.addSubview(txtComment)
        self.view.addSubview(ScrollviewObj)
        ScrollviewObj.isHidden = true
        
        /*---------- Table View Delegate/Layout  ----------*/
        
        tblComment.delegate = self
        tblComment.dataSource = self
        tblComment.tableFooterView = UIView()
        tblComment.rowHeight = UITableViewAutomaticDimension
        tblComment.estimatedRowHeight = 100
        
        
        /*---------- Switch Layout  ----------*/

        swtLike.transform = CGAffineTransform(scaleX: 0.70, y: 0.70)

        
        
        
        /*---------- Get Today's Date  ----------*/

        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = "YYYY-MM-dd"
        date_string = formatter.string(from: date) as NSString
        print(date_string)
        
        
        /*---------- Set Up Values  ----------*/

        
//        lblLike.text = "122"
        
        
        lblLike.text = "\(SuggestionDetail_dict.object(forKey: "like")!)"
        lblTitle.text = "\(SuggestionDetail_dict.object(forKey: "topic")!)"
        lblDescription.text = "\(SuggestionDetail_dict.object(forKey: "description")!)"

        
        
        
        /*---------- Tap Gesture Recognizer  ----------*/

        let tap = UITapGestureRecognizer(target: self, action:#selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        /*---------- Label Layout  ----------*/
        
        lblLike.sizeToFit()
        
        
        
        
        
        
        
        
        self.AllCommentApiCall()
        
        
        
        
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()

        
        
//        func keyboardWillShow(notification:NSNotification) {
//            adjustingHeight(show: true, notification: notification)
//        }
//        
//        func keyboardWillHide(notification:NSNotification) {
//            adjustingHeight(show: false, notification: notification)
//        }
        
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    // MARK: - All Actions

    
    @IBAction func selBackAct(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    @IBAction func selSwtLikeAct(_ sender: Any) {
        
        
        
        if swtLike.isOn == true
        {
            switch_tag = 1
            self.LikeSuggestionApiCall()

            
        }
        else
        {
            
            switch_tag = 0
            self.LikeSuggestionApiCall()

            
        }
        
        
    }
    
    
    @IBAction func selAddCommentAct(_ sender: Any) {
        
        txtComment.becomeFirstResponder()
        ScrollviewObj.isHidden = false
        txtComment.isHidden = false
        
        
        
        UIView.animate(withDuration: 0.30, animations: {
            self.ScrollviewObj.contentOffset.y = self.ScrollviewObj.frame.origin.y + 220
        })
        
        let neFrame = self.view.convert(ScrollviewObj.frame, to: txtComment.superview)
        print(neFrame)
        print(ScrollviewObj.frame)
        
        print("Bound: \(ScrollviewObj.bounds.origin.y)")
        print("frame: \(ScrollviewObj.frame.origin.y)")
        
        
        
    }
    
    
    
    // MARK: - Table View Delegate Methods
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return comments_array.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let comment = tblComment.dequeueReusableCell(withIdentifier: "CommentCellVC")as! CommentCell

        print(comments_array)
        
        
        var temp_dict = NSDictionary()
        
//        if temp_dict != nil{
        
            temp_dict = comments_array.object(at: indexPath.row)as! NSDictionary
            
            
            
            comment.lblName.text = "\(temp_dict.object(forKey: "agent_name")!)"
            comment.lblComment.text = "\(temp_dict.object(forKey: "forum_comment")!)"
        
        
        
            comment.lblDate.text = "\(temp_dict.object(forKey: "forum_post_date")!)"
            
//            let imagUrlString = temp_dict.object(forKey: "agent_pic") as! String
//            let url = URL(string: imagUrlString)
//            let placeimage = UIImage(named: profile_icon)
//            comment.imgViewUser.sd_setImage(with: url, placeholderImage: placeimage)
            comment.imgViewUser.layer.cornerRadius = comment.imgViewUser.frame.height / 2
//            comment.imgViewUser.sd_setImage(with: URL(string:temp_dict.object(forKey: "agent_pic") as! String)!)
        
        let imgUrl = "\(temp_dict.object(forKey: "agent_pic")!)"
        let url = URL(string: imgUrl )
        let placeHolderImage = "Profile"
        let placeimage = UIImage(named: placeHolderImage)
        comment.imgViewUser.sd_setImage(with: url, placeholderImage: placeimage)
        
        
        
            
//        }
        
        
        return comment
    }
    
    
    // MARK: - Text View Delegate Methods
    
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        txtComment.text = ""
        txtComment.textColor = UIColor.black
        return true
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
      
       
        if (text == "\n")
        {
            view.endEditing(true)
            
            
            self.AddCommentApiCall()
            
            
//            self.AllCommentApiCall()
            
            
            
            self.ScrollviewObj.setContentOffset(.zero, animated: true)
            ScrollviewObj.isHidden = true
            txtComment.isHidden = true
            txtComment.resignFirstResponder()

            return false
        }
        else
        {
            return true
        }
    }
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//    {
//        suggestion_dict = suggestion_array.object(at: indexPath.row) as! NSDictionary
//        print(suggestion_dict)
//        
//    }

    
    // MARK: - Text Field Delegate

    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
//        txtComment.becomeFirstResponder()
        return true
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        txtComment.resignFirstResponder()
        return true
    }
    

    
    
      // MARK: - Web Service Call Methods
    
    
    func AddCommentApiCall()
    {
        
        
        btnBack.isEnabled = false
        btnAddComment.isEnabled = false
        swtLike.isEnabled = false
        
        
        self.view.endEditing(true)
        SVProgressHUD.show()
        
        print("\(SuggestionDetail_dict.object(forKey: "user_id")!)")
        print(txtComment.text!)
        print(date_string)

        
        if (txtComment.text?.characters.count)! <= 0
        {
            SVProgressHUD.showError(withStatus: "Enter Comment")
        }
        else
        {
        
        
        let global = GlobalMethods()
//        let param =  [GlobalMethods.METHOD_NAME: "saveForumComment","user_id": SuggestionDetail_dict.object(forKey: "user_id")!,"forum_id":SuggestionDetail_dict.object(forKey: "user_id")!,"comment":"\(txtComment.text!)","comment_date":"\(date_string)"] as [String : Any]
        
        
        
        let param = [GlobalMethods.METHOD_NAME: "saveForumComment","user_id":"\(SuggestionDetail_dict.object(forKey: "user_id")!)","forum_id":"\(SuggestionDetail_dict.object(forKey: "id")!)","comment":"\(txtComment.text!)","comment_date":"\(date_string)"] as [String : Any]
        
        global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
            
            
            if error != nil
            {
                SVProgressHUD.showInfo(withStatus: error?.description as String!)
            }
            else
            {
                
                let dictResponse = Response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0
                {
                    
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        
                        SVProgressHUD.dismiss()
                        SVProgressHUD.showInfo(withStatus: "Appointment Booked Successfully")
                    })

                    self.btnBack.isEnabled = true
                    self.btnAddComment.isEnabled = true
                    self.swtLike.isEnabled = true
                    

                    print(Response)
                    
                    
                    /*---------- Sync comment_array by Calling AllCommentApiCall()  ----------*/

                    self.AllCommentApiCall()
                    
//                    let data_dict = Response.object(forKey: "data")as! NSDictionary
//                    let comment_array = data_dict.object(forKey: "comment")as! NSArray
//                    print(data_dict)
//                    print(comment_array)
                    
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                }
            }
        }
            
        }
    }

    
    func AllCommentApiCall()
    {
        
        btnBack.isEnabled = false
        btnAddComment.isEnabled = false
        swtLike.isEnabled = false
        
        let userDefaults = Foundation.UserDefaults.standard
        let dict = userDefaults.object(forKey: kLoginUserDic)as! NSDictionary
        print("DICT IS: \(dict)")
        let agent_id = "\(dict.object(forKey: "id")!)"
        print("\(agent_id)")
        
        
        
        self.view.endEditing(true)
        SVProgressHUD.show()
        
        print("\(SuggestionDetail_dict.object(forKey: "id")!)")
        print(txtComment.text!)
        print(date_string)
        
        
        let global = GlobalMethods()
        //        let param =  [GlobalMethods.METHOD_NAME: "saveForumComment","user_id": SuggestionDetail_dict.object(forKey: "user_id")!,"forum_id":SuggestionDetail_dict.object(forKey: "user_id")!,"comment":"\(txtComment.text!)","comment_date":"\(date_string)"] as [String : Any]
        
        
        
        let param = [GlobalMethods.METHOD_NAME: "forumCommentListing","forum_id":"\(SuggestionDetail_dict.object(forKey: "id")!)","agent_id":"\(agent_id)!)"] as [String : Any]
        
        
        global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
            
            
            if error != nil
            {
                SVProgressHUD.showInfo(withStatus: error?.description as String!)
            }
            else
            {
                
                let dictResponse = Response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0
                {
                    SVProgressHUD.dismiss()
                    self.btnBack.isEnabled = true
                    self.btnAddComment.isEnabled = true
                    self.swtLike.isEnabled = true
                    
                    print(Response)
                    
                    
                    let like_status : Any
                    
                    let data_dict = Response.object(forKey: "data")as! NSDictionary
                    let data_array = data_dict.object(forKey: "comment")as! NSArray
                    like_status = data_dict.object(forKey: "like_status")!
                    self.comments_array = data_array.mutableCopy() as! NSArray as! NSMutableArray

                    print("data_dict:  \(data_dict)")
                    print("data_array:  \(data_array)")
                    print("like_status:  \(like_status)")
                    print("comments_array:  \(self.comments_array)")

                    
                    /*---------- Setting Up Switch ----------*/
                    
                    let status = "\(like_status)"
                    
                    if status.isEqual("0")
                    {
                        self.swtLike.isOn = false
                    }
                    else
                    {
                        self.swtLike.isOn = true
                    }
                    
                    
                    
                    
                    
                    self.tblComment.reloadData()
                    
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                    self.btnBack.isEnabled = true
                    self.btnAddComment.isEnabled = true
                    self.swtLike.isEnabled = true
                }
            }
        }
    }

    
    func LikeSuggestionApiCall()
    {
        
        btnBack.isEnabled = false
        btnAddComment.isEnabled = false
        swtLike.isEnabled = false

        
        
        let userDefaults = Foundation.UserDefaults.standard
        let dict = userDefaults.object(forKey: kLoginUserDic)as! NSDictionary
        print("DICT IS: \(dict)")
        let agent_id = "\(dict.object(forKey: "id")!)"
        print("\(agent_id)")
        print()
        
        
        self.view.endEditing(true)
        SVProgressHUD.show()
        
        print("\(SuggestionDetail_dict.object(forKey: "user_id")!)")
        print(txtComment.text!)
        print(date_string)
        
        
        let global = GlobalMethods()
        
        
        let param =  [GlobalMethods.METHOD_NAME: "saveForumLikes","forum_id":"\(SuggestionDetail_dict.object(forKey: "id")!)","agent_id":"\(agent_id)","agent_like":switch_tag] as [String : Any]
        
        
        
        
        
        global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
            
            
            if error != nil
            {
                SVProgressHUD.showInfo(withStatus: error?.description as String!)
            }
            else
            {
                
                let dictResponse = Response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0
                {
                    SVProgressHUD.dismiss()
                    self.btnBack.isEnabled = true
                    self.btnAddComment.isEnabled = true
                    self.swtLike.isEnabled = true

                    
                    print(Response)
                    
                    if self.switch_tag == 0
                    {
                        self.swtLike.isOn = false
                    }
                    else
                    {
                        self.swtLike.isOn = true
                    }
                    
                    
                    let data_dict = Response.object(forKey: "data")as! NSDictionary
                    let total_like = data_dict.object(forKey: "total_like")as! Int
                    
                    self.lblLike.text = "\(total_like)"
                    
                    //                    let data_dict = Response.object(forKey: "data")as! NSDictionary
                    //                    let comment_array = data_dict.object(forKey: "comment")as! NSArray
                    //                    print(data_dict)
                    //                    print(comment_array)
                    
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
//                    self.btnBack.isEnabled = true
//                    self.btnAddComment.isEnabled = true
//                    self.swtLike.isEnabled = true
                }
            }
        }
    }

    
    
    
    
    
    
    
    
    
    
    
    
    // MARK: - Custom Methods


    func dismissKeyboard() {
        
        
        self.ScrollviewObj.setContentOffset(.zero, animated: true)
        ScrollviewObj.isHidden = true
        txtComment.isHidden = true
        txtComment.resignFirstResponder()
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        tblComment.resignFirstResponder()
    }
    

    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        
        
        lblAgentForum.text = strAgentForum
        lblFeedback.text = strFeedback
        lblPeople.text = strpeople
        lblThis.text = strthis
        lblWantThis.text = strIWantthis
        btnAddComment.setTitle(strAddComment, for: UIControlState.normal)
    }


//    func keyboardWillShow(notification:NSNotification) {
//        adjustingHeight(show: true, notification: notification)
//    }
//    
//    func keyboardWillHide(notification:NSNotification) {
//        adjustingHeight(show: false, notification: notification)
//    }
//    
//    func adjustingHeight(show:Bool, notification:NSNotification) {
//        // 1
//        var userInfo = notification.userInfo!
//        // 2
//        let keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
//        // 3
//        let animationDurarion = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! TimeInterval
//        // 4
//        let changeInHeight = (keyboardFrame.height + 40) * (show ? 1 : -1)
//        //5
//        UIView.animateWithDuration(animationDurarion, animations: { () -> Void in
//            self.bottomConstraint.constant += changeInHeight
//        })
//        
//    }
    
    
//    
//    func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y == 0{
//                self.view.frame.origin.y -= keyboardSize.height
//            }
//        }
//    }
//    
//    func keyboardWillHide(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y != 0{
//                self.view.frame.origin.y += keyboardSize.height
//            }
//        }
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
