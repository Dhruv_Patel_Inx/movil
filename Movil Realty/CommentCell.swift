//
//  CommentCell.swift
//  Movil Realty
//
//  Created by BAPS on 4/12/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    
    
    
    @IBOutlet weak var lblName: UILabel!

    @IBOutlet weak var lblComment: UILabel!
    
    @IBOutlet weak var imgViewUser: UIImageView!
    
    @IBOutlet var lblDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        imgViewUser.layer.cornerRadius = imgViewUser.frame.size.height/2
        imgViewUser.clipsToBounds = true
        // Configure the view for the selected state
    }

}
