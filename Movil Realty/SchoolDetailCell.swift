//
//  SchoolDetailCell.swift
//  Movil Realty
//
//  Created by BAPS on 4/24/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class SchoolDetailCell: UITableViewCell {

    
    @IBOutlet weak var lblSchoolTitle: UILabel!
    
    @IBOutlet weak var lblSchoolSubTitle: UILabel!
    
    
    @IBOutlet weak var lblSchoolType: UILabel!
    
    
    @IBOutlet weak var lblSchoolTime: UILabel!
    
    
    @IBOutlet weak var lblSchoolDistance: UILabel!
    
    
    @IBOutlet weak var imgViewStar_1: UIView!
    
    @IBOutlet weak var imgViewStar_2: UIView!
    
    
    @IBOutlet weak var imgViewStar_3: UIView!
    
    
    @IBOutlet weak var imgViewStar_4: UIView!
    
    
    
    @IBOutlet weak var imgViewStar_5: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        imgViewStar_1.layer.cornerRadius = imgViewStar_1.frame.height / 2
        imgViewStar_2.layer.cornerRadius = imgViewStar_1.frame.height / 2
        imgViewStar_3.layer.cornerRadius = imgViewStar_1.frame.height / 2
        imgViewStar_4.layer.cornerRadius = imgViewStar_1.frame.height / 2
        imgViewStar_5.layer.cornerRadius = imgViewStar_1.frame.height / 2

        
        imgViewStar_1.clipsToBounds = true
        imgViewStar_2.clipsToBounds = true
        imgViewStar_3.clipsToBounds = true
        imgViewStar_4.clipsToBounds = true
        imgViewStar_5.clipsToBounds = true
        
        // Configure the view for the selected state
    }

}
