//
//  ViewController.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 10/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import CoreLocation

let appdel:AppDelegate = UIApplication.shared.delegate as! AppDelegate


class ViewController: UIViewController {

    @IBOutlet var progressBar:UIProgressView!
    
    @IBOutlet weak var lblLoading: UILabel!
    
    
    var locationManager:CLLocationManager!
    
    
    
    // MARK: - Life Cycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        // Do any additional setup after loading the view, typically from a nib.
        
        let parameters =
            [GlobalMethods.METHOD_NAME: "getListing","latitude":"35.99868","longitude":"-78.898519"] as [String : Any]
        let globalMethodObj = GlobalMethods()
        
        globalMethodObj.callWebService(parameter: parameters as AnyObject!) { (response, error) in
            
            if error != nil
            {
                SVProgressHUD.showError(withStatus: error?.localizedDescription)
            }
            else
            {
                let dataDict  = response.object(forKey: "data") as! NSDictionary
                if let agents = dataDict.object(forKey: "agent") as? [Any] {
                    appdel.arrayAgents.addObjects(from: agents)
                }
                
                if let proparty = dataDict.object(forKey: "property") as? [Any] {
                    appdel.arrayProparty.addObjects(from: proparty)
                }
            }
            
            appdel.tabbar = self.storyboard?.instantiateViewController(withIdentifier: "tabBarControllerVC") as! tabBarController
            self.navigationController?.pushViewController(appdel.tabbar, animated: true)
        }
        
        
        /*---------- Call SetUpValue ---------*/
        
        self.SetUpValue()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    
    
    // MARK: - SetUp Value Method
    
    func SetUpValue()
    {
        
        lblLoading.text = strLoading
        
    }

    
    

}

