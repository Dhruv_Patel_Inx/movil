//
//  RIghtMenuViewController.swift
//  Movil Realty
//
//  Created by BAPS on 4/5/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class RIghtMenuViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIToolbarDelegate , UIScrollViewDelegate{
   

    
    @IBOutlet weak var btnReset: UIButton!
    
    @IBOutlet weak var ScrollViewObj: UIScrollView!
    
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var lblSubType: UILabel!
    @IBOutlet weak var btnSubType: UIButton!
    
    
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var slidePrice: NMRangeSlider!
    @IBOutlet weak var lblLowerPrice: UILabel!
    
    
    @IBOutlet weak var lblSquareFeet: UILabel!
    @IBOutlet weak var slideSquareFit: NMRangeSlider!
    @IBOutlet weak var lblLowerSquareFeet: UILabel!
        
    
    @IBOutlet weak var BedroomView: UIView!
    @IBOutlet weak var lblBedrooms: UILabel!
    @IBOutlet weak var lblBedroomNumber: UILabel!
    @IBOutlet weak var btnBedroomPlus: UIButton!
    @IBOutlet weak var btnBedroomMinus: UIButton!
    
    
    @IBOutlet weak var BathroomView: UIView!
    @IBOutlet weak var lblBathroom: UILabel!
    @IBOutlet weak var lblBathroomNumber: UILabel!
    @IBOutlet weak var btnBathroomPlus: UIButton!
    @IBOutlet weak var btnBathroomMinus: UIButton!
    
    
    @IBOutlet weak var txtField: UITextField!
    
    
    @IBOutlet weak var GarageView: UIView!
    @IBOutlet weak var lblGarage: UILabel!
    @IBOutlet weak var lblGarageNumber: UILabel!
    @IBOutlet weak var btnGaragePlus: UIButton!
    @IBOutlet weak var btnGarageMinus: UIButton!
    
    
    @IBOutlet weak var lblListaDate: UILabel!
    @IBOutlet weak var btnListDate: UIButton!
    
    
    @IBOutlet weak var swtWaterFront: UISwitch!
    @IBOutlet weak var swtReducedDays: UISwitch!
    @IBOutlet weak var swtOpenHouse: UISwitch!

    
    @IBOutlet weak var btnSearchHomes: UIButton!
    
    
    
    var Temp_Dict = NSMutableDictionary()
    
    
    let picker = UIPickerView()
    var toolbarObj = UIToolbar()
    var selectedtag = Int()
    var status_array = NSArray()
    var type_array = NSArray()
    var Subtype_array = NSArray()
    var strDate = String()
    var datePickerObj = UIDatePicker()
    var pickertag = Int()
    
    var tap = UITapGestureRecognizer()
    
    
    // MARK: - View LifeCyCle Methods

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedtag = 0
        pickertag = 0
        
        status_array = ["Active","Pending","Contigent","Closed"]
        type_array = ["Type_1","Type_2","Type_3"]
        Subtype_array = ["SubType_1","SubType_2","SubType_3","SubType_4"]
        
        txtField.isUserInteractionEnabled = false
        
    
        
        /*---------- Button Lay out ----------*/
        
        btnReset.layer.cornerRadius = 3.0
        btnStatus.layer.cornerRadius = 3.0
        btnType.layer.cornerRadius = 3.0
        btnListDate.layer.cornerRadius = 3.0
        btnSubType.layer.cornerRadius = 3.0
        BedroomView.layer.cornerRadius = 3.0
        BathroomView.layer.cornerRadius = 3.0
        GarageView.layer.cornerRadius = 3.0
        txtField.layer.cornerRadius = 3.0

        
    
        btnBedroomPlus.layer.borderWidth = 1.0
        btnBedroomMinus.layer.borderWidth = 1.0
        btnBathroomPlus.layer.borderWidth = 1.0
        btnBathroomMinus.layer.borderWidth = 1.0
        btnGaragePlus.layer.borderWidth = 1.0
        btnGarageMinus.layer.borderWidth = 1.0
        txtField.layer.borderWidth = 1.0
        
        btnBedroomPlus.layer.cornerRadius = 3.0
        btnBedroomMinus.layer.cornerRadius = 3.0
        btnBathroomPlus.layer.cornerRadius = 3.0
        btnBathroomMinus.layer.cornerRadius = 3.0
        btnGaragePlus.layer.cornerRadius = 3.0
        btnGarageMinus.layer.cornerRadius = 3.0
        
        
        btnBedroomPlus.layer.borderColor = UIColor.gray.cgColor
        btnBedroomMinus.layer.borderColor = UIColor.gray.cgColor
        btnBathroomPlus.layer.borderColor = UIColor.gray.cgColor
        btnBathroomMinus.layer.borderColor = UIColor.gray.cgColor
        btnGaragePlus.layer.borderColor = UIColor.gray.cgColor
        btnGarageMinus.layer.borderColor = UIColor.gray.cgColor
        txtField.layer.borderColor = UIColor.gray.cgColor

        
        
        btnStatus.layer.borderWidth = 1.0
        btnType.layer.borderWidth = 1.0
        btnSubType.layer.borderWidth = 1.0
        btnListDate.layer.borderWidth = 1.0
        BedroomView.layer.borderWidth = 1.0
        BathroomView.layer.borderWidth = 1.0
        GarageView.layer.borderWidth = 1.0
        
        BedroomView.layer.borderColor = UIColor.gray.cgColor
        BathroomView.layer.borderColor = UIColor.gray.cgColor
        GarageView.layer.borderColor = UIColor.gray.cgColor
        btnStatus.layer.borderColor = UIColor.lightGray.cgColor
        btnType.layer.borderColor = UIColor.lightGray.cgColor
        btnSubType.layer.borderColor = UIColor.lightGray.cgColor
        btnListDate.layer.borderColor = UIColor.lightGray.cgColor

        
        /*---------- Slider Image Setting ----------*/

        
        slidePrice.trackBackgroundImage = UIImage(named: "SliderPath")
        slideSquareFit.trackBackgroundImage = UIImage(named: "SliderPath")
        slidePrice.trackImage = UIImage(named: "SliderPath")
        slideSquareFit.trackImage = UIImage(named: "SliderPath")
        slidePrice.lowerHandleImageNormal = UIImage(named: "FilterSlider")
        slideSquareFit.lowerHandleImageNormal = UIImage(named: "FilterSlider")
        slidePrice.upperHandleImageNormal = UIImage(named: "FilterSlider")
        slideSquareFit.upperHandleImageNormal = UIImage(named: "FilterSlider")
        slidePrice.lowerHandleImageHighlighted = UIImage(named: "FilterSlider")
        slideSquareFit.lowerHandleImageHighlighted = UIImage(named: "FilterSlider")
        slidePrice.upperHandleImageHighlighted = UIImage(named: "FilterSlider")
        slideSquareFit.upperHandleImageHighlighted = UIImage(named: "FilterSlider")

//        slidePrice.lowerHandle.image = UIImage(named: "FilterSlider")
//        slideSquareFit.lowerHandle.image = UIImage(named: "FilterSlider")
//        slidePrice.upperHandle.image = UIImage(named: "FilterSlider")
//        slidePrice.upperHandle.image = UIImage(named: "FilterSlider")

        
        /*---------- Picker View ----------*/
        
        picker.frame = CGRect(x: 0, y: self.view.frame.height - 200, width: self.view.frame.width, height: 200)
        picker.backgroundColor = UIColor.white
        picker.delegate = self
        picker.dataSource = self
        self.view.addSubview(picker)
        picker.isHidden = true
        
        

        

        /*---------- Toolbar View ----------*/

        toolbarObj = UIToolbar.init(frame: CGRect(x: 0, y: view.frame.height - (50+200), width: view.frame.width, height: 50))
        toolbarObj.barStyle = UIBarStyle.default
        toolbarObj.delegate = self
        
        let donebutton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(RIghtMenuViewController.PickerDoneSelAct))
        let cancelbutton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(RIghtMenuViewController.PickerCancelSelAct))
        let fixspacebutton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        toolbarObj.setItems([donebutton,fixspacebutton,cancelbutton], animated: false)
        toolbarObj.isUserInteractionEnabled = true
        self.view.addSubview(toolbarObj)
        toolbarObj.isHidden = true
        
        
        toolbarObj.isTranslucent = false
        toolbarObj.barTintColor = UIColor(red: 255/255, green: 102/255, blue: 30/255, alpha: 1.0)
        donebutton.tintColor = UIColor.white
        cancelbutton.tintColor = UIColor.white
        donebutton.setTitleTextAttributes([NSFontAttributeName:UIFont.optimaBold(Size: 18.0),NSForegroundColorAttributeName : UIColor.white], for: .normal)
        cancelbutton.setTitleTextAttributes([NSFontAttributeName:UIFont.optimaBold(Size: 18.0),NSForegroundColorAttributeName : UIColor.white], for: .normal)

        
        
        /*---------- Slider View ----------*/
        
        slideSquareFit.minimumValue = 1000
        slideSquareFit.maximumValue = 20000
        slidePrice.minimumValue = 10000
        slidePrice.maximumValue = 200000
        
        
        /*---------- Switch View Layout ----------*/

        swtWaterFront.transform = CGAffineTransform(scaleX: 0.65, y: 0.65)
        swtReducedDays.transform = CGAffineTransform(scaleX: 0.65, y: 0.65)
        swtOpenHouse.transform = CGAffineTransform(scaleX: 0.65, y: 0.65)

        
        
        /*----- create datepicker  -----*/
        
        datePickerObj = UIDatePicker.init(frame: CGRect(x: 0, y: view.frame.height - 200, width: view.frame.width, height: 200))
        datePickerObj.datePickerMode = UIDatePickerMode.date
        datePickerObj.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        datePickerObj.backgroundColor = UIColor.white
        self.view.addSubview(datePickerObj)

        datePickerObj.isHidden = true

        
        ScrollViewObj.delegate = self
        
        
        
        
        
        
       
        
        
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        print("viewWillAppear")
        
        
        
        
        
        /*----- Check Search Dictionary availibility -----*/
        
        if UserDefaults.standard.object(forKey: kSearchDict) == nil
        {
            
            let check = 0
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let today_date = formatter.string(from: date)
            print("\(today_date)")
            
            Temp_Dict.setValue("Active", forKey: "Status")
            Temp_Dict.setValue("Residential", forKey: "Type")
            Temp_Dict.setValue("AnySubType", forKey: "SubType")
            Temp_Dict.setValue("\(check)", forKey: "Bathroom")
            Temp_Dict.setValue("\(check)", forKey: "Bedroom")
            Temp_Dict.setValue("\(check)", forKey: "Garage")
            Temp_Dict.setValue("1", forKey: "FrontWater")
            Temp_Dict.setValue("1", forKey: "ReduceDay")
            Temp_Dict.setValue("1", forKey: "OpenHouse")
            Temp_Dict.setValue("\(1000)", forKey: "SquareFeet_LowerSlide")
            Temp_Dict.setValue("\(5000)", forKey: "SquareFeet_UpperSlide")
            Temp_Dict.setValue("\(10000)", forKey: "Price_LowerSlide")
            Temp_Dict.setValue("\(30000)", forKey: "Price_UpperSlide")
            Temp_Dict.setValue("\(today_date)", forKey: "List_Date")

            
            
            
            
            
            /*----- create appointemnt user default -----*/
            let userDefaults = Foundation.UserDefaults.standard
            userDefaults.set(Temp_Dict, forKey: kSearchDict)
            userDefaults.synchronize()
            
            slidePrice.upperValue = 30000
            slidePrice.lowerValue = 10000

            
            
            slideSquareFit.upperValue = 5000
            slideSquareFit.lowerValue = 1000

            
            btnStatus.setTitle("Ative", for: .normal)
            btnType.setTitle("Residential", for: .normal)
            btnSubType.setTitle("AnySubType", for: .normal)

            lblBedroomNumber.text = "0"
            lblBedroomNumber.text = "0"
            lblGarageNumber.text = "0"
            
            lblPrice.text = "30000"
            lblLowerPrice.text = "10000"
            
            lblSquareFeet.text = "5000"
            lblLowerSquareFeet.text = "1000"

//            self.setupvalue()

            
        }
        else
        {
            self.setupvalue()
            
        }
        
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    
    // MARK: - All Button Action

    
    
    @IBAction func selResetAct(_ sender: Any) {
        
        let check = 0
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let today_date = formatter.string(from: date)
        print("\(today_date)")
        
        Temp_Dict.setValue("Active", forKey: "Status")
        Temp_Dict.setValue("Residential", forKey: "Type")
        Temp_Dict.setValue("AnySubType", forKey: "SubType")
        Temp_Dict.setValue("\(check)", forKey: "Bathroom")
        Temp_Dict.setValue("\(check)", forKey: "Bedroom")
        Temp_Dict.setValue("\(check)", forKey: "Garage")
        Temp_Dict.setValue("1", forKey: "FrontWater")
        Temp_Dict.setValue("1", forKey: "ReduceDay")
        Temp_Dict.setValue("1", forKey: "OpenHouse")
        Temp_Dict.setValue("\(1000)", forKey: "SquareFeet_LowerSlide")
        Temp_Dict.setValue("\(5000)", forKey: "SquareFeet_UpperSlide")
        Temp_Dict.setValue("\(10000)", forKey: "Price_LowerSlide")
        Temp_Dict.setValue("\(30000)", forKey: "Price_UpperSlide")
        Temp_Dict.setValue("\(today_date)", forKey: "List_Date")
        
        
        
        
        /*----- create appointemnt user default -----*/
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.set(Temp_Dict, forKey: kSearchDict)
        userDefaults.synchronize()
        
        
        
        slidePrice.upperValue = 30000
        slidePrice.lowerValue = 10000

        
        slideSquareFit.upperValue = 5000
        slideSquareFit.lowerValue = 1000

        
        btnStatus.setTitle("Ative", for: .normal)
        btnType.setTitle("Residential", for: .normal)
        btnSubType.setTitle("AnySubType", for: .normal)
        
        lblBedroomNumber.text = "0"
        lblBedroomNumber.text = "0"
        lblGarageNumber.text = "0"
        
        
        lblPrice.text = "30000"
        lblLowerPrice.text = "10000"

        lblSquareFeet.text = "5000"
        lblLowerSquareFeet.text = "1000"

        
    }
    
    
    @IBAction func selStatusAct(_ sender: Any) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        selectedtag = 1
        pickertag = 0
        picker.isHidden = false
        toolbarObj.isHidden = false
        picker.reloadAllComponents()
        
        
    }
    
    
    
    @IBAction func selTypeAct(_ sender: Any) {
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        selectedtag = 2
        pickertag = 0
        picker.isHidden = false
        toolbarObj.isHidden = false
        picker.reloadAllComponents()

    }
    
    
    @IBAction func selSubType(_ sender: Any) {
        
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        selectedtag = 3
        pickertag = 0
        picker.isHidden = false
        toolbarObj.isHidden = false
        picker.reloadAllComponents()

    }
    
    
    
    @IBAction func selBedroomPlusAct(_ sender: Any) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        
        let myString : String = (Temp_Dict.object(forKey: "Bedroom")!) as! String
        print("\(myString)")
        var check: Int =   Int(myString)!
        print("\(check)")
        
        
        check += 1
        
        lblBedroomNumber.text = "\(check)"
        
        print("Bedroom: \(check)")

        Temp_Dict.setValue("\(check)", forKey: "Bedroom")
        
        
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.removeObject(forKey: kSearchDict)
        userDefaults.set(Temp_Dict, forKey: kSearchDict)
        userDefaults.synchronize()

        
        
    }
    
    @IBAction func selBedroomMinusAct(_ sender: Any) {
        
        
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        
        
        let myString : String = (Temp_Dict.object(forKey: "Bedroom")!) as! String
        print("\(myString)")
        var check: Int =   Int(myString)!
        print("\(check)")
        
        
        
        if check > 0 {
            
            check -= 1
            lblBedroomNumber.text = "\(check)"
            
        }
        
        lblBedroomNumber.text = "\(check)"
        print("Bedroom: \(check)")

        Temp_Dict.setValue("\(check)", forKey: "Bedroom")
        
        
//        let userDefaults = Foundation.UserDefaults.standard
//        userDefaults.removeObject(forKey: kSearchDict)
//        userDefaults.set(Temp_Dict, forKey: kSearchDict)
//        userDefaults.synchronize()

        
        
    }
    
    
    @IBAction func selBathroomPlusAct(_ sender: Any) {
        
        
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        
        let myString : String = (Temp_Dict.object(forKey: "Bathroom")!) as! String
        print("\(myString)")
        var check: Int =   Int(myString)!
        print("\(check)")

        
        check += 1
        
        lblBathroomNumber.text = "\(check)"
        print("Bathroom: \(check)")

        Temp_Dict.setValue("\(check)", forKey: "Bathroom")
        
        
//        let userDefaults = Foundation.UserDefaults.standard
//        userDefaults.removeObject(forKey: kSearchDict)
//        userDefaults.set(Temp_Dict, forKey: kSearchDict)
//        userDefaults.synchronize()

        
        
    }
    
    
    @IBAction func selBathroomMinusAct(_ sender: Any) {
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        let myString : String = (Temp_Dict.object(forKey: "Bathroom")!) as! String
        print("\(myString)")
        var check: Int =   Int(myString)!
        print("\(check)")
        
        
        if check > 0 {
            
            check -= 1
            lblBathroomNumber.text = "\(check)"
            
        }
        
        print("Bathroom: \(check)")

        Temp_Dict.setValue("\(check)", forKey: "Bathroom")
        
        
//        let userDefaults = Foundation.UserDefaults.standard
//        userDefaults.removeObject(forKey: kSearchDict)
//        userDefaults.set(Temp_Dict, forKey: kSearchDict)
//        userDefaults.synchronize()
        

        
        
    }

    
    
    
    @available(iOS 9.0, *)
    @IBAction func selPriceSliderAct(_ sender: Any) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        self.configurePriceSlider()
        
    }
    
    
    @available(iOS 9.0, *)
    @IBAction func selSquareFeetAct(_ sender: Any) {
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        self.configureSquareFeetSlider()

    }
    
    
    
    
    @IBAction func selGaragePlusAct(_ sender: Any) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        let myString : String = (Temp_Dict.object(forKey: "Garage")!) as! String
        print("\(myString)")
        var check: Int =   Int(myString)!
        print("\(check)")

        
        
       
        
        check += 1
        
        lblGarageNumber.text = "\(check)"
        
        
        print("Garage: \(check)")

        Temp_Dict.setValue("\(check)", forKey: "Garage")
        
        
//        let userDefaults = Foundation.UserDefaults.standard
//        userDefaults.removeObject(forKey: kSearchDict)
//        userDefaults.set(Temp_Dict, forKey: kSearchDict)
//        userDefaults.synchronize()
        
        
    }
    
    @IBAction func selGarageMinusAct(_ sender: Any) {
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        let myString : String = (Temp_Dict.object(forKey: "Garage")!) as! String
        print("\(myString)")
        var check: Int =   Int(myString)!
        print("\(check)")
        
        if check > 0 {
            
            check -= 1
            lblGarageNumber.text = "\(check)"
            
        }
        print("Garage: \(check)")

        Temp_Dict.setValue("\(check)", forKey: "Garage")
        
        
//        let userDefaults = Foundation.UserDefaults.standard
//        userDefaults.removeObject(forKey: kSearchDict)
//        userDefaults.set(Temp_Dict, forKey: kSearchDict)
//        userDefaults.synchronize()
        
    }
    
    @IBAction func selListDateAct(_ sender: Any) {
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        pickertag = 1
        
        
        UIView.animate(withDuration: 0.30, animations: {
            self.ScrollViewObj.contentOffset.y = self.ScrollViewObj.frame.origin.y + 550
        })
        
        let neFrame = self.view.convert(btnListDate.frame, to: btnListDate.superview)
        print(neFrame)
        print(btnListDate.frame)
        
        print("Bound: \(ScrollViewObj.bounds.origin.y)")
        print("frame: \(ScrollViewObj.frame.origin.y)")
        
        datePickerObj.isHidden = false
        toolbarObj.isHidden = false

    }
    
    
    
    
    
    @IBAction func selSwitchWaterFrontAct(_ sender: Any) {
        
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })

        
        var check : String = (Temp_Dict.object(forKey: "FrontWater")!) as! String

        
        if check.isEqual("0") {
            
            check = "1"
            swtWaterFront.isOn = true
            
        }
        else{
            
            check = "0"
            swtWaterFront.isOn = false
            
        }
        
        print("FrontWater: \(check)")

        Temp_Dict.setValue("\(check)", forKey: "FrontWater")
        
        
//        let userDefaults = Foundation.UserDefaults.standard
//        userDefaults.removeObject(forKey: kSearchDict)
//        userDefaults.set(Temp_Dict, forKey: kSearchDict)
//        userDefaults.synchronize()
//
        
        

        
        
        
    }
    
    
    @IBAction func selSwitchReducedAct(_ sender: Any) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        var check : String = (Temp_Dict.object(forKey: "ReduceDay")!) as! String

        
        
        
        if check.isEqual("0") {
            
            check = "1"
            swtReducedDays.isOn = true
            
        }
        else{
            
            check = "0"
            swtReducedDays.isOn = false
            
        }
        print("ReduceDay: \(check)")

        Temp_Dict.setValue("\(check)", forKey: "ReduceDay")
        
        
//        let userDefaults = Foundation.UserDefaults.standard
//        userDefaults.removeObject(forKey: kSearchDict)
//        userDefaults.set(Temp_Dict, forKey: kSearchDict)
//        userDefaults.synchronize()
        
    }
    
    
    @IBAction func selSwitchOpenHouseAct(_ sender: Any) {
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        var check : String = (Temp_Dict.object(forKey: "OpenHouse")!) as! String
        
        
        
        if check.isEqual("0") {
            
            check = "1"
            swtOpenHouse.isOn = true
            
        }
        else{
            
            check = "0"
            swtOpenHouse.isOn = false
            
        }
        print("kOpenHouse: \(check)")
        Temp_Dict.setValue("\(check)", forKey: "OpenHouse")
        
        
//        let userDefaults = Foundation.UserDefaults.standard
//        userDefaults.removeObject(forKey: kSearchDict)
//        userDefaults.set(Temp_Dict, forKey: kSearchDict)
//        userDefaults.synchronize()
        

    }
    
    
    
    @IBAction func selBackAct(_ sender: Any) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func selSearchHomesAct(_ sender: Any) {
        
        
       
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
            SVProgressHUD.showInfo(withStatus: "Search Home Saved Successfully")

        })
        
//        self.SaveSearches()
        let userDefaults = Foundation.UserDefaults.standard
        userDefaults.removeObject(forKey: kSearchDict)
        userDefaults.set(Temp_Dict, forKey: kSearchDict)
        userDefaults.synchronize()

        
        
    }
    
    
    
    
    
    // MARK: - PickerView Delegates

    
  
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if selectedtag == 1{
            
            return status_array.count
            
        }
        else if selectedtag == 2
        {
            return type_array.count

        }
        else{
            return Subtype_array.count

        }
        
    }
    
    
    
   func numberOfComponents(in pickerView: UIPickerView) -> Int {
    
        return 1
    
    }
    
    
    

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
        if selectedtag == 1{
            
            return "  \(status_array[row])"
            
        }
        else if selectedtag == 2
        {
            
            return "  \(type_array[row])"

        }
        else{
            
            return "  \(Subtype_array[row])"
            
        }

        
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        if selectedtag == 1{
            
            
            btnStatus.setTitle("\(status_array[row])", for: .normal)
            
            Temp_Dict.setValue("\(status_array[row])", forKey: "Status")
            
//            let userDefaults = Foundation.UserDefaults.standard
//            userDefaults.removeObject(forKey: kSearchDict)
//            userDefaults.set(Temp_Dict, forKey: kSearchDict)
//            userDefaults.synchronize()

            
            
            
        }
        else if selectedtag == 2
        {
            
            btnType.setTitle("\(type_array[row])", for: .normal)
            
            Temp_Dict.setValue("\(type_array[row])", forKey: "Type")
            
//            let userDefaults = Foundation.UserDefaults.standard
//            userDefaults.removeObject(forKey: kSearchDict)
//            userDefaults.set(Temp_Dict, forKey: kSearchDict)
//            userDefaults.synchronize()
            
        }
        else{
            
            btnSubType.setTitle("\(Subtype_array[row])", for: .normal)
            
            Temp_Dict.setValue("\(Subtype_array[row])", forKey: "SubType")
            
//            let userDefaults = Foundation.UserDefaults.standard
//            userDefaults.removeObject(forKey: kSearchDict)
//            userDefaults.set(Temp_Dict, forKey: kSearchDict)
//            userDefaults.synchronize()

        }
        
    }
    
    /*---------- To Set the text of Picker ---------*/
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        
        
        
        
        let pickerLabel = UILabel()
       
        
        
        
        if selectedtag == 1{
            
            
            
            let titleData = status_array[row]
            let myTitle = NSAttributedString(string: titleData as! String, attributes: [NSFontAttributeName:UIFont(name: "Optima", size: 16.0)!])
            pickerLabel.attributedText = myTitle
            pickerLabel.textAlignment = .center
            return pickerLabel

            
        }
        else if selectedtag == 2
        {
            let titleData = type_array[row]
            let myTitle = NSAttributedString(string: titleData as! String, attributes: [NSFontAttributeName:UIFont(name: "Optima", size: 16.0)!])
            pickerLabel.attributedText = myTitle
            pickerLabel.textAlignment = .center
            return pickerLabel

            
            
        }
        else{
            
            let titleData = Subtype_array[row]
            let myTitle = NSAttributedString(string: titleData as! String, attributes: [NSFontAttributeName:UIFont(name: "Optima", size: 16.0)!])
            pickerLabel.attributedText = myTitle
            pickerLabel.textAlignment = .center
            return pickerLabel
            
            
        }
        
        
       

        
        
        
        
        
    }
    
    
    
    // MARK: - Scroll View Delegate Methods

    
    /*----- To Prevent Horizontal Scrolling -----*/
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if ScrollViewObj.contentOffset.x>0 {
            
            ScrollViewObj.contentOffset.x = 0
        }
        else if ScrollViewObj.contentOffset.x<0 {
            
            ScrollViewObj.contentOffset.x = 0
        }
        
        
        toolbarObj.isHidden = true
        picker.isHidden = true
        datePickerObj.isHidden = true
        
        
    }
    
    
    
    // MARK: - RangeSlider Methods
 
    
    func setLowerValue(_ lowerValue: Float, animated: Bool) {
        
        
    }
    
    
    
    // MARK: - Custom Methods
    
    
    
    @available(iOS 9.0, *)
    func configureSquareFeetSlider() {
        

        var lowerCenter = CGPoint.zero
        var upperCenter = CGPoint.zero


        if slideSquareFit.lowerHandle.isHighlighted == true {
            
            
            print("LOwer")
            lowerCenter.x = (self.slideSquareFit.lowerCenter.x + self.slideSquareFit.frame.origin.x)
            lowerCenter.y = (self.slideSquareFit.center.y - 30.0)
            //        self.lblSquareFeet.center = lowerCenter
            self.lblLowerSquareFeet.text! = "\(Int(self.slideSquareFit.lowerValue))"
            print("\(Int(self.slideSquareFit.lowerValue))")

        }
        else
        {
            upperCenter.x = (self.slideSquareFit.upperCenter.x + self.slideSquareFit.frame.origin.x)
            print("self.slideSquareFit.upperCenter.x: \(self.slideSquareFit.upperCenter.x)")
            print("self.slideSquareFit.frame.origin.x: \(self.slideSquareFit.frame.origin.x)")

            upperCenter.y = (self.slideSquareFit.center.y - 30.0)
            
            
            
            //        self.lblSquareFeet.center = upperCenter
            self.lblSquareFeet.text! = "\(Int(self.slideSquareFit.upperValue))"
            print("\(Int(self.slideSquareFit.upperValue))")

        }
        
        
        print("\(Int(self.slideSquareFit.lowerValue))")
        print("\(Int(self.slideSquareFit.upperValue))")

        
        
        Temp_Dict.setValue("\(Int(self.slideSquareFit.lowerValue))", forKey: "SquareFeet_LowerSlide")
        Temp_Dict.setValue("\(Int(self.slideSquareFit.upperValue))", forKey: "SquareFeet_UpperSlide")

        
//        let userDefaults = Foundation.UserDefaults.standard
//        userDefaults.removeObject(forKey: kSearchDict)
//        userDefaults.set(Temp_Dict, forKey: kSearchDict)
//        userDefaults.synchronize()

        
    }
    
    
    
    
    @available(iOS 9.0, *)
    func configurePriceSlider() {
        
        
        
        var lowerCenter = CGPoint.zero
        var upperCenter = CGPoint.zero

        
        if slidePrice.lowerHandle.isHighlighted == true {
            
            print("LOwer")
            lowerCenter.x = (self.slidePrice.lowerCenter.x + self.slidePrice.frame.origin.x)
            lowerCenter.y = (self.slidePrice.center.y - 30.0)
            //        self.lblSquareFeet.center = lowerCenter
            self.lblLowerPrice.text = "\(Int(self.slidePrice.lowerValue))"
            print("\(Int(self.slidePrice.lowerValue))")
            
        }
        else
        {
            upperCenter.x = (self.slidePrice.upperCenter.x + self.slidePrice.frame.origin.x)
            upperCenter.y = (self.slidePrice.center.y - 30.0)
            //        self.lblSquareFeet.center = upperCenter
            self.lblPrice.text! = "\(Int(self.slidePrice.upperValue))"
            print("\(Int(self.slidePrice.upperValue))")
            
        }
        
        
        
        print("\(Int(self.slidePrice.lowerValue))")
        print("\(Int(self.slidePrice.upperValue))")
        
        
        
        Temp_Dict.setValue("\(Int(self.slidePrice.lowerValue))", forKey: "Price_LowerSlide")
        Temp_Dict.setValue("\(Int(self.slidePrice.upperValue))", forKey: "Price_UpperSlide")
        
        
//        let userDefaults = Foundation.UserDefaults.standard
//        userDefaults.removeObject(forKey: kSearchDict)
//        userDefaults.set(Temp_Dict, forKey: kSearchDict)
//        userDefaults.synchronize()
        

        
        
    }

    
    
    
    func setupvalue()
    {
        
        
        
        
        let userDefaults = Foundation.UserDefaults.standard
        //            temp_array1 = (userDefault.object(forKey: kAppointmentDic) as! NSArray).mutableCopy() as! NSMutableArray
        
        Temp_Dict = (userDefaults.object(forKey: kSearchDict)as! NSDictionary).mutableCopy() as! NSMutableDictionary
        
        
        lblBedroomNumber.text = "\(Temp_Dict.object(forKey: "Bedroom")!)"
        lblBathroomNumber.text = "\(Temp_Dict.object(forKey: "Bathroom")!)"
        lblGarageNumber.text = "\(Temp_Dict.object(forKey: "Garage")!)"
        
        
        
        
        /*----- Set the Value of FRONT WATER Switch -----*/
        
        let check : String = (Temp_Dict.object(forKey: "FrontWater")!) as! String
        
        print("FrontWater\(check)")
        if check.isEqual("0") {
            
            swtWaterFront.isOn = false
            
        }
        else{
            
            swtWaterFront.isOn = true
            
        }
        
        /*----- Set the Value of REDUCE DAY Switch -----*/
        
        let check1 : String = (Temp_Dict.object(forKey: "ReduceDay")!) as! String
        
        
        
        print("ReduceDay\(check1)")
        
        if check1.isEqual("0") {
            
            swtReducedDays.isOn = false
            
        }
        else{
            
            swtReducedDays.isOn = true
            
        }
        
        
        /*----- Set the Value of OPEN HOUSE Switch -----*/
        
        let check3 : String = (Temp_Dict.object(forKey: "OpenHouse")!) as! String
        
        
        print("OpenHouse\(check3)")
        
        if check3.isEqual("0") {
            
            swtOpenHouse.isOn = false
            
        }
        else{
            
            swtOpenHouse.isOn = true
            
        }
        
        /*----- Set the Value of PICKERS: STATUS/TYPE/SUBTYPE -----*/
        btnStatus.setTitle("\(Temp_Dict.object(forKey: "Status")!)", for: .normal)
        btnType.setTitle("\(Temp_Dict.object(forKey: "Type")!)", for: .normal)
        btnSubType.setTitle("\(Temp_Dict.object(forKey: "SubType")!)", for: .normal)
        btnListDate.setTitle("\(Temp_Dict.object(forKey: "List_Date")!)", for: .normal)

        
        
        /*----- Set the Value of Squarefeet Slider -----*/
        
        let Squarefit_lower_string : String = (Temp_Dict.object(forKey: "SquareFeet_LowerSlide")!) as! String
        let Squarefit_lower_float : Float = Float(Squarefit_lower_string)!
        
        let Squarefit_upper_string : String = (Temp_Dict.object(forKey: "SquareFeet_UpperSlide")!) as! String
        let Squarefit_upper_float : Float = Float(Squarefit_upper_string)!
        
        
        print("\(Squarefit_lower_float)")
        print("\(Squarefit_upper_float)")
        
        slideSquareFit.upperValue = Squarefit_upper_float
        slideSquareFit.lowerValue = Squarefit_lower_float

        lblSquareFeet.text! = "\(Int(Squarefit_upper_float))"
        lblLowerSquareFeet.text! = "\(Int(Squarefit_lower_float))"
        
        
        /*----- Set the Value of Price Slider -----*/
        
        let Price_lower_string : String = (Temp_Dict.object(forKey: "Price_LowerSlide")!) as! String
        let Price_lower_float : Float = Float(Price_lower_string)!
        
        let Price_upper_string : String = (Temp_Dict.object(forKey: "Price_UpperSlide")!) as! String
        let Price_upper_float : Float = Float(Price_upper_string)!
        
        
        print("\(Price_lower_float)")
        print("\(Price_upper_float)")
        
        slidePrice.upperValue = Price_upper_float
        slidePrice.lowerValue = Price_lower_float

        lblPrice.text! = "\(Int(Price_upper_float))"
        lblLowerPrice.text = "\(Int(Price_lower_float))"

        
        
        

        
        

        
        
    }

    func PickerDoneSelAct() {
        
        if pickertag == 1{
            
            
            
            UIView.animate(withDuration: 0.30, animations: {
                self.ScrollViewObj.contentOffset.y = self.btnListDate.frame.origin.y - 450
                
                
                self.btnListDate.setTitle("\(self.strDate)", for: .normal)
                self.Temp_Dict.setValue("\(self.strDate)", forKey: "List_Date")
                
                self.datePickerObj.isHidden = true
                self.toolbarObj.isHidden = true

            })
            
//            self.ScrollViewObj.setContentOffset(.zero, animated: true)

            
        }
        else
        {
        
        
        picker.reloadAllComponents()
        toolbarObj.isHidden = true
        picker.isHidden = true
            
        }

    }
    
    func PickerCancelSelAct() {
        
        if pickertag == 1{
            
           
            
            
            UIView.animate(withDuration: 0.30, animations: {
                self.ScrollViewObj.contentOffset.y = self.btnListDate.frame.origin.y - 450
                
                
                self.btnListDate.setTitle("\(self.strDate)", for: .normal)
                self.datePickerObj.isHidden = true
                self.toolbarObj.isHidden = true
            })

//            self.ScrollViewObj.setContentOffset(.zero, animated: true)

            
        }
        else
        {
            
            
            picker.reloadAllComponents()
            toolbarObj.isHidden = true
            picker.isHidden = true
            
        }
        
    }

    
    func dateChanged(_ sender: UIDatePicker) {
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        
        strDate = formatter.string(from: sender.date)
        
    }
    
    func SaveSearches()
    {
        
        
//        if UserDefaults.standard.object(forKey: kSavedHomes) != nil
//        {
//            let userDefaults = Foundation.UserDefaults.standard
//            userDefaults.removeObject(forKey: kSearchDict)
//            userDefaults.set(Temp_Dict, forKey: kSearchDict)
//            userDefaults.synchronize()
//
//        }
        
    }
    
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
