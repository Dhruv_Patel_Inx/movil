//
//  SavedSearchCell.swift
//  Movil Realty
//
//  Created by BAPS on 5/1/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class SavedSearchCell: UITableViewCell {

    @IBOutlet var imgCell: UIImageView!
    @IBOutlet var lblSecond: UILabel!
    @IBOutlet var lblFirst: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
