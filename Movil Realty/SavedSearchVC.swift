//
//  SavedSearchVC.swift
//  Movil Realty
//
//  Created by BAPS on 5/1/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class SavedSearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

     var agent_search_array = NSMutableArray()
     var pro_search_array = NSMutableArray()
    
    @IBOutlet weak var txtSearchBar: UISearchBar!
    @IBOutlet weak var tblSavedSearch: UITableView!
    
    var SavedSearch_Array = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        tblSavedSearch.delegate = self
        tblSavedSearch.dataSource = self
        
        txtSearchBar.delegate = self
        
        
        if UserDefaults.standard.object(forKey: kSearchResult) != nil
        {
            let userDefaults1 = Foundation.UserDefaults.standard
            
            
            var temp_array1 = NSMutableArray()
            
            
            temp_array1 = (userDefaults1.object(forKey: kSearchResult) as! NSArray).mutableCopy() as! NSMutableArray
            /*----- adding object for appointment -----*/
            SavedSearch_Array = temp_array1.mutableCopy() as! NSMutableArray
            
            print(SavedSearch_Array)
            
        }
        
        self.searchBarCutomize()
        // Do any additional setup after loading the view.
        
        self.tblSavedSearch.tableFooterView = UIView()
        
        self.tblSavedSearch.rowHeight = UITableViewAutomaticDimension
        self.tblSavedSearch.estimatedRowHeight = 72
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    // MARK: - All Acions

    
    @IBAction func selMenuAct(_ sender: Any) {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }

    
    
    
    // MARK: - Table View Delegate Methods
    
    // Number of Section In Tableview
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 2
    
    }
    
    // Create Custom Header View
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerFrame:CGRect = tableView.frame
        
        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: headerFrame.size.width, height: headerFrame.size.height))
   
        headerView.backgroundColor = OrangeThemeColor
        
        let label = UILabel(frame: CGRect(x: 8, y: 0, width: headerFrame.size.width - 8, height: 35))
        
        if section == 0
        {
             label.text = "Agent List"
        }
        else
        {
            label.text = "Property List"
        }
        

        label.font = UIFont.optimaBold(Size: 17)
        
        label.textColor = UIColor.white
        headerView.addSubview(label)
        
        return headerView
    }
    
    // Hight of Section
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 35
    }
    
    // Number of Row in Section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {

        if section == 0
        {
            return self.agent_search_array.count
        }
        else
        {
            return self.pro_search_array.count
        }
        
    }
    // Create Cell
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath)
        -> UITableViewCell
    {
        
        let searchCell = tblSavedSearch.dequeueReusableCell(withIdentifier: "SavedSearchCell")as! SavedSearchCell
        
        if indexPath.section == 0
        {
            let currentDict = self.agent_search_array.object(at: indexPath.row) as! NSDictionary
            
            print(currentDict)
            
            let name_agent = (self.agent_search_array.object(at: indexPath.row) as! NSDictionary).object(forKey: "name")!
            
            let address_agent = (self.agent_search_array.object(at: indexPath.row) as! NSDictionary).object(forKey: "company_name")!
            
            //address
            let imagUrl = (self.agent_search_array.object(at: indexPath.row) as! NSDictionary).object(forKey: "agent_pic")!
            
            searchCell.lblFirst.text = "\(name_agent)"
        
            searchCell.lblSecond.text = "\(address_agent)"
            
            let url = URL(string: imagUrl as! String)
            let placeHolderImage = "Profile"
            let placeimage = UIImage(named: placeHolderImage)
            searchCell.imgCell.sd_setImage(with: url, placeholderImage: placeimage)
            
            
            
                searchCell.imgCell.layer.cornerRadius  = 5
                searchCell.imgCell.layer.borderWidth = 1.0
                searchCell.imgCell.layer.borderColor = OrangeThemeColor.cgColor
            searchCell.imgCell.layer.masksToBounds = true
            

            
        }
        else
        {
            let currentDict = self.pro_search_array.object(at: indexPath.row) as! NSDictionary
            
            print(currentDict)
            
             let price = currentDict.object(forKey: "L_AskingPrice")!
            
            searchCell.lblFirst.text = "$\(price)"
            
             searchCell.lblSecond.text = currentDict.object(forKey: "L_Address") as? String
            
            if currentDict.object(forKey: "path") is NSNull
            {
                let placeHolderImage = "propertyList"
                searchCell.imgCell.image = UIImage(named: placeHolderImage)
            }
            else
            {
                let imagUrlString = currentDict.object(forKey: "path") as! String
                
                
                let url = URL(string: imagUrlString)
                let placeHolderImage = "propertyList"
                let placeimage = UIImage(named: placeHolderImage)
                searchCell.imgCell.sd_setImage(with: url, placeholderImage: placeimage)
                
                
                searchCell.imgCell.layer.cornerRadius  = 5
                searchCell.imgCell.layer.borderWidth = 1.0
                searchCell.imgCell.layer.borderColor = OrangeThemeColor.cgColor
                searchCell.imgCell.layer.masksToBounds = true
                
            }

            
        }
        
        
        return searchCell
    }

    
   /* public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return SavedSearch_Array.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        var Reversed_Array = NSMutableArray()

        Reversed_Array =  NSMutableArray(array: SavedSearch_Array.reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray
        print(Reversed_Array)

        
        let savedsearch = tblSavedSearch.dequeueReusableCell(withIdentifier: "SavedSearchCell")as! SavedSearchCell
        
    
        savedsearch.textLabel?.text = "\(Reversed_Array[indexPath.row])"
        savedsearch.textLabel?.font = UIFont(name: "Optima", size: 17.0)
        
        return savedsearch
    }

    */
    
    // MARK: - Search Bar delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        txtSearchBar.resignFirstResponder()
        print(txtSearchBar.text!)
        
        
        
        SavedSearch_Array.add(txtSearchBar.text!)
        print(SavedSearch_Array)
        
        
        GetSuggestionApi()
    
   /*     let userDefaults1 = Foundation.UserDefaults.standard
        userDefaults1.removeObject(forKey: kSearchResult)
        userDefaults1.set(SavedSearch_Array, forKey: kSearchResult)
        userDefaults1.synchronize()
        print(userDefaults1.object(forKey: kSearchResult)!)
        
    */
        tblSavedSearch.reloadData()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if txtSearchBar.isFirstResponder {
            if (txtSearchBar.textInputMode?.primaryLanguage == "emoji") || !((txtSearchBar.textInputMode?.primaryLanguage) != nil) {
                return false
            }
        }
        return true
        
    }
    
    
    // MARK: - Custom Methods
    
    func searchBarCutomize()
    {
        if let textFieldInsideSearchBar = self.txtSearchBar.value(forKey: "searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView
        {
            textFieldInsideSearchBar.setValue(OrangeThemeColor, forKeyPath: "_placeholderLabel.textColor")
            
            textFieldInsideSearchBar.font = UIFont.optimaRegular(Size: 15)
            
            glassIconView.image = glassIconView.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            glassIconView.tintColor = OrangeThemeColor
            textFieldInsideSearchBar.layer.borderColor = OrangeThemeColor.cgColor
            textFieldInsideSearchBar.layer.borderWidth = 1.0
            textFieldInsideSearchBar.layer.cornerRadius = 5.0
        }
        txtSearchBar.backgroundColor = UIColor.white
        txtSearchBar.backgroundImage = UIImage()
    }
    // MARK: - Web Service Call
    
    func GetSuggestionApi()
    {

            var parameters : Any?
            SVProgressHUD.show()
            
                 parameters =
                    [GlobalMethods.METHOD_NAME: "searchagent","searchText":"\(txtSearchBar.text!)"] as [String : Any]
        
        
            let globalMethodObj = GlobalMethods()
            
            globalMethodObj.callWebService(parameter: parameters as AnyObject!) { (response, error) in
                
                
                if error != nil
                {
                    SVProgressHUD.showError(withStatus: error?.localizedDescription)
                }
                else
                {
                    
                    let dictResponse = response as! NSDictionary
                    let status = dictResponse.object(forKey: "status") as! Int
                    
                    if status != 0 {
                        
                        SVProgressHUD.dismiss()
                       
                        
                        print(response)
                        
                        self.agent_search_array.removeAllObjects()
                        self.pro_search_array.removeAllObjects()
                        
                        let dataDict  = response.object(forKey: "data") as! NSDictionary
                        if let agents = dataDict.object(forKey: "agent") as? [Any] {
                            self.agent_search_array.addObjects(from: agents)
                        }
                        
                        if let proparty = dataDict.object(forKey: "property") as? [Any] {
                            self.pro_search_array.addObjects(from: proparty)
                        }
                        
                        
                        self.tblSavedSearch.reloadData()
                    }
                    else
                    {
                        SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                    }
                    
                }
            }
    }

}
