//
//  MortageViewController.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 14/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit




class MortageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    
    @IBOutlet weak var lblPerMonth: UILabel!
    @IBOutlet var lblTotalAmount: UILabel!
    @IBOutlet var tblMortage: UITableView!
    @IBOutlet var btnMoreOption: UIButton!
    @IBOutlet var viewPrincipal: KDCircularProgress!
    @IBOutlet var viewHome: KDCircularProgress!
    @IBOutlet var viewHoaDue: KDCircularProgress!
    @IBOutlet var viewPropertyTaxes: KDCircularProgress!
    @IBOutlet weak var lblPrincipal: UILabel!
    @IBOutlet weak var lblHomeIns: UILabel!
    @IBOutlet weak var lblHoaDues: UILabel!
    @IBOutlet weak var lblPropertyTaxes: UILabel!
    

    
    
    var selectedtag = Int()
    var cellType_array = NSMutableArray()
    var rate_array = NSMutableArray()
    var Price = Float()
    var Rate = Float()
    var Down_Payment = Float()
    var Monthly_InterestRate = Float()
    var Years = Float()
    var Month_Term = Float()
    var Loan_Amount = Float()
    var interest = Float()

    var Principal = Float()
    
    
    var HOADues = NSInteger()
    var HOI = NSInteger()
    var ProTax = NSInteger()
    
    
    var PrincipalStartAngle = NSInteger()
    var HomeInsStartAngle = NSInteger()
    var HOAStartAngle = NSInteger()
    var ProTaxStartAngle = NSInteger()
    
    // MARK: - View Life Cycle Methods

    override func viewDidAppear(_ animated: Bool) {
       
        
        /*-------- Views LayOut --------*/
        
        viewPrincipal.layer.cornerRadius = viewPrincipal.frame.size.height / 2
        viewHome.layer.cornerRadius = viewPrincipal.frame.size.height / 2

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        selectedtag = 0
        
        cellType_array = ["Property Price","Down Payment","Interest Rate","Loan Term","Property Taxes","Home Owners Insurance","HOA Dues"]
        
        
    
        /*-------- Progress Indicator LayOut --------*/

        PrincipalStartAngle = (360 * PropertyMin)/PropertyMax
    
        
        viewPrincipal.startAngle = 0
        viewPrincipal.angle = Double(PrincipalStartAngle)
        viewPrincipal.trackThickness = 0.5
        viewPrincipal.progressThickness = 0.2
        viewPrincipal.clockwise = true
        viewPrincipal.gradientRotateSpeed = 2
        viewPrincipal.roundedCorners = false
        viewPrincipal.glowMode = .forward
        viewPrincipal.glowAmount = 0.9
        viewPrincipal.animate(fromAngle: 0, toAngle: Double(PrincipalStartAngle), duration: 1, completion: nil)
        

        
        HOI = 10
        HomeInsStartAngle = (360 * HOI)/HomeInsMax
        
        viewHome.startAngle = 0
        viewHome.angle = Double(HomeInsStartAngle)
        viewHome.trackThickness = 0.5
        viewHome.progressThickness = 0.2
        viewHome.clockwise = true
        viewHome.gradientRotateSpeed = 2
        viewHome.roundedCorners = false
        viewHome.glowMode = .forward
        viewHome.glowAmount = 0.9
        viewHome.animate(fromAngle: 0, toAngle: Double(HomeInsStartAngle), duration: 1, completion: nil)

        
        
        HOADues = 10
        HOAStartAngle = (360 * HOADues)/HOAMax
        
        viewHoaDue.startAngle = 0
        viewHoaDue.angle = Double(HOAStartAngle)
        viewHoaDue.trackThickness = 0.5
        viewHoaDue.progressThickness = 0.2
        viewHoaDue.clockwise = true
        viewHoaDue.gradientRotateSpeed = 2
        viewHoaDue.roundedCorners = false
        viewHoaDue.glowMode = .forward
        viewHoaDue.glowAmount = 0.9
        viewHoaDue.animate(fromAngle: 0, toAngle: Double(HOAStartAngle), duration: 1, completion: nil)

        
        
        ProTax = 100

        ProTaxStartAngle = (360 * ProTax)/ProTaxMax
        viewPropertyTaxes.startAngle = 0
        viewPropertyTaxes.angle = Double(ProTaxStartAngle)
        viewPropertyTaxes.trackThickness = 0.5
        viewPropertyTaxes.progressThickness = 0.2
        viewPropertyTaxes.clockwise = true
        viewPropertyTaxes.gradientRotateSpeed = 2
        viewPropertyTaxes.roundedCorners = false
        viewPropertyTaxes.glowMode = .forward
        viewPropertyTaxes.glowAmount = 0.9
        viewPropertyTaxes.animate(fromAngle: 0, toAngle: Double(ProTaxStartAngle), duration: 1, completion: nil)

        
        
        

        /*-------- Button LayOut --------*/

        btnMoreOption.layer.borderWidth = 1.5
        btnMoreOption.layer.cornerRadius = 2.0
        btnMoreOption.layer.borderColor = UIColor(red: 255/255, green: 102/255, blue: 30/255, alpha: 1.0).cgColor
        
        
        
        /*-------- Table View delegate --------*/

        tblMortage.delegate = self
        tblMortage.dataSource = self
        
        
        
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()
        
        
        // Do any additional setup after loading the view.
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    // MARK: - All Actions

    
    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }

    
    
    @IBAction func selMoreOptionAct(_ sender: Any) {
        
        if selectedtag == 0
        {
            selectedtag = 1
        }
        else
        {
            selectedtag = 0
        }
        
        
        tblMortage.reloadData()
        
    }
    
    
    @IBAction func selSlideAct(_ sender: UISlider) {
        

        switch sender.tag {
        case 0:
            
            let cell = tblMortage.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! MortageCell
//            let roundedValue1 = round(sender.value * 100) / 100
//            cell.lblRate.text = "$ \(roundedValue1)"
            cell.lblRate.text = "$ \(sender.value)"

            
            Price = Float(sender.value)
            if Price == 0
            {
                Price = 50000
            }
           
            
            if Rate == 0
            {
                Rate = 20
            }

            
            let Tcell = tblMortage.cellForRow(at: IndexPath(row: 1, section: 0)) as! MortageCell

            
            let roundedValue3 = round(Price * 100) / 100
            let roundedValue4 = round(Rate * 100) / 100

            let roundedValue2 = (Float((roundedValue3 * roundedValue4) / 100))
            
            
            Down_Payment = round(roundedValue2 * 100) / 100
            
            
            Tcell.lblValue.text = "$ (\(Down_Payment))"
            
            
            let loan_amount = round(Price - Down_Payment)
            Loan_Amount = round(loan_amount * 100) / 100
            print(Loan_Amount)
            
            
            
            self.Principal_Amount()
            
            break
            
        case 1:
            
            let cell = tblMortage.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! MortageCell
            
            
            let roundedValue1 = round(sender.value * 100) / 100
            cell.lblRate.text = "\(roundedValue1) %"
            Rate = Float(sender.value)
            
            if Rate == 0
            {
                Rate = 20
            }
            
            Down_Payment = (Float((Price * Rate) / 100))
            
            
            let roundedValue3 = round(Price * 100) / 100
            let roundedValue4 = round(Rate * 100) / 100
            
            let roundedValue2 = (Float((roundedValue3 * roundedValue4) / 100))
            
            
            Down_Payment = round(roundedValue2 * 100) / 100
            cell.lblValue.text = "$ (\(Down_Payment))"
            
            
            let loan_amount = round(Price - Down_Payment)
            Loan_Amount = round(loan_amount * 100) / 100
            print(Loan_Amount)

            
            self.Principal_Amount()

            
            
            break
            
            
        case 2:
            
            let cell = tblMortage.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! MortageCell
            
            let roundedValue1 = round(sender.value * 100) / 100
            cell.lblRate.text = "\(roundedValue1) %"
            
            interest = Float(sender.value)
            let roundedValue3 = round(sender.value * 100) / 100
 
            let monthly_interest_rate = (Float((roundedValue3) / 12))
            Monthly_InterestRate = round(monthly_interest_rate * 100) / 100
            print(Monthly_InterestRate)
            
            
            
            self.Principal_Amount()

        
            break
            
            
        case 3:
            
            let cell = tblMortage.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! MortageCell
            
            let roundedValue1 = round(sender.value * 100) / 100
            cell.lblRate.text = "\(roundedValue1) years"
            
            Years = Float(sender.value)
            
            
            let roundedValue3 = round(Years * 100) / 100
            let month_term = (Float((roundedValue3) * 12))
            Month_Term = round(month_term * 100) / 100
            print(Month_Term)

            
            //self.Principal_Amount()

            break
            
            
        case 4:
            
            let cell = tblMortage.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! MortageCell
            
            ProTax = NSInteger(Float(sender.value))
            
            cell.lblRate.text = "$\(ProTax)/yr"
            
          //  self.Principal_Amount()


            break
            
            
        case 5:
            
            let cell = tblMortage.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! MortageCell
            
            let roundedValue1 = round(sender.value * 100) / 100
            cell.lblRate.text = "\(roundedValue1) %"
            
           
            
            HOI = NSInteger(Float(sender.value))
            
            cell.lblRate.text = "$\(HOI)/yr"
            
         //   self.Principal_Amount()


            break
        
        case 6:
            
            let cell = tblMortage.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! MortageCell
            
           // let roundedValue1 = round(sender.value)
            
           // cell.lblRate.text = "$\(roundedValue1)/mo"
            
            
    
            HOADues = NSInteger(Float(sender.value))
            
            cell.lblRate.text = "$\(HOADues)/mo"
            
            
            
            break
            
        default:
            
            
            
            break
            

        }

        
    
    }
    
    
    
    
    @IBAction func sliderEndAct(_ sender: UISlider)
    {
    }
    
    
    
    // MARK: - Table View Delegate Methods
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if selectedtag == 0
        {
            return 3
        }
        else
        {
            return cellType_array.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let mortagecell = tblMortage.dequeueReusableCell(withIdentifier: "MortageCellVC")as! MortageCell
        
        mortagecell.lblValue.isHidden = true
        mortagecell.sliderValue.tag = indexPath.row
        
    
        if indexPath.row == 0
        {
            
            mortagecell.sliderValue.minimumValue = 50000
            mortagecell.sliderValue.maximumValue = 100000
            mortagecell.sliderValue.value = Float(Price)
            
            let roundedValue1 = round(Price * 100) / 100
            mortagecell.lblRate.text = "$ \(roundedValue1)"
        
        }
        else if indexPath.row == 1
        {
            
            mortagecell.sliderValue.minimumValue = 0
            mortagecell.sliderValue.maximumValue = 100
            mortagecell.lblValue.isHidden = false
            
            let roundedValue1 = round(Rate * 100) / 100
            mortagecell.lblRate.text = "\(roundedValue1) %"
            
            let roundedValue2 = round(Down_Payment * 100) / 100
            mortagecell.lblValue.text = "$ (\(roundedValue2))"
            
        }
        else if indexPath.row == 2
        {
            mortagecell.sliderValue.minimumValue = 0
            mortagecell.sliderValue.maximumValue = 100
            mortagecell.sliderValue.value = Float(interest)
            
            let roundedValue1 = round(interest * 100) / 100
            mortagecell.lblRate.text = "\(roundedValue1) %"
        }
        else if indexPath.row == 3
        {
            
            mortagecell.sliderValue.minimumValue = 0
            mortagecell.sliderValue.maximumValue = 100
            mortagecell.sliderValue.value = Float(Years)
            
            let roundedValue1 = round(Years * 100) / 100
            mortagecell.lblRate.text = "\(roundedValue1) years"

        }
        else if indexPath.row == 4
        {
            
            mortagecell.sliderValue.minimumValue = Float(ProTaxMin)
            mortagecell.sliderValue.maximumValue = Float(ProTaxMax)
            mortagecell.sliderValue.value = Float(ProTax)
            mortagecell.lblRate.text = "$\(ProTax)/yr"

        }
        else if indexPath.row == 5
        {
            
            mortagecell.sliderValue.minimumValue = Float(HomeInsMin)
            mortagecell.sliderValue.maximumValue = Float(HomeInsMax)
            mortagecell.sliderValue.value = Float(HOI)
            
            mortagecell.lblRate.text = "$\(HOI)/yr"

        }
        else if indexPath.row == 6
        {
            mortagecell.sliderValue.minimumValue = Float(HOAMin)
            mortagecell.sliderValue.maximumValue = Float(HOAMax)
            mortagecell.sliderValue.value = Float(HOADues)
            
            mortagecell.lblRate.text = "$\(HOADues)/mo"

        }
            
        else
        {
            
            mortagecell.sliderValue.minimumValue = Float(HOAMin)
            mortagecell.sliderValue.maximumValue = Float(HOAMax)

            
        }
        
        mortagecell.sliderValue.addTarget(self, action: #selector(sliderDidEndSliding), for: [.touchUpInside, .touchUpOutside])
        
        mortagecell.lblCellType.text = "\(cellType_array[indexPath.row])"
        return mortagecell
        
        
    }
    
    func sliderDidEndSliding(sender: UISlider)
    {
        
        if sender.tag == 0
        {
            print(" 1 end sliding")
        }
        else if sender.tag == 1
        {
            
             viewPropertyTaxes.animate(fromAngle: 0, toAngle: 360, duration: 1, completion: nil)
            print("2 end sliding")
        }
        else if sender.tag == 4
        {
            let oldAngle = ProTaxStartAngle
            
            var dues = NSInteger()
            
            dues = NSInteger(ProTax)
            
            ProTaxStartAngle = (360 * dues)/ProTaxMax
            
            viewPropertyTaxes.animate(fromAngle: Double(oldAngle), toAngle: Double(ProTaxStartAngle), duration: 1, completion: nil)
            
        }
        else if sender.tag == 5
        {
            let oldAngle = HomeInsStartAngle
            
            var dues = NSInteger()
            
            dues = NSInteger(HOI)
            
            HomeInsStartAngle = (360 * dues)/HomeInsMax
            
            viewHome.animate(fromAngle: Double(oldAngle), toAngle: Double(HomeInsStartAngle), duration: 1, completion: nil)
            
        }

        else if sender.tag == 6
        {
            let oldAngle = HOAStartAngle
            
            var dues = NSInteger()
            
            dues = NSInteger(HOADues)
            
            HOAStartAngle = (360 * dues)/HOAMax
            
            viewHoaDue.animate(fromAngle: Double(oldAngle), toAngle: Double(HOAStartAngle), duration: 1, completion: nil)
            
        }
        else
        {
            
            
            
            
            print("end sliding")
        }
        
    }
    
    // MARK: - Custom Method
    
    func Principal_Amount()
    {
        
        
        print(Loan_Amount)
        print(Monthly_InterestRate)
        print(Month_Term)

        let numerator : Float = (Loan_Amount * Monthly_InterestRate)
        print(numerator)

        
        
        let denomenator : Float = 1 - powf((1 + Monthly_InterestRate), (-Month_Term))
        print(denomenator)

        
        let payment : Float = (numerator) / (denomenator)
        print(payment)

        
        Principal = round(payment * 100) / 100
        print(Principal)
        
        
    }

    
    
    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        
        lblPrincipal.text = strPrincipalInterest
        lblHomeIns.text = strHomeIns
        lblHoaDues.text = strHOADues
        lblPropertyTaxes.text = "Property Taxes"
        lblPerMonth.text = strmo
        
        btnMoreOption.setTitle(strMoreOption, for: UIControlState.normal)
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
