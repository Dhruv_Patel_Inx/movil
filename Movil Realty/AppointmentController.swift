//
//  AppointmentController.swift
//  Movil Realty
//
//  Created by Apple on 15/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import GoogleMaps

class AppointmentController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, PlaceSearchTextFieldDelegate, UIGestureRecognizerDelegate {
    

    @IBOutlet weak var scrollviewObj: UIScrollView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet var Agenttable_Obj: UITableView!
    @IBOutlet var BookView: UIView!
    @IBOutlet var txtTime: UITextField!
    @IBOutlet var txtDate: UITextField!
    @IBOutlet var txtEndTime: UITextField!
    @IBOutlet var txtContactNumber: UITextField!
    
    @IBOutlet weak var btnContactNumber: UIButton!
    @IBOutlet weak var txtLocation: MVPlaceSearchTextField!
    
    @IBOutlet weak var lblSchedualAppointment: UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    var agentname_array = NSMutableArray()
    var strDate = String()
    var datePickerObj = UIDatePicker()
    var StarttableObj = UITableView()
    var EndtableObj = UITableView()
    let cellIdentifier_1 = "Cell"
    let cellIdentifierr_2 = "EndCell"
    var agent_appointment : String = ""
    var agentid_array = NSMutableArray()
    var agent_id : String = ""
    var Appointment_array = NSMutableArray()
    var toolbarObj = UIToolbar()
    var time_array = NSMutableArray()
    var selectedtag = Int()
    var tap = UITapGestureRecognizer()

    
    
    
    // MARK: - UIViewLife Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedtag = -1
        
        /*----- UItextfield set Padding -----*/
        let cont_padding = UIView(frame: CGRect(x: 0.0, y: 0.0, width: btnContactNumber.frame.size.width, height: txtContactNumber.frame.size.height))
        txtContactNumber.rightView = cont_padding
        txtContactNumber.rightViewMode = .always
        let Loc_padding = UIView(frame: CGRect(x: 0.0, y: 0.0, width: btnContactNumber.frame.size.width, height: txtLocation.frame.size.height))
        txtLocation.rightView = Loc_padding
        txtLocation.rightViewMode = .always
        
        
        /*----- UITextbox set layout -----*/
        txtTime.layer.borderWidth = 1.0
        txtTime.layer.borderColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0).cgColor
        
        txtDate.layer.borderWidth = 1.0
        txtDate.layer.borderColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0).cgColor
        
        btnCancel.layer.cornerRadius = btnCancel.frame.size.height/2
        
        
        /*----- MVPlacesearchTextField properties -----*/
        txtLocation.placeSearchDelegate = self
        txtLocation.strApiKey = "AIzaSyCODzz1jvUh3c8oZGSHjGhOUM8BcPs8YWo"
        txtLocation.superViewOfList = self.view;  // View, on which Autocompletion list should be appeared.
        txtLocation.autoCompleteShouldHideOnSelection = true
        txtLocation.maximumNumberOfAutoCompleteRows = 4

        
        
        /*-----  Set view object delegate -----*/
        txtDate.delegate = self
        txtTime.delegate = self
        txtEndTime.delegate = self
        txtLocation.delegate = self
        txtContactNumber.delegate = self
        
        
        Agenttable_Obj.delegate = self
        Agenttable_Obj.dataSource = self
        

        txtDate.resignFirstResponder()
        txtTime.resignFirstResponder()
        txtEndTime.resignFirstResponder()
        
        
        /*----- create toolbar  -----*/
        toolbarObj = UIToolbar.init(frame: CGRect(x: 0, y: view.frame.height - (50+200), width: view.frame.width, height: 50))
        toolbarObj.barStyle = UIBarStyle.default
        toolbarObj.isTranslucent = false
        toolbarObj.barTintColor = UIColor(red: 255/255, green: 102/255, blue: 30/255, alpha: 1.0)
        
        
        let donebutton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AppointmentController.dateDoneSelAct))
        let cancelbutton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(AppointmentController.dateCancelSelAct))
        let fixspacebutton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        
        
        donebutton.tintColor = UIColor.white
        cancelbutton.tintColor = UIColor.white
        
        
        
        toolbarObj.setItems([donebutton,fixspacebutton,cancelbutton], animated: false)
        toolbarObj.isUserInteractionEnabled = true
        donebutton.setTitleTextAttributes([NSFontAttributeName:UIFont.optimaBold(Size: 18.0),NSForegroundColorAttributeName : UIColor.white], for: .normal)
        cancelbutton.setTitleTextAttributes([NSFontAttributeName:UIFont.optimaBold(Size: 18.0),NSForegroundColorAttributeName : UIColor.white], for: .normal)
        self.view.addSubview(toolbarObj)

        
        /*----- create datepicker  -----*/
        datePickerObj = UIDatePicker.init(frame: CGRect(x: 0, y: view.frame.height - 200, width: view.frame.width, height: 200))
        datePickerObj.datePickerMode = UIDatePickerMode.date
        datePickerObj.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        datePickerObj.backgroundColor = UIColor.white
        strDate = "Thursday,March 16, 2017"
        datePickerObj.minimumDate = Date()
        self.view.addSubview(datePickerObj)
        
        /*----- create timer array  -----*/
        for hour in 1...23 {
            
            for  minute in 00...59 {
                if minute == 00 {
                    time_array.add("\(hour):\(minute)0:00")
                }
                else if minute == 15
                {
                    time_array.add("\(hour):\(minute):00")
                }
                else if minute == 30
                {
                    time_array.add("\(hour):\(minute):00")
                }
                else if minute == 45
                {
                    time_array.add("\(hour):\(minute):00")
                }
                
            }
            
        }
        
        
        
        
        /*----- add bookview  -----*/
        let w = UIScreen.main.bounds.size.width
        let h = UIScreen.main.bounds.size.height
        
        BookView.frame = CGRect(x: 12, y: 30, width: (w-24), height: (h-60))
        
        view.addSubview(BookView)
        
        
        
        
        /*----- adding start date tableview  -----*/
        StarttableObj = UITableView.init(frame: CGRect(x: (view.frame.width/2) + 10, y: scrollviewObj.frame.origin.y + 84 + 20 + (2*(txtDate.frame.size.height)), width: (view.frame.width/2) - 20, height: (h*290)/667 ))
//        StarttableObj = UITableView.init(frame: CGRect(x: (view.frame.width/2) + 10  , y: 238, width: (view.frame.width/2) - 20, height: (h*290)/667 ))
        StarttableObj.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        StarttableObj.delegate = self
        StarttableObj.dataSource = self
        StarttableObj.layer.borderWidth = 1.0
        StarttableObj.layer.borderColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0).cgColor
        self.view.addSubview(StarttableObj)
        
        /*----- adding end date tableview  -----*/
        EndtableObj = UITableView.init(frame: CGRect(x: (view.frame.width/2) + 10  , y: scrollviewObj.frame.origin.y + 84 + 40 + (3*(txtLocation.frame.size.height)), width: (view.frame.width/2) - 20, height: (h*290)/667 ))
//        EndtableObj = UITableView.init(frame: CGRect(x: (view.frame.width/2) + 10  , y: 300, width: (view.frame.width/2) - 20, height: (h*290)/667 ))
        EndtableObj.register(UITableViewCell.self, forCellReuseIdentifier: "EndCell")
        EndtableObj.delegate = self
        EndtableObj.dataSource = self
        EndtableObj.layer.borderWidth = 1.0
        EndtableObj.layer.borderColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0).cgColor
        self.view.addSubview(EndtableObj)
        
        
        
       
        
        
        
        /*----- intial to hiding view -----*/
        
        BookView.isHidden = true
        StarttableObj.isHidden = true
        EndtableObj.isHidden = true
        datePickerObj.isHidden = true
        toolbarObj.isHidden = true
        
        //----------- To Move TextField/Prevent Hidding -----------//
       /* NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)*/
        //----------- To Move TextField/Prevent Hidding -----------//
        
        
         /*----- check apointement array availibility -----*/
        if UserDefaults.standard.object(forKey: kAppointmentDic) != nil
        {
            let userDefault = Foundation.UserDefaults.standard
            
            var temp_array1 = NSMutableArray()
            

            temp_array1 = (userDefault.object(forKey: kAppointmentDic) as! NSArray).mutableCopy() as! NSMutableArray
            /*----- adding object for appointment -----*/
            Appointment_array = temp_array1.mutableCopy() as! NSMutableArray
        }
        else
        {
            /*----- create appointemnt user default -----*/
            let userDefaults = Foundation.UserDefaults.standard
            userDefaults.set(Appointment_array, forKey: kAppointmentDic)
            userDefaults.synchronize()
        }
        
        
        /*---------- Tap Gesture Recognizer  ----------*/
        
        tap = UITapGestureRecognizer(target: self, action:#selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.isEnabled = false
        tap.delegate = self
        
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()
        
        
        
       
    }
    override func viewDidAppear(_ animated: Bool) {
        txtLocation.autoCompleteRegularFontName =  "HelveticaNeue-Bold";
        txtLocation.autoCompleteBoldFontName = "HelveticaNeue";
        txtLocation.autoCompleteTableCornerRadius = 0.0
        txtLocation.autoCompleteRowHeight = 35
        txtLocation.autoCompleteTableCellTextColor = UIColor(white: 0.131, alpha: 1.000)
        txtLocation.autoCompleteFontSize = 14;
        txtLocation.autoCompleteTableBorderWidth = 1.0
        txtLocation.showTextFieldDropShadowWhenAutoCompleteTableIsOpen = true
        txtLocation.autoCompleteShouldHideOnSelection = true
        txtLocation.autoCompleteShouldHideClosingKeyboard = true
        txtLocation.autoCompleteShouldSelectOnExactMatchAutomatically = true
        txtLocation.autoCompleteTableCornerRadius = 1.0
        txtLocation.autoCompleteTableBorderColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0)
        txtLocation.autoCompleteTableFrame = CGRect(x: txtLocation.frame.origin.x, y: txtLocation.frame.origin.y + txtLocation.frame.size.height, width: txtLocation.frame.size.width, height: 50)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: - UIView Action Methods
    @IBAction func backbtn(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selDateAct(_ sender: Any) {
        
        tap.isEnabled = true

        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })

    
        
        datePickerObj.isHidden = false
        StarttableObj.isHidden = true
        EndtableObj.isHidden = true
        toolbarObj.isHidden = false
    
        
    }

    @IBAction func selTimeAct(_ sender: Any) {
        
        tap.isEnabled = false

        
        self.scrollviewObj.setContentOffset(.zero, animated: true)
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })

//        let neFrame = self.view.convert(txtTime.frame, to: txtTime.superview)
//        print(neFrame)
//        print(txtLocation.frame)
//        let h = UIScreen.main.bounds.size.height
////        txtLocation.autoCompleteTableFrame = CGRect(x: neFrame.origin.x, y: neFrame.origin.y + txtLocation.frame.size.height, width: neFrame.size.width, height: 40)
//        StarttableObj = UITableView.init(frame: CGRect(x: neFrame.origin.x  , y: neFrame.origin.y + txtDate.frame.size.height, width: neFrame.size.width, height: (h*290)/667 ))
        
        StarttableObj.isHidden = false
        EndtableObj.isHidden = true
        datePickerObj.isHidden = true
        toolbarObj.isHidden = true

    }
    
    @IBAction func selEndTimeAct(_ sender: Any) {
        self.scrollviewObj.setContentOffset(.zero, animated: true)
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        tap.isEnabled = false

        
        StarttableObj.isHidden = true
        EndtableObj.isHidden = false
        datePickerObj.isHidden = true
        toolbarObj.isHidden = true
        

    }
    @IBAction func selSubmitAct(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if (txtDate.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: msgDateRequire)
        }
        else if (txtTime.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: msgStartTimeRequire)
        }
        else if (txtEndTime.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: msgEndTimeRequire)
        }
        else if (txtContactNumber.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: msgContactNumerRequire)
        }
        else if (txtLocation.text?.characters.count)! <= 0 {
            SVProgressHUD.showInfo(withStatus: msgLocationRequire)
        }
        else if (((txtContactNumber.text?.characters.count)!) <= 9) || (((txtContactNumber.text?.characters.count)!) > 16)
        {
            SVProgressHUD.showError(withStatus: msgInvalidPhone)
        }
        else
        {
            self.getagentlistingApi()
        }
    }
    
    
    
    @IBAction func selCloseViewAct(_ sender: Any) {
        BookView.isHidden = true
    }
    @IBAction func selBookAct(_ sender: Any) {
 
        self.saveAppointmentApi()
     }
    
    
    // MARK: - Webservices Call
    func getagentlistingApi()
    {
        SVProgressHUD.show()
        
        let parameters =
            [GlobalMethods.METHOD_NAME: "getAvailableAgentList","date":"\(txtDate.text!)","start_time":"\(txtTime!)","end_time":"\(txtEndTime!)","contact_number":"\(txtContactNumber!)","location":"\(txtLocation!)"] as [String : Any]
        let globalMethodObj = GlobalMethods()
        
        globalMethodObj.callWebService(parameter: parameters as AnyObject!) { (response, error) in
            
            
            if error != nil
            {
                SVProgressHUD.showError(withStatus: error?.localizedDescription)
            }
            else
            {
                
                
                //                print("\(response)")
                let dictResponse = response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0 {
                    
                    SVProgressHUD.dismiss()
                    
                    
                    let data_array = dictResponse.object(forKey: "data") as! NSArray
                    
                    
                    //                    print("\(data_array)")
                    let i = data_array.count - 1
                    
                    for index in 0...i {
                        let data_dict = data_array.object(at: index) as! NSDictionary
                        self.agentname_array.add("\(data_dict.object(forKey: "name")!)")
                        self.agentid_array.add("\(data_dict.object(forKey: "id")!)")
                        //                        print("\(self.agentid_array)")
                        
                    }
                    self.Agenttable_Obj.reloadData()
                    self.BookView.isHidden = false
                    self.StarttableObj.isHidden = true
                    self.EndtableObj.isHidden = true
                    self.datePickerObj.isHidden = true
                    self.toolbarObj.isHidden = true
                    
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                }
                
            }
            
            
        }
        
    }

    func saveAppointmentApi()
    {
        self.view.endEditing(true)
        SVProgressHUD.show()
        
        let global = GlobalMethods()
        let param  =  [GlobalMethods.METHOD_NAME: "saveAppointment","agent_id":"\(self.agent_id)","date":"\(txtDate.text!)","start_time":"\(txtTime.text!)","end_time":"\(txtEndTime.text!)","contact_number":"\(txtContactNumber.text!)","location":"\(txtLocation.text!)"] as [String : Any]
        
        global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
            
            
            if error != nil
            {
                SVProgressHUD.showInfo(withStatus: error?.description as String!)
            }
            else
            {
                
                print("\(Response)")
                
                let dictResponse = Response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0
                {
                    self.BookView.isHidden = true
                    
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                       
                        SVProgressHUD.showInfo(withStatus: "Appointment Booked Successfully")
                    })

                    
                    let appoint_dict = dictResponse.object(forKey: "data") as! NSDictionary
                    let appointment_id = "\(appoint_dict.object(forKey: "appointment_id")!)"
//                    print("\(appointment_id)")
                    if UserDefaults.standard.object(forKey: kLoginUserDic) == nil
                    {
                        self.saveAppointmentDict(appointment_id: appointment_id)
                    }
                    

                    self.txtDate.text = ""
                    self.txtTime.text = ""
                    self.txtEndTime.text = ""
                    self.txtContactNumber.text = ""
                    self.txtLocation.text = ""
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                }
            }
        }
    }
    
    

   
    
    // MARK: - UITableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == StarttableObj {
            return time_array.count
            
        }
        else if tableView == EndtableObj{
            return time_array.count
            
        }
        else if tableView == Agenttable_Obj{
            
            return agentname_array.count
        }
        else
        {
            return 10
        }
        
        
    }
    
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == StarttableObj {
            
            let cellObj = StarttableObj.dequeueReusableCell(withIdentifier: "Cell")
            cellObj?.textLabel?.text = "\(time_array[indexPath.row])"
            cellObj?.textLabel?.font = UIFont(name: "Optima", size: 17)
            return cellObj!
            
        }
        else if tableView == EndtableObj{
            
            let cellObj = EndtableObj.dequeueReusableCell(withIdentifier: "EndCell")
            cellObj?.textLabel?.text = "\(time_array[indexPath.row])"
            cellObj?.textLabel?.font = UIFont(name: "Optima", size: 17)
            return cellObj!
            
        }
            
        else
        {
            let cellObj = Agenttable_Obj.dequeueReusableCell(withIdentifier: "AgentCell")
            cellObj?.textLabel?.text = "\(agentname_array[indexPath.row])"
            cellObj?.textLabel?.font = UIFont(name: "Optima", size: 17)
            cellObj?.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0)
            if selectedtag == indexPath.row {
                cellObj?.textLabel?.textColor = OrangeThemeColor
            }
            else{
                cellObj?.textLabel?.textColor = UIColor.black
            }
            cellObj?.selectionStyle = .none
            return cellObj!
            
        }
       
    }
     public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
     {
        let h = UIScreen.main.bounds.size.height
        if tableView == StarttableObj {
            
            return (((h*340)/667)/12)
        }
        else if tableView == EndtableObj{
            
            return (((h*340)/667)/12)
            
        }
        else
        {
             return 50
        }
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if tableView == StarttableObj{
            txtTime.text = "\(time_array[indexPath.row])"
            StarttableObj.isHidden = true
            
        }
        else if tableView == EndtableObj{
            txtEndTime.text = "\(time_array[indexPath.row])"
            EndtableObj.isHidden = true
            
        }
        else if tableView == Agenttable_Obj{
            
            agent_appointment = "\(agentname_array[indexPath.row])"
            agent_id = "\(agentid_array[indexPath.row])"
            selectedtag = indexPath.row
            Agenttable_Obj.reloadData()
//            print("\(agent_id)")
//            print("\(agent_appointment)")
        }
        
    }
    
    // MARK: - MVPlaceSearchTextField Delegate Methods
    @available(iOS 2.0, *)
    public func placeSearch(_ textField: MVPlaceSearchTextField!, resultCell cell: UITableViewCell!, with placeObject: PlaceObject!, at index: Int) {
        
    }
    
    public func placeSearchWillHideResult(_ textField: MVPlaceSearchTextField!) {
        
    }
    
    public func placeSearchWillShowResult(_ textField: MVPlaceSearchTextField!) {
        
    }
    
    public func placeSearch(_ textField: MVPlaceSearchTextField!, responseForSelectedPlace responseDict: GMSPlace!) {
        
        
    }
    

    // MARK: - UITextField Delegate Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
//        let neFrame = self.view.convert(txtLocation.frame, to: txtLocation.superview)
//        print(neFrame)
//        print(txtLocation.frame)
//        txtLocation.autoCompleteTableFrame = CGRect(x: neFrame.origin.x, y: neFrame.origin.y + txtLocation.frame.size.height, width: neFrame.size.width, height: 40)

        
        if textField == txtLocation {
            
            self.scrollviewObj.isScrollEnabled = false

            UIView.animate(withDuration: 0.30, animations: {
                self.scrollviewObj.contentOffset.y = self.scrollviewObj.frame.origin.y + 230
            })
            
            let neFrame = self.view.convert(txtLocation.frame, to: txtLocation.superview)
            print(neFrame)
            print(txtLocation.frame)
            
            print("Bound: \(scrollviewObj.bounds.origin.y)")
            print("frame: \(scrollviewObj.frame.origin.y)")
            txtLocation.autoCompleteTableFrame = CGRect(x: txtLocation.frame.origin.x, y: txtLocation.frame.origin.y - 168 , width: txtLocation.frame.size.width, height: 80)
            
            
            tap.isEnabled = true
            txtLocation.autoCompleteTableView.isHidden = false


        }
        
        else if textField == txtContactNumber{
            UIView.animate(withDuration: 0.30, animations: { 
                self.scrollviewObj.contentOffset.y = self.scrollviewObj.frame.origin.y + 100
            })
            
        }
        
        
        

        txtDate.resignFirstResponder()
        txtTime.resignFirstResponder()
        txtEndTime.resignFirstResponder()
        StarttableObj.isHidden = true
        EndtableObj.isHidden = true
        datePickerObj.isHidden = true
        toolbarObj.isHidden = true
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtLocation {
            
            self.scrollviewObj.setContentOffset(.zero, animated: true)
            
        }
        
        else if textField == txtContactNumber{
            
            self.scrollviewObj.setContentOffset(.zero, animated: true)
        }
        else{
            
        }
        txtDate.resignFirstResponder()
        txtContactNumber.resignFirstResponder()
        txtLocation.resignFirstResponder()
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == txtLocation {
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.view.endEditing(true)
            })
            self.scrollviewObj.setContentOffset(.zero, animated: true)
            self.scrollviewObj.isScrollEnabled = true
            
        }
            
        else if textField == txtContactNumber{
            DispatchQueue.main.async(execute: {() -> Void in
                self.view.endEditing(true)
            })
            self.scrollviewObj.setContentOffset(.zero, animated: true)
        }
        else{
            
        }

        txtDate.resignFirstResponder()
        txtContactNumber.resignFirstResponder()
        txtLocation.resignFirstResponder()
        return true
    }
    
    // MARK: - UIView Function
    func dateChanged(_ sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
//        formatter.dateStyle = .full
        
        
        strDate = formatter.string(from: sender.date)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        closekeyboard()
    }
    func closekeyboard() {
        self.view.endEditing(true)
    }
    
   
    func saveAppointmentDict(appointment_id: String)
    {
        
        let dict:NSDictionary = ["appointment_date":"\(txtDate.text!)","appointment_start_time":"\(txtTime.text!)","appointment_end_time":"\(txtEndTime.text!)","user_contact":"\(txtContactNumber.text!)","agent_name":"\(agent_appointment)","agent_id":"\(agent_id)","appointment_id":"\(appointment_id)","user_location":"\(txtLocation.text!)"]
        
//        appointmentDict1.setValue(txtDate.text!, forKey: "appointment_date")
//        appointmentDict1.setValue(txtTime.text!, forKey: "appointment_start_time")
//        appointmentDict1.setValue(txtEndTime.text!, forKey: "appointment_end_time")
//        appointmentDict1.setValue(txtContactNumber.text!, forKey: "user_contact")
//        appointmentDict1.setValue(txtLocation.text!, forKey: "user_location")
//        appointmentDict1.setValue(agent_appointment, forKey: "agent_name")
//        appointmentDict1.setValue(agent_id, forKey: "agent_id")
//        appointmentDict1.setValue(appointment_id, forKey: "appointment_id")
        
        print(dict)
        Appointment_array.add(dict)
        print(Appointment_array)
        
        
        //Remove object
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: kAppointmentDic)
        userDefaults.synchronize()
        
        //To save the string
        
        let userDefaults1 = UserDefaults.standard
        userDefaults1.set(Appointment_array, forKey: kAppointmentDic)
        userDefaults1.synchronize()
        print(Appointment_array)

    }

    func dateDoneSelAct() {
        txtDate.text = strDate
        datePickerObj.isHidden = true
        toolbarObj.isHidden = true
    }
    func dateCancelSelAct() {
        
        datePickerObj.isHidden = true
        toolbarObj.isHidden = true
    }
    
    
    
    
    
    
    // MARK: - Tap GestureRecognizer Delegate / Custom Methods

    
    
    func dismissKeyboard() {
        
        
        
        self.scrollviewObj.setContentOffset(.zero, animated: true)
        
        txtLocation.autoCompleteTableView.isHidden = true
        txtLocation.resignFirstResponder()
        txtContactNumber.resignFirstResponder()
        StarttableObj.isHidden = true
        EndtableObj.isHidden = true
        
        //        txtComment.isHidden = true
        //        txtComment.resignFirstResponder()
        
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if view.bounds.contains(touch.location(in: txtLocation.autoCompleteTableView)) {
            return false
        }
        else if view.bounds.contains(touch.location(in: StarttableObj)) {
            return false
        }
        return true
    }
    
    
    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        txtDate.placeholder = strDate
        txtTime.placeholder = strStartTime
        txtEndTime.placeholder = strEndTime
        txtContactNumber.placeholder = strContactNumber
        txtLocation.placeholder = strLocation
        lblSchedualAppointment.text = strSchedualAppointment
        btnSubmit.setTitle(strSubmit, for: UIControlState.normal)
    }

    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

