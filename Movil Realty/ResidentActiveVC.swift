//
//  ResidentActiveVC.swift
//  Movil Realty
//
//  Created by Apple on 17/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class ResidentActiveVC: UIViewController {
    
    

    
    
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var lblResidentialActive: UILabel!
    @IBOutlet var imgHomePic: UIImageView!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblbeds: UILabel!
    @IBOutlet var lblBath: UILabel!
    @IBOutlet var lblSQFeet: UILabel!
    @IBOutlet var lblDays: UILabel!
    @IBOutlet var btnDetail: UIButton!
    @IBOutlet var btnNeighbor: UIButton!
    @IBOutlet var btnAgentRemark: UIButton!
    @IBOutlet var btnPublicRemark: UIButton!
    @IBOutlet var viewRemarkBottom: UIView!
    @IBOutlet weak var viewPublicRemark: UIView!
    @IBOutlet var lblRemarkDetail: UILabel!
    @IBOutlet var btnshare: UIButton!
    @IBOutlet var btnChat: UIButton!
    
    
    
    
    @IBOutlet weak var LBLBeds: UILabel!
    @IBOutlet weak var LBLBath: UILabel!
    @IBOutlet weak var LBLSquareFeet: UILabel!
        @IBOutlet weak var LBLDaysOn: UILabel!
    
    
    var AgentRemark = ""
    var PublicRemark = ""
    
    var imgArray = NSMutableArray()
    var propertyDict = NSDictionary()
    
    var propertyId : String!
    var detail_tag = Int()
    
    
    var FavouriteTag = Int()
    var Property_ID = ""
    var Favourite_Array = NSMutableArray()
    
    
    
    
    //MARK:- UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        detail_tag = 0
        
        // Do any additional setup after loading the view.
        btnChat.layer.cornerRadius = 5.0
        
        self.getPropertyDetailApi()
        
        
        
        
        viewRemarkBottom.isHidden = false
        viewPublicRemark.isHidden = true
        viewRemarkBottom.backgroundColor = OrangeThemeColor
        viewPublicRemark.backgroundColor = OrangeThemeColor

        
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()

        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - UIView Action Methods
    @IBAction func btnBackClick(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    @IBAction func selFavouriteAct(_ sender: Any) {
        
        if FavouriteTag == 0 {
            
            
            FavouriteTag = 1

//            btnFavourite.setTitle("Fav", for: UIControlState.normal)
            btnFavourite.setImage(#imageLiteral(resourceName: "FavouriteLight"), for: UIControlState.normal)
            
            let userDefaults = Foundation.UserDefaults.standard
            Favourite_Array.add(propertyId)
            print(Favourite_Array)
            userDefaults.set(Favourite_Array, forKey: kMyFavouriteDic)
            userDefaults.synchronize()

            
        }
        else
        {
            
            
            FavouriteTag = 0

            let userDefaults = Foundation.UserDefaults.standard
            Favourite_Array.remove(propertyId)
            print(Favourite_Array)
            userDefaults.set(Favourite_Array, forKey: kMyFavouriteDic)
            userDefaults.synchronize()

//            btnFavourite.setTitle("UnFav", for: UIControlState.normal)
            btnFavourite.setImage(#imageLiteral(resourceName: "FavouriteDark"), for: UIControlState.normal)
        }
        
        
    }
    
    
    @IBAction func btnDetailClick(_ sender: Any) {
        
        detail_tag = 1
        
        let residentDetail = self.storyboard?.instantiateViewController(withIdentifier: "residentDetailVC") as! residentDetailVC
        
        residentDetail.property_dict = propertyDict
        residentDetail.detailview_tag = detail_tag
        self.navigationController?.pushViewController(residentDetail, animated: true)
    }

    
    @IBAction func btnNeighborClick(_ sender: Any) {
        
        detail_tag = 2

        
        let residentDetail = self.storyboard?.instantiateViewController(withIdentifier: "residentDetailVC") as! residentDetailVC
        residentDetail.property_dict = propertyDict
        residentDetail.detailview_tag = detail_tag

        
        self.navigationController?.pushViewController(residentDetail, animated: true)
        
    }
    
    
    @IBAction func btnAgentRemarkClick(_ sender: Any) {
        
        
        lblRemarkDetail.text = "\(AgentRemark)"
        
        viewRemarkBottom.isHidden = false
        viewPublicRemark.isHidden = true
        
        btnAgentRemark.setTitleColor(OrangeThemeColor, for: UIControlState.normal)
        btnPublicRemark.setTitleColor(UIColor(colorLiteralRed: 184/255, green: 184/255, blue: 184/255, alpha: 1.0), for: UIControlState.normal)
        
        
    }
    
    
    @IBAction func btnPublicRemarkClick(_ sender: Any) {
        
        
        lblRemarkDetail.text = "Public Remark Here"

        
        viewRemarkBottom.isHidden = true
        viewPublicRemark.isHidden = false

        
        btnPublicRemark.setTitleColor(OrangeThemeColor, for: UIControlState.normal)
        btnAgentRemark.setTitleColor(UIColor(colorLiteralRed: 184/255, green: 184/255, blue: 184/255, alpha: 1.0), for: UIControlState.normal)
    }
    
    
    @IBAction func btnShareClick(_ sender: Any) {
    }
    
    
    @IBAction func btnChatClick(_ sender: Any) {
    }
    
    
    
    @IBAction func imgHomeClick(_ sender: Any)
    {
        let photoVC = self.storyboard?.instantiateViewController(withIdentifier: "PhotoAlbumVC") as! PhotoAlbumVC
        
        photoVC.imgArray = imgArray
        
        self.navigationController?.pushViewController(photoVC, animated: true)
    }
    
    
    //MARK: - Web Services Call
    func getPropertyDetailApi()
    {
        
        SVProgressHUD.show()
        
        
        print(propertyId)
        let global = GlobalMethods()
        let param =  [GlobalMethods.METHOD_NAME: "getPropertyById","property_id":propertyId] as [String : Any]
        
        global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
            
            
            if error != nil
            {
                SVProgressHUD.showInfo(withStatus: error?.description as String!)
            }
            else
            {
                
                let dictResponse = Response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0
                {
                    
                    print(Response)
                    
                    SVProgressHUD.dismiss()
                   
                    self.propertyDict = dictResponse.object(forKey: "data") as! NSDictionary
                    print(self.propertyDict)
                    
//                    self.Property_ID = self.propertyDict.object(forKey: "id") as! String
//                    print(self.Property_ID)
                    
                    self.AgentRemark = self.propertyDict.object(forKey: "agent_remark") as! String
                    print(self.AgentRemark)
                    
                    
                    
                    self.setupScreen()
                    
                    self.checkForFavourite()
 
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                }
            }
        }
    }
    
    //MARK: - UIView Functions
    func setupScreen()
    {
        print(propertyDict)
        
        if let otherImageUrl = propertyDict.object(forKey: "other_image_url") as? [Any]
        {
            imgArray.addObjects(from: otherImageUrl)
        }
        
        print(imgArray)
        
        
        
        if propertyDict.object(forKey: "image_url") is NSNull
        {
            let placeHolderImage = "propertyList"
            self.imgHomePic.image = UIImage(named: placeHolderImage)
        }
        else
        {
            let imagUrlString = propertyDict.object(forKey: "image_url") as! String
            
            
            let url = URL(string: imagUrlString)
            let placeHolderImage = "propertyList"
            let placeimage = UIImage(named: placeHolderImage)
            self.imgHomePic.sd_setImage(with: url, placeholderImage: placeimage)

            
        }
        
        
        
        
        
        
        
        
        self.lblAddress.text = propertyDict.object(forKey: "address") as? String
        
      
        
        self.lblAmount.text = "$\(propertyDict.object(forKey: "price")!)"
        
         self.lblbeds.text = propertyDict.object(forKey: "bedroom") as? String
        
         self.lblBath.text = propertyDict.object(forKey: "total_bathroom") as? String
        
         self.lblSQFeet.text = propertyDict.object(forKey: "square_feet") as? String
        
    
        
        let days = propertyDict.object(forKey: "days_on")!
        
        print(days)
        
        self.lblDays.text = "\(days)"
        
        self.lblRemarkDetail.text = propertyDict.object(forKey: "agent_remark") as? String
    }
 
    
    func checkForFavourite()
    {
        /*-------- SetUp FavouriteTag --------*/
        
        
        
        
        if UserDefaults.standard.object(forKey: kMyFavouriteDic) != nil
        {
            let userDefaults = Foundation.UserDefaults.standard
            
            
            Favourite_Array = (userDefaults.object(forKey: kMyFavouriteDic) as! NSArray).mutableCopy() as! NSMutableArray
            
            if Favourite_Array.contains(propertyId) {
                
                FavouriteTag = 1
//                btnFavourite.setTitle("Fav", for: UIControlState.normal)
                btnFavourite.setImage(#imageLiteral(resourceName: "FavouriteLight"), for: UIControlState.normal)

            }
            else
            {
                
                FavouriteTag = 0
//                btnFavourite.setTitle("UnFav", for: UIControlState.normal)
                btnFavourite.setImage(#imageLiteral(resourceName: "FavouriteDark"), for: UIControlState.normal)

                
            }
            
            
        }
        else
        {
            FavouriteTag = 0
//            btnFavourite.setTitle("UnFav", for: UIControlState.normal)
            btnFavourite.setImage(#imageLiteral(resourceName: "FavouriteDark"), for: UIControlState.normal)

        }

    }
    
    
    
    
    
    
    
    
    
    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        LBLBath.text = strbath
        LBLBeds.text = strbeds
        LBLDaysOn.text = strdayson
        LBLSquareFeet.text = strfinsqft
        lblResidentialActive.text = strResidentialActive
        

        btnNeighbor.setTitle(strNEIGHBORHOOD, for: UIControlState.normal)
        btnAgentRemark.setTitle(strAGENTREMARKS, for: UIControlState.normal)
        btnChat.setTitle(strChatAboutProperty, for: UIControlState.normal)
        btnDetail.setTitle(strDETAILS, for: UIControlState.normal)
        btnPublicRemark.setTitle(strPUBLICREMARKS, for: UIControlState.normal)

    }

    
    
}
