//
//  ChatViewController.swift
//  Movil Realty
//
//  Created by Apple on 17/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import Applozic

class ChatViewController: UIViewController {

    
    
    @IBOutlet weak var lblChat: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        
        SetUpValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
    
    // MARK: - Navigation

    
    func SetUpValue()
    {
        lblChat.text = strChat
        
        let ChatManager = ALChatManager.init(applicationKey: "7c955853838cc1e67c982cf661a6653b")
        
        
        
        let alUser =  ALUser()
        alUser.userId = "101"       // NOTE : +,*,? are not allowed chars in userId.
        alUser.email = ""
        alUser.imageLink = ""    // User's profile image link.
        alUser.displayName = "Sagar"  // User's Display Name
        
        ALUserDefaultsHandler.setUserId(alUser.userId)
        ALUserDefaultsHandler.setEmailId(alUser.email)
        ALUserDefaultsHandler.setDisplayName(alUser.displayName)
        
        ChatManager?.launchChatForUser(withDisplayName: "102", withGroupId: nil, andwithDisplayName: "AA", andFrom: self)
        
        

    }
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
