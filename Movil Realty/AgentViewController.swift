//
//  AgentViewController.swift
//  Movil Realty
//
//  Created by Apple on 17/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

extension NSObject
{
    func getPropWidth(width:CGFloat) -> CGFloat
    {
        return (UIScreen.main.bounds.size.width * width) / 375
    }
    
    func getPropHeight(height:CGFloat) -> CGFloat
    {
        return (UIScreen.main.bounds.size.height * height) / 667
    }
}

class AgentViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{

    @IBOutlet var movilagentbtn: UIButton!
    
    @IBOutlet weak var lblOurAgents: UILabel!
    
    @IBOutlet var collect: UICollectionView!
    
    var AgentList_Array = NSArray()
    
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        collect.dataSource = self
        collect.delegate = self
        movilagentbtn.layer.cornerRadius = 5.0
        // Do any additional setup after loading the view.
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 25
        layout.sectionInset = UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 6)
        layout.itemSize = CGSize(width: getPropWidth(width: 111) , height: getPropHeight(height: 140))
        collect.setCollectionViewLayout(layout, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        
        
        
        movilagentbtn.isEnabled = false
        movilagentbtn.isUserInteractionEnabled = false
        
        
        /*---------- GetAgentListing Api Call ---------*/

        self.AgentListApiCall()
        
        
        
        /*---------- Call SetUpValue ---------*/
        
        self.SetUpValue()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
      
    }
    
    
    
    
    // MARK: - All Actions

    
    @IBAction func movilagent(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: kLoginUserDic) == nil
        {
            // exist
            
            let signIn = self.storyboard?.instantiateViewController(withIdentifier: "signInViewC") as! signInViewC
            self.navigationController?.pushViewController(signIn, animated: true)
            
            
        }

        
        
    }
    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
    
    
   
    
    // MARK: - Collection View Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return AgentList_Array.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let agentcell = collect.dequeueReusableCell(withReuseIdentifier: "AgentsCellVC", for: indexPath) as? AgentsCell
        let name_agent = (AgentList_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "name")!
        let imagUrl = (AgentList_Array.object(at: indexPath.row) as! NSDictionary).object(forKey: "agent_pic")!
        
        agentcell?.agentname.text = "\(name_agent)"
        
//        let url = URL(string: imagUrl as! String)
//        agentcell?.agentimage.sd_setImage(with: url)
        
        
        let url = URL(string: imagUrl as! String)
        let placeHolderImage = "Profile"
        let placeimage = UIImage(named: placeHolderImage)
        agentcell?.agentimage.sd_setImage(with: url, placeholderImage: placeimage)
        
        
        
        return agentcell!
        
    }
    
//    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        
//        let widthof = (self.collect.bounds.size.width-50)/3
//        let heightof = (self.collect.bounds.size.height-20)/3
//        return CGSize(width: widthof, height: heightof)
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dict: NSMutableDictionary = NSMutableDictionary.init()
        dict.addEntries(from: AgentList_Array.object(at: indexPath.row) as! [AnyHashable : Any])
        let story: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let agentdetail = story.instantiateViewController(withIdentifier:"AgentDetailsViewControllerVC") as! AgentDetailsViewController
        agentdetail.agentsDict = dict
        self.navigationController?.pushViewController(agentdetail, animated: true)
    }

    
    
    // MARK: - Web Services Method
    
    func AgentListApiCall()
    {
        
        
        
            
            self.view.endEditing(true)
            SVProgressHUD.show()
            
            let global = GlobalMethods()
            let param = [GlobalMethods.METHOD_NAME: "getAllAgent"] as [String : Any]
            
            
            global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
                
                
                if error != nil
                {
                    SVProgressHUD.showInfo(withStatus: error?.description as String!)
                }
                else
                {
                    SVProgressHUD.dismiss()
                    print("\(Response)")
                    
                    let dictResponse = Response as! NSDictionary
                    let status = dictResponse.object(forKey: "status") as! Int
                    
                    if status != 0
                    {
                        
                        
//                        print(Response)
                        
                        self.AgentList_Array = Response.object(forKey: "data")as! NSArray
                        
                        print("Agent Array:\(self.AgentList_Array)")
                        
                        
                        self.collect.reloadData()
                        
                    }
                    else
                    {
                        SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                    }
                
            }
            
            
        }
        
    }

    
    
    
    
    
    // MARK: - SetUpvalue Method
    
    func SetUpValue()
    {
       
        lblOurAgents.text = strOurAgents
        movilagentbtn.setTitle(strBecomeMovilAgent, for: UIControlState.normal)
        
    }
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
