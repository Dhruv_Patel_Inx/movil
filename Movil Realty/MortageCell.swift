//
//  MortageCell.swift
//  Movil Realty
//
//  Created by Apple on 18/04/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class MortageCell: UITableViewCell {

    
    
    
    @IBOutlet var lblCellType: UILabel!
    
    @IBOutlet var lblRate: UILabel!
    
    
    @IBOutlet var lblValue: UILabel!
    
    
    @IBOutlet var sliderValue: UISlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        sliderValue.setThumbImage(#imageLiteral(resourceName: "SliderMortage"), for: .normal)
        
        // Configure the view for the selected state
    }

}
