//
//  ShareMyAppController.swift
//  Movil Realty
//
//  Created by Apple on 01/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import Social
import MessageUI
import AddressBook
import MediaPlayer
import AssetsLibrary
import CoreLocation
import CoreMotion


class ShareMyAppController: UIViewController, MFMessageComposeViewControllerDelegate ,MFMailComposeViewControllerDelegate,GPPSignInDelegate{
    


    @IBOutlet weak var lblShareMyApp: UILabel!
    @IBOutlet var facebookbutton: UIButton!
    @IBOutlet var googlebutton: UIButton!
    @IBOutlet var smsbutton: UIButton!
    @IBOutlet var emailbutton: UIButton!
    @IBOutlet var twitterbutton: UIButton!
    
     var signin: GPPSignIn?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        facebookbutton.layer.cornerRadius = 5.0
        facebookbutton.layer.borderWidth = 1.0
        facebookbutton.layer.borderColor = UIColor(red: 63/255, green: 94/255, blue: 158/255, alpha: 1).cgColor
//        (red: 58, green: 91, blue: 156, alpha: 1).cgColor
        
        googlebutton.layer.cornerRadius = 5.0
        googlebutton.layer.borderWidth = 1.0
        googlebutton.layer.borderColor = UIColor(colorLiteralRed: 212/255, green: 114/255, blue: 107/255, alpha: 1).cgColor
        
        smsbutton.layer.cornerRadius = 5.0
        smsbutton.layer.borderWidth = 1.0
        smsbutton.layer.borderColor = UIColor(colorLiteralRed: 104/255, green: 104/255, blue: 104/255, alpha: 1).cgColor
        
        emailbutton.layer.cornerRadius = 5.0
        emailbutton.layer.borderWidth = 1.0
        emailbutton.layer.borderColor = UIColor(colorLiteralRed: 153/255, green: 153/255, blue: 153/255, alpha: 1).cgColor
        
        twitterbutton.layer.cornerRadius = 5.0
        twitterbutton.layer.borderWidth = 1.0
        twitterbutton.layer.borderColor = UIColor(colorLiteralRed: 102/255, green: 186/255, blue: 237/255, alpha: 1).cgColor

        // Do any additional setup after loading the view.
        
        /*--------- SetUpValue Call ---------*/

        self.SetUpValue()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClick(_ sender: UIButton)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
    @IBAction func facebooksharebtn(_ sender: Any)
    {
        if let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook) {
            vc.setInitialText("The Movil Reality is Great App To Use")
            vc.add(UIImage(named: "car")!)
            vc.add(URL(string: "https://www.hackingwithswift.com"))
            present(vc, animated: true)
        }
    }
    
    @IBAction func googlesharebtn(_ sender: Any) {
        
        signin = GPPSignIn.sharedInstance()
        signin?.shouldFetchGooglePlusUser = true
        signin?.clientID = "913325491213-50ibcdov79h39ndgtl0hrj8cbdnna3mt.apps.googleusercontent.com"
        signin?.scopes = [kGTLAuthScopePlusLogin]
        signin?.delegate = self
        signin?.authenticate()
        
    }
    //MARK:- GPP Sign in Delegate Method -
    
    func finished(withAuth auth: GTMOAuth2Authentication!, error: Error!) {
        if error == nil {
            let shareBuilder: GPPNativeShareBuilder? = GPPShare.sharedInstance().nativeShareDialog()
            shareBuilder?.setURLToShare(URL(string: "https://www.example.com/restaurant/sf/1234567/")).open()
            
        }
    }
    func finishedSharingWithError(_ error: Error?) {
        if error == nil {
            print("succsess")
        }
        else if (error as! NSError).code == kGPPErrorShareboxCanceled {
            
        }
        else {
            print(error!.localizedDescription)
        }
        
    }
    
    @IBAction func smssharebtn(_ sender: Any)
    {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "The Movil Reality is Great App To Use"
//            let phoneNumber = "7383252686"
//            controller.recipients = [phoneNumber]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func emailsharebtn(_ sender: Any)
    {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
        @IBAction func twittersharebtn(_ sender: Any)
    {
        if let vc = SLComposeViewController(forServiceType: SLServiceTypeTwitter) {
            vc.setInitialText("The Movil Reality is Great App To Use")
//            vc.add(imageView.image!)
            vc.add(UIImage(named:"car"))
            vc.add(URL(string: "http://www.photolib.noaa.gov/nssl"))
            present(vc, animated: true)
        }
    }
    
    
    /*--------- To DISSMISS THE MESSAGE SHARING ---------*/
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    /*--------- To DISSMISS THE MESSAGE SHARING ---------*/
    
    
    /*--------- FOR THE CONFIGURATION OF MAIL ---------*/
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        //        let recipient = agentsDict.object(forKey: "email")
        
        //        mailComposerVC.setToRecipients(["\(recipient)"])
        //        mailComposerVC.setSubject("Sending you an in-app e-mail...")
        mailComposerVC.setMessageBody("The Movil Reality is Great App To Use", isHTML: false)
        
        return mailComposerVC
    }
    /*--------- FOR THE CONFIGURATION OF MAIL ---------*/
    
    
    /*--------- FOR THE ERROR ALERT ---------*/
    func showSendMailErrorAlert() {
        
        let mailerroralert = UIAlertController(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", preferredStyle: UIAlertControllerStyle.alert)
        mailerroralert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(mailerroralert, animated: true, completion: nil)
        
        
        //----- DEPRECATED -----//
        /*        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
         sendMailErrorAlert.show() */
        //----- DEPRECATED -----//
    }
    /*--------- FOR THE ERROR ALERT ---------*/
    
    
    
    /*--------- MFMailComposeViewControllerDelegate Method ---------*/
    
    
    
    // MARK: - MFMailComposeViewControllerDelegate Method
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    /*--------- MFMailComposeViewControllerDelegate Method ---------*/

    
    // MARK: - SetUpvalue Method
    
    func SetUpValue()
    {
       
        
        facebookbutton.setTitle(strFacebook, for: UIControlState.normal)
        googlebutton.setTitle(strGoogle, for: UIControlState.normal)
        emailbutton.setTitle(strEmail, for: UIControlState.normal)
        twitterbutton.setTitle(strTwitter, for: UIControlState.normal)
        smsbutton.setTitle(strSMS, for: UIControlState.normal)
        lblShareMyApp.text = strShareMyApp

        
    }
    
}


