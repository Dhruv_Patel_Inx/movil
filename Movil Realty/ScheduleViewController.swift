//
//  ScheduleViewController.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 14/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import Alamofire

class ScheduleViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var btnDismissView: UIButton!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var CalenderObj: FSCalendar!
    @IBOutlet var SchedualtableObj: UITableView!
    @IBOutlet var ReminderView: UIView!
    @IBOutlet weak var RemindertableObj: UITableView!
    
    @IBOutlet weak var lblSchedual: UILabel!
    
    var agent_id : String = ""
    var agentid_array = NSMutableArray()
    var appointmentid_array = NSMutableArray()
    var appointment_id : String = ""
    var appointment_index = Int()
    var appointment_indexpath = IndexPath()
    var reminderAppoint_array = NSMutableArray()
    var date = NSDate()
    var date_array = NSMutableArray()
    var num_index = Int()
    var blurEffectView = UIVisualEffectView()
    
    
    var component_1 = DateComponents()
    
    
    var appointmentArray = NSMutableArray()
    
    
    
    // MARK: - UIView LifeCycle Methods

    override func viewDidLoad() {
        
        num_index = 0
        
        super.viewDidLoad()
        
        /*----- Reminder appointment View Layout -----*/
        
        let w = UIScreen.main.bounds.size.width
        let h = UIScreen.main.bounds.size.height
        
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        blurEffectView.isHidden = true
        
        ReminderView.frame = CGRect(x: 12, y: 30, width: (w-24), height: (h-60))
        ReminderView.backgroundColor = UIColor.clear
        view.addSubview(ReminderView)
        ReminderView.isHidden = true
        
        
        /*----- Reminder Table View Layout -----*/
        
//        RemindertableObj.frame = CGRect(x: 24, y: 60, width: (ReminderView.frame.size.width - 24), height: (ReminderView.frame.size.height - 60))
    
        /*----- btnDismissView Layout -----*/
        
        btnDismissView.layer.cornerRadius = btnDismissView.frame.size.height / 2
        
        
        /*----- CalenderObj Layout -----*/
        CalenderObj.dataSource = self
        CalenderObj.delegate = self
        CalenderObj.appearance.weekdayTextColor = OrangeThemeColor
        CalenderObj.appearance.borderRadius = 0.0
        CalenderObj.appearance.todayColor = OrangeThemeColor
        CalenderObj.appearance.headerTitleColor = OrangeThemeColor
        CalenderObj.appearance.headerTitleFont = UIFont(name: "Optima", size: 12)
        CalenderObj.appearance.headerMinimumDissolvedAlpha = 0.0
        CalenderObj.appearance.eventDefaultColor = UIColor.blue
        CalenderObj.appearance.caseOptions = [.weekdayUsesSingleUpperCase]
        CalenderObj.appearance.weekdayFont = UIFont(name: "Optima", size: 12)
        CalenderObj.appearance.subtitleFont = UIFont(name: "Optima", size: 12)
        CalenderObj.appearance.titleFont = UIFont(name: "Optima", size: 12)
        CalenderObj.appearance.selectionColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        CalenderObj.appearance.eventSelectionColor = UIColor.black
        
        /*----- UItableview Delegate -----*/
        SchedualtableObj.dataSource = self
        SchedualtableObj.delegate = self
        RemindertableObj.delegate = self
        RemindertableObj.dataSource = self
        RemindertableObj.layer.cornerRadius = 10.0
        ReminderView.layer.cornerRadius = 10.0
        
        /*----- Setting Current Date in Label -----*/
        date = Date() as NSDate
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date as Date)
        
        
        let day = components.day
        lblDate.text = "\(day!)"
        print("\(day)")
        
        /*----- Hide Table when Empty -----*/
        SchedualtableObj.tableFooterView = UIView()
        RemindertableObj.tableFooterView = UIView()
//        RemindertableObj.tableFooterView?.backgroundColor = UIColor(red: 250/255, green: 255/255, blue: 255/255, alpha: 0.7)
        
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
    
        self.reloadAllArray()
        
        if UserDefaults.standard.object(forKey: kLoginUserDic) != nil
        {
            let userDefaults = Foundation.UserDefaults.standard
            userDefaults.object(forKey: kLoginUserDic)
            print("\(userDefaults.object(forKey: kLoginUserDic))")
            let data_dict = userDefaults.object(forKey: kLoginUserDic) as! NSDictionary
            let agentid = data_dict.object(forKey: "id") as! String
            self.getAgentAppointmentapi(agentid: "\(agentid)")
            
        }
        //        agentid_array.removeAllObjects()
//        appointmentid_array.removeAllObjects()
//        
//        if UserDefaults.standard.object(forKey: kAppointmentDic) != nil
//        {
//            
//            let userDefault = Foundation.UserDefaults.standard
//            
//            var temp_array1 = NSMutableArray()
//            
//            
//            temp_array1 = (userDefault.object(forKey: kAppointmentDic) as! NSArray).mutableCopy() as! NSMutableArray
//            
//            if(temp_array1.count>0)
//            {
//                
//                appointmentArray = (userDefault.object(forKey: kAppointmentDic) as! NSArray).mutableCopy() as! NSMutableArray
//                
//                var dict = NSMutableDictionary()
//                let i: Int = temp_array1.count - 1
//                print("\(i)")
//                for index in 0...i{
//                    
//                    dict = temp_array1.object(at: index) as! NSMutableDictionary
//                    let date = dict.object(forKey: "Date")
//                    date_array.add(date!)
//                    print("\(date_array)")
//                    
//                    
//                    
//                    agentid_array.add(dict.object(forKey: "agent_id")!)
//                    appointmentid_array.add(dict.object(forKey: "appointment_id")!)
//
//                    
//                }
//                
//                
//            }
//            CalenderObj.reloadData()
//            SchedualtableObj.reloadData()
//            
//            /// date_array = temp_array1.mutableCopy() as! NSMutableArray
//        }
//        
        
    
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    // MARK: - Webservices Call
    func getAgentAppointmentapi(agentid: String){
        
        print("\(agentid)")
        self.view.endEditing(true)
        SVProgressHUD.show()
        
        let global = GlobalMethods()
        let param  =  [GlobalMethods.METHOD_NAME: "getAgentAppointment","agent_id":"\(agentid)"] as [String : Any]
        
        global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
            
            
            if error != nil
            {
                SVProgressHUD.showInfo(withStatus: error?.description as String!)
            }
            else
            {
                
                print("This is response: \(Response)")
                
                let dictResponse = Response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0
                {
                    SVProgressHUD.dismiss()
                    print("After Status:\(Response)")
                    
                    
                    let temp_array1 = NSMutableArray()
                    
                    if let check = dictResponse.object(forKey: "data") as? [Any] {
                        temp_array1.addObjects(from: check)
                    }
                    
                    print("\(temp_array1)")
                    
                    if(temp_array1.count>0)
                    {
                        let i = temp_array1.count - 1
                        
                        for index in 0...i{
                    
                        self.appointmentArray.addObjects(from: [temp_array1.object(at: index)])
                            
                        //                        testArrray = self.appointmentArray + temp_array1
                        }
                        print("FINL ARRAY: \(self.appointmentArray)")

//                       self.appointmentArray.add(temp_array1)
//                        print("/()")
//                        
//                        var dict = NSMutableDictionary()
//                        let i: Int = temp_array1.count - 1
//                        print("\(i)")
//                        for index in 0...i{
//                            
//                            dict = temp_array1.object(at: index) as! NSMutableDictionary
//                            let date = dict.object(forKey: "Date")
//                            self.date_array.add(date!)
//                            //                    print("\(date_array)")
//                            
//                            
//                            
//                            self.agentid_array.add(dict.object(forKey: "agent_id")!)
//                            self.appointmentid_array.add(dict.object(forKey: "appointment_id")!)
//                            
                        
                        //}
                        
                        
                    }
                    print("\(self.date_array)")
                    self.CalenderObj.reloadData()
                    self.SchedualtableObj.reloadData()
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                }
            }
        }
    }
    
    
    
    func deleteappointmentApiCall(index:Int){
        
        
        print("\(appointment_index)")
        //        print("\(appointmentid_array.object(at: appointment_index))")
        let dict = appointmentArray.object(at: index)as! NSDictionary
        print("\(dict.object(forKey: "appointment_id")!)")
        
        
        self.view.endEditing(true)
        SVProgressHUD.show()
        
        let global = GlobalMethods()
        let param  =  [GlobalMethods.METHOD_NAME: "deleteAgentAppointment","appointment_id":dict.object(forKey: "appointment_id")!] as [String : Any]
        
        global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
            
            if error != nil
            {
                SVProgressHUD.showInfo(withStatus: error?.description as String!)
            }
            else
            {
                
                print("\(Response)")
                
                let dictResponse = Response as! NSDictionary
                let status = dictResponse.object(forKey: "status") as! Int
                
                if status != 0
                {
                    SVProgressHUD.dismiss()
                    self.deleteAppointmentDict()
                }
                else
                {
                    SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                }
            }
        }
        
        
    }

    
    
    
    
    // MARK: - UIView Action Methods

    @IBAction func appointmentbtn(_ sender: Any) {
        
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        let appoint_obj = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentControllerVC") as! AppointmentController
        navigationController?.pushViewController(appoint_obj, animated: true)
    }
    
    @IBAction func selPrevMonthAct(_ sender: Any) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        num_index -= 1
        
        var component_1 = DateComponents()
        component_1.setValue(num_index, for: .month )
    
        let prev = Calendar.current.date(byAdding: component_1, to: date as Date)
        CalenderObj.setCurrentPage(prev!, animated: true)
        
        print("\(num_index)")
    }
    @IBAction func selNextMonthAct(_ sender: Any) {
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        num_index += 1
        print("\(num_index)")
        var component_1 = DateComponents()
        component_1.setValue(num_index, for: .month)
        let next = Calendar.current.date(byAdding: component_1, to: date as Date)
        CalenderObj.setCurrentPage(next!, animated: true)
        
        
        
        print("\(num_index)")
        
    }
    
    @IBAction func selDeleteAct(_ sender: Any) {
        print("Delete Act: \(agent_id)")
        
    }
    
    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
    
    
    
    
    
    
    @IBAction func selDissmissViewAct(_ sender: Any) {
        
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        self.ReminderView.alpha = 1
        self.blurEffectView.alpha = 1
        
        UIView.animate(withDuration: 0.9, animations: {
            self.ReminderView.alpha = 0
            self.blurEffectView.alpha = 0
        }, completion: {
            finished in
            self.ReminderView.isHidden = true
            self.blurEffectView.isHidden = true
        })
        
    }
    
    // MARK: - UITableView Delegate Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if tableView == SchedualtableObj {
            
            return appointmentArray.count
            
        }
        else{
            return reminderAppoint_array.count
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        
        
        
        if tableView == SchedualtableObj{
            
            let dict = appointmentArray.object(at: indexPath.row) as! NSDictionary
            
            let schedualcell = SchedualtableObj.dequeueReusableCell(withIdentifier: "SchedualCellVC")as! SchedualCell
            
            
            schedualcell.lblAppointment.text = ":     Booked"
            schedualcell.lblAgentname.text = ":     \(dict.object(forKey: "agent_name")!)"
            schedualcell.lblStartime.text = ":     \(dict.object(forKey: "appointment_start_time")!)"
            schedualcell.lblEndtime.text = ":     \(dict.object(forKey: "appointment_end_time")!)"
            schedualcell.lblLocation.text = ":     \(dict.object(forKey: "user_location")!)"
            //        appointmentid_array.add(dict.object(forKey: "appointment_id")!)
            print("\(appointmentid_array)")
            return schedualcell
            

            
        }
        else  {
            
            let remindercell = RemindertableObj.dequeueReusableCell(withIdentifier: "ReminderCellVC")as! ReminderCell
            
            let dict = reminderAppoint_array.object(at: indexPath.row) as! NSDictionary
            
            remindercell.lblAppStatus.text = ":     Booked"
            remindercell.lblAgentname.text = ":     \(dict.object(forKey: "agent_name")!)"
            remindercell.lblStarttime.text = ":     \(dict.object(forKey: "appointment_start_time")!)"
            remindercell.lblEndtime.text = ":     \(dict.object(forKey: "appointment_end_time")!)"
            remindercell.lblLocation.text = ":     \(dict.object(forKey: "user_location")!)"
            
            
            
            return remindercell
        }

        
        
        
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if tableView == SchedualtableObj{
            return true
        }
       return false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if tableView == SchedualtableObj {
            
            if editingStyle == .delete {
                print("Deleted")
                
                appointment_index = indexPath.row
                appointment_indexpath = indexPath
           
                self.deleteappointmentApiCall(index: indexPath.row)
                
                
            }
            
        }
        
        
    }
    
    // MARK: - FSCalender Delegate Methods
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        
        if UserDefaults.standard.object(forKey: kAppointmentDic) != nil {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let day = dateFormatter.string(from: date as Date)
            
            if date_array.contains("\(day)") {
                
                
                return 1
            }
            else
            {
                return 0
            }
            
        }
        else{
            return 0
        }
        
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let day = dateFormatter.string(from: date as Date)
        
        
        if appointmentArray.count > 0
        {
            reminderAppoint_array.removeAllObjects()

            let i = appointmentArray.count - 1
            for index in 0...i {
                
                
                let dict = appointmentArray.object(at: index) as! NSDictionary
                print("\(dict.object(forKey: "appointment_date")!)")
                let rem_date = dict.object(forKey: "appointment_date") as! String
                if rem_date.isEqual("\(day)") {
                    
                    print("\(index)")
                    
                    print("Events Found on : \(dict.object(forKey: "appointment_date")!)")
                    print("Events Found on : \(dict.object(forKey: "appointment_start_time")!)")
                    print("Events Found on : \(dict.object(forKey: "appointment_end_time")!)")
                    print("Events Found on : \(dict.object(forKey: "agent_name")!)")
                    print("Events Found on : \(dict.object(forKey: "user_location")!)")
                    
                    reminderAppoint_array.add(dict)
                    RemindertableObj.reloadData()
                    
                    
                    
                    self.ReminderView.alpha = 0
                    self.blurEffectView.alpha = 0

                    
                    UIView.animate(withDuration: 0.9, animations: {
                        self.ReminderView.alpha = 1
                        self.blurEffectView.alpha = 1
                    }, completion: {
                        finished in
                        self.ReminderView.isHidden = false
                        self.blurEffectView.isHidden = false
                    })
                    
                    
                }
                else{
                    
                    print("No Events Found")
                }
                
                
            }
            

        }
        else{
            
            print("array is nil")
            
        }
        
        print("\(reminderAppoint_array)")
        
        
        //        if date_array.contains("\(day)")  {
        //
        //            let i = appointmentArray.count - 1
        //            for index in 0...i {
        //
        //                let dict = appointmentArray.object(at: index) as! NSDictionary
        //                print("\(dict.object(forKey: "appointment_date")!)")
        //
        //
        //                if date_array.contains("\(dict.object(forKey: "appointment_date")!)") {
        //
        //                    print("Events Found on : \(dict.object(forKey: "appointment_date")!)")
        //                    print("Events Found on : \(dict.object(forKey: "appointment_start_time")!)")
        //                    print("Events Found on : \(dict.object(forKey: "appointment_end_time")!)")
        //                    print("Events Found on : \(dict.object(forKey: "agent_appointment")!)")
        //                    print("Events Found on : \(dict.object(forKey: "user_location")!)")
        //                }
        //            }
        //
        //        }
        //        else{
        //            print("No events on this date")
        //        }
        
    }

    
    
    // MARK: - UIView Function
    
    func deleteAppointmentDict(){
        
//                    self.appointmentid_array.removeObject(at: appointment_index)
                    self.appointmentArray.removeObject(at: appointment_index)
                    print("\(appointmentArray)")
                    self.SchedualtableObj.deleteRows(at: [appointment_indexpath], with: .automatic)
                    print("array:\(appointmentArray)")
        
                    let userDefaults = Foundation.UserDefaults.standard
                    userDefaults.removeObject(forKey: kAppointmentDic)
                    userDefaults.synchronize()
                    print("\(userDefaults.object(forKey: kAppointmentDic))")
        
                    let userDefaults1 = Foundation.UserDefaults.standard
                    userDefaults1.set(appointmentArray, forKey: kAppointmentDic)
                    userDefaults1.synchronize()
                    print("userdefault:\(userDefaults1.object(forKey: kAppointmentDic))")
                    print("array:\(appointmentArray)")
        
                    self.reloadAllArray()
        
//                    CalenderObj.reloadData()
        
        //            SchedualtableObj.reloadData()

        
    }
    
    func reloadAllArray(){
        
        
        appointmentArray.removeAllObjects()
        agentid_array.removeAllObjects()
        appointmentid_array.removeAllObjects()
        date_array.removeAllObjects()

        
        if UserDefaults.standard.object(forKey: kAppointmentDic) != nil
        {
            
            let userDefault = UserDefaults.standard
            
//            var temp_array1 = NSMutableArray()
            
            var temp_array1 = userDefault.object(forKey: kAppointmentDic) as! NSArray
            
            if(temp_array1.count>0)
            {
                
                for(index,element) in temp_array1.enumerated()
                {
                    let dictElement = element as! NSDictionary
                    appointmentArray.add(dictElement)
                    
                    let date = dictElement["appointment_date"] as! String
                    date_array.add(date)
                    
                    let agent_id = dictElement["appointment_date"] as! String
                    agentid_array.add(agent_id)
                    
                    let appointment_id = dictElement["appointment_id"] as! String
                    appointmentid_array.add(appointment_id)
                    
                }
                
                //                appointmentArray = (userDefault.object(forKey: kAppointmentDic) as! NSArray).mutableCopy() as! NSMutableArray
                
                /*
                 var dict = NSMutableDictionary()
                 let i: Int = temp_array1.count - 1
                 print("\(i)")
                 for index in 0...i{
                 
                 dict = temp_array1.object(at: index) as! NSMutableDictionary
                 print("aaaaaa\(dict)")
                 
                 let date = dict.object(forKey: "appointment_date")
                 date_array.add(date!)
                 //                    print("\(date_array)")
                 
                 
                 appointmentArray.add(dict)
                 agentid_array.add(dict.object(forKey: "agent_id")!)
                 appointmentid_array.add(dict.object(forKey: "appointment_id")!)
                 
                 }*/
                
            }
            
            CalenderObj.reloadData()
            SchedualtableObj.reloadData()
            
            /// date_array = temp_array1.mutableCopy() as! NSMutableArray
        }
        
        
        
        
    }

    // MARK: - SetUpValue Methods

    func SetUpValue()
    {
        lblSchedual.text = strSchedual
    }
    
    
    

    
    
   
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
