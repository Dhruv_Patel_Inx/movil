//
//  signInViewC.swift
//  Movil Realty
//
//  Created by Apple on 23/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class signInViewC: UIViewController ,UITextFieldDelegate{

    @IBOutlet var txtUserName: MRTextField!
    @IBOutlet var txtPassword: MRTextField!
    @IBOutlet var rememberMe: UISwitch!
    @IBOutlet var btnSignin: UIButton!
    @IBOutlet weak var lblRememberMe: UILabel!
    @IBOutlet weak var lblDontHaveAccount: UILabel!
    
    @IBOutlet weak var btnSignUp: UIButton!
    
        
    override func viewDidLoad() {
        super.viewDidLoad()

    
       txtUserName.delegate = self
        // Do any additional setup after loading the view.
        btnSignin.layer.cornerRadius = 5.0
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()
        
        
        
    }

    @IBAction func remembermechnaged(_ sender: Any)
    {
        
    }
    
    @IBAction func btnBackClick(_ sender: UIButton)
    {
//        let view1 = self.storyboard?.instantiateViewController(withIdentifier: "tabBarControllerVC") as! tabBarController
//        self.navigationController?.pushViewController(view1, animated: false)
        
        
        
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
        
    }
    
    @IBAction func btnSigninclick(_ sender: Any)
    {
        
        if (txtUserName.text?.characters.count)! <= 0 {
            SVProgressHUD.showError(withStatus: msgUserNameRequire)
        }
        else if (txtPassword.text?.characters.count)! <= 0{
            SVProgressHUD.showError(withStatus: msgPasswordRequire)
        }
        else
        {
            self.view.endEditing(true)
            SVProgressHUD.show()
            
            let global = GlobalMethods()
            let param =  [GlobalMethods.METHOD_NAME: "login","name":"\(txtUserName.text!)","password":"\(txtPassword.text!)"] as [String : Any]
            
            global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
                
                
                if error != nil
                {
                    SVProgressHUD.showInfo(withStatus: error?.description as String!)
                }
                else
                {
                    
                    let dictResponse = Response as! NSDictionary
                    let status = dictResponse.object(forKey: "status") as! Int
                    
                    if status == 1
                    {
                        SVProgressHUD.dismiss()
                        let dataDict = dictResponse.object(forKey: "data") as! NSDictionary
                        
                        print(dataDict)
                        
                        self.NavigationMethod()
                        
                        //To save the string
                        let userDefaults = Foundation.UserDefaults.standard
                        userDefaults.set( dataDict, forKey: kLoginUserDic)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfileDetail"), object: nil)
                        let view1 = self.storyboard?.instantiateViewController(withIdentifier: "tabBarControllerVC") as! tabBarController
                        self.navigationController?.pushViewController(view1, animated: false)
                    }
                    else
                    {
                        SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                    }
                }
            }
        }
        
    }
    
    
    
    
    //MARK: - UITextField Method -
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if txtUserName == textField {
            textField.resignFirstResponder()
            txtPassword.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
            return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if txtUserName.isFirstResponder {
            if (txtUserName.textInputMode?.primaryLanguage == "emoji") || !((txtUserName.textInputMode?.primaryLanguage) != nil) {
                return false
            }
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func selSignUpAct(_ sender: AnyObject)
    {
        let view1 = self.storyboard?.instantiateViewController(withIdentifier: "signUpViewC") as! signUpViewC
        self.navigationController?.pushViewController(view1, animated: true)
    }
    
    
    
    
    
    // MARK: - SetUpValue Method

    func SetUpValue()
    {
        lblRememberMe.text = strRemember
        lblDontHaveAccount.text = strDontHaveAccount
        txtUserName.placeholder = strPlaceholderUsername
        txtPassword.placeholder = strPlaceholderPassword
        btnSignin.setTitle(strSignIn, for: UIControlState.normal)
        btnSignUp.setTitle(strSignup, for: UIControlState.normal)
        
    }
    
    // MARK: - NavigationMethod Method

    func NavigationMethod()
    {
        
        
        
        let userDefaults1 = Foundation.UserDefaults.standard
        print(userDefaults1.object(forKey: kNavigation)!)
        
        
        
        if ((userDefaults1.object(forKey: kNavigation)!) as AnyObject).isEqual("SavedSearch")
        {
            
            
            let savedSearch = self.storyboard?.instantiateViewController(withIdentifier: "SavedSearchVC") as! SavedSearchVC
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: savedSearch, withSlideOutAnimation: false, andCompletion: nil)
            
            
        }
        else if ((userDefaults1.object(forKey: kNavigation)!) as AnyObject).isEqual("MyHome")
        {
            
            let SearchVCAgent = self.storyboard?.instantiateViewController(withIdentifier: "SearchControllerAgentVC") as! SearchControllerAgentVC
            
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: SearchVCAgent, withSlideOutAnimation: false, andCompletion: nil)
            
            
        }
        else if ((userDefaults1.object(forKey: kNavigation)!) as AnyObject).isEqual("Schedual")
        {
            
            
            let schedule = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleVC") as! ScheduleViewController
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: schedule, withSlideOutAnimation: false, andCompletion: nil)

        }
        else if ((userDefaults1.object(forKey: kNavigation)!) as AnyObject).isEqual("MyProfile")
        {
            let profile = self.storyboard?.instantiateViewController(withIdentifier: "myProfileVC") as! myProfileVC
            
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: profile, withSlideOutAnimation: false, andCompletion: nil)
        }
        else
        {
            
        }

        
        
    }
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
