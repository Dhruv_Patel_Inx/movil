//
//  SearchHomeCell.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 19/04/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class SearchHomeCell: UITableViewCell {

    @IBOutlet var lblClientName: UILabel!
   
    @IBOutlet var lblLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
