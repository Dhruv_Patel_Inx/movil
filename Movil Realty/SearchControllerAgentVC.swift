//
//  SearchControllerAgentVC.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 19/04/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class SearchControllerAgentVC: UIViewController ,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tblSearchHome: UITableView!
    
    @IBOutlet var segmentObj: UISegmentedControl!
    @IBOutlet weak var lblMyHome: UILabel!
    
    
    var arrayHome = NSMutableArray()
    var arrayAssign = NSMutableArray()
    var arrayUnAssign = NSMutableArray()
    
    // MARK: - UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblSearchHome.tableFooterView = UIView()
        
        
        self.GetSearchHomeApi()
        
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()

        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Web Service Call
    
    func GetSearchHomeApi()
    {
    
            let userDefaults = Foundation.UserDefaults.standard
            let dict = userDefaults.object(forKey: kLoginUserDic)as! NSDictionary
          //  print("DICT IS: \(dict)")
            let agent_id = "\(dict.object(forKey: "id")!)"
            print("\(agent_id)")
            
            var parameters : Any?
            
            
            SVProgressHUD.show()
        
            parameters =
                    [GlobalMethods.METHOD_NAME: "getMyHome","id": agent_id] as [String : Any]
             
        
            let globalMethodObj = GlobalMethods()
            
            globalMethodObj.callWebService(parameter: parameters as AnyObject!) { (response, error) in
                
                
                if error != nil
                {
                    SVProgressHUD.showError(withStatus: error?.localizedDescription)
                }
                else
                {
                    
                
                    let dictResponse = response as! NSDictionary
                    let status = dictResponse.object(forKey: "status") as! Int
                    
                    if status != 0 {
                        
                        SVProgressHUD.dismiss()
 
                        
                      //  print(response)
                        
                        
                      
                        
                        let dataDict  = response.object(forKey: "data") as! NSDictionary
                        
                        if let assign = dataDict.object(forKey: "agentAssignHome") as? [Any]
                        {
                            self.arrayAssign.addObjects(from: assign)
                        }
                        
                        if let unassign = dataDict.object(forKey: "unAssign") as? [Any]
                        {
                            self.arrayUnAssign.addObjects(from: unassign)
                        }
                        self.tblSearchHome.reloadData()
                        
                    }
                    else
                    {
                        SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                    }
                }
            }
    }

    // MARK: - Table View Delegate Methods
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if segmentObj.selectedSegmentIndex==0
        {
            return arrayUnAssign.count
        }
        else
        {
            return arrayAssign.count
        }

    }
    

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        var Home_dict = NSDictionary()
        
        if segmentObj.selectedSegmentIndex==0
        {
            Home_dict = arrayUnAssign.object(at: indexPath.row) as! NSDictionary
        }
        else
        {
            
            Home_dict = arrayAssign.object(at: indexPath.row) as! NSDictionary
        }
        
       // print(Home_dict)
        
        let home = tblSearchHome.dequeueReusableCell(withIdentifier: "SearchHomeCell")as! SearchHomeCell
        
    
        home.lblClientName.text = "\(Home_dict.object(forKey: "fu_name")!)"
        
        home.lblLocation.text = "\(Home_dict.object(forKey: "mh_address")!)"
        

        return home
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        var Home_dict = NSDictionary()
        
        if segmentObj.selectedSegmentIndex==0
        {
            Home_dict = arrayUnAssign.object(at: indexPath.row) as! NSDictionary
        }
        else
        {
            
            Home_dict = arrayAssign.object(at: indexPath.row) as! NSDictionary
        }
        
        
        let view1 = self.storyboard?.instantiateViewController(withIdentifier: "AgentUnassignedVC") as! AgentUnassignedVC
        view1.homeDict = Home_dict
        self.navigationController?.pushViewController(view1, animated: true)
    }
    
    // MARK: - UIView Action
    @IBAction func backSel(_ sender: Any)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
    
    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
       lblMyHome.text = strMyHome
        
        let font = UIFont.optimaRegular(Size: 15)
        
        segmentObj.setTitleTextAttributes([NSFontAttributeName:font],for: UIControlState.normal)
        
    }

    @IBAction func segmentChange(_ sender: Any)
    {
        if segmentObj.selectedSegmentIndex==0
        {
            print("1")
        }
        else
        {
            print("2")
        }
        
        tblSearchHome.reloadData()
    }
    
}
