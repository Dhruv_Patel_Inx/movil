//
//  SearchNearViewController.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 14/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation


class SearchNearViewController: UIViewController,AnnotationViewDelegate {

    @IBOutlet var gmsMapView: GMSMapView!
    
    @IBOutlet weak var lblSearch: UILabel!
    
    
    
    var locationManager:CLLocationManager!

    var arrayPlace = NSMutableArray()
    var places = [Place]()
    var arView:ARViewController!
    var distanceInMeters = CLLocationDistance()
    var Filter_Radius = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        arrayPlace.add(["name":"sola civil","lat":"23.083357","long":"72.526224"])
        arrayPlace.add(["name":"Gota","lat":"23.097735","long":"72.549124"])
        arrayPlace.add(["name":"chanakyapuri","lat":"23.078577","long":"72.535093"])
        arrayPlace.add(["name":"sciencecity","lat":"23.079635","long":"72.494763"])
        arrayPlace.add(["name":"thaltej","lat":"23.048848","long":"72.51619"])
        arrayPlace.add(["name":"kalasagar mall","lat":"23.066419","long":"72.531717"])
        
        // Do any additional setup after loading the view.
        gmsMapView.isMyLocationEnabled = true
        addMarkerstoMap()
        
        /*-------- Filter_Radius Value SetUp -------*/

        Filter_Radius = 4000
        
        
        
        /*-------- SetUpValue Call -------*/
        
        self.SetUpValue()
        
        
    }
    
    
    func addMarkerstoMap()  {
        
        
        
        
        gmsMapView.clear()
        
//        for index in 0..<arrayPlace.count
//        {
//            let marker = GMSMarker()
//            
//            let lat = (arrayPlace[index] as! NSDictionary).value(forKey: "lat")!
//            
//            let long = (arrayPlace[index] as! NSDictionary).value(forKey: "long")!
//            
//            marker.position = CLLocationCoordinate2D(latitude:  CLLocationDegrees((lat as! NSString).floatValue) ,  longitude: CLLocationDegrees((long as! NSString).floatValue))
//            
//            marker.map = gmsMapView
//            let locationobj = CLLocation(latitude: CLLocationDegrees((lat as! NSString).floatValue), longitude: CLLocationDegrees((long as! NSString).floatValue))
//
//            let place = Place(location: locationobj , reference: (arrayPlace[index] as! NSDictionary).value(forKey: "name")! as! String, name: (arrayPlace[index] as! NSDictionary).value(forKey: "name")! as! String , address: "")
//            place.tag = index
//            places.append(place)
//            
//        }
        var Position = CLLocationCoordinate2D()
        var my_Location = CLLocation()
        my_Location = CLLocation(latitude:  (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!)
        
        for index in 0..<appdel.arrayProparty.count{
            
            
            
            
            
            let lat = (appdel.arrayProparty.object(at: index) as! NSDictionary).object(forKey: "LMD_MP_Latitude")!
            
            let long = (appdel.arrayProparty.object(at: index) as! NSDictionary).object(forKey: "LMD_MP_Longitude")!
            
            
            print(lat)
            print(long)
            
            let address = (appdel.arrayProparty.object(at: index) as! NSDictionary).object(forKey: "L_Address")!
            let price = (appdel.arrayProparty.object(at: index) as! NSDictionary).object(forKey: "L_AskingPrice")!

            print(address)
            print(price)

            let Property_Location = CLLocation(latitude:  CLLocationDegrees((lat as! NSString).floatValue) ,  longitude: CLLocationDegrees((long as! NSString).floatValue))
            
            print(Property_Location)
            
            distanceInMeters = Property_Location.distance(from: my_Location)
            print(distanceInMeters)
            
            if distanceInMeters <= Filter_Radius
            {
                Position = CLLocationCoordinate2D(latitude:  CLLocationDegrees((lat as! NSString).floatValue) ,  longitude: CLLocationDegrees((long as! NSString).floatValue))
                let marker = GMSMarker(position: Position)
                
                
                marker.title = "\(price)"
                marker.snippet = "\(address)"
                
                
                let location = CLLocation(latitude: CLLocationDegrees((lat as! NSString).floatValue), longitude: CLLocationDegrees((long as! NSString).floatValue))
                let currentDict = appdel.arrayProparty.object(at: index) as! NSDictionary
                
                let place = Place(location: location, reference: currentDict.object(forKey: "L_Address") as! String, name: "$\(currentDict.object(forKey: "L_AskingPrice")!)" , address: currentDict.object(forKey: "L_NumAcres") as! String)
                place.tag = index
                
                
                marker.map = gmsMapView
                
                
                places.append(place)

            }
            
            

            
        }

        let camera = GMSCameraPosition.camera(withLatitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!, zoom: 10)
    
        self.gmsMapView.camera = camera
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    @IBAction func clickOnCamera(_ sender: AnyObject){
        arView = ARViewController()
        arView.dataSource = self
        arView.uiOptions.closeButtonEnabled = true
        arView.maxDistance = 0
        arView.maxVisibleAnnotations = 30
        arView.maxVerticalLevel = 5
        arView.headingSmoothingFactor = 0.05
        arView.trackingManager.userDistanceFilter = 25
        arView.trackingManager.reloadDistanceFilter = 75
        arView.setAnnotations(places)
        arView.uiOptions.debugEnabled = false
        self.present(arView, animated: true, completion: nil)
    }
    
    func didTouch(annotationView: AnnotationView)  {
        
    }
    
    // MARK: - SetUpValue Method

    func SetUpValue()
    {
        lblSearch.text = strSearch
    }
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension SearchNearViewController: ARDataSource {
    func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
        let annotationView = AnnotationView()
        annotationView.annotation = viewForAnnotation
        annotationView.delegate = self
        annotationView.frame = CGRect(x: 0, y: 0, width: 140, height: 62)
        
        return annotationView
    }
}


import MapKit

class MapUtil {
    
    class func translateCoordinate(coordinate: CLLocationCoordinate2D, metersLat: Double,metersLong: Double) -> (CLLocationCoordinate2D) {
        var tempCoord = coordinate
        
        let tempRegion = MKCoordinateRegionMakeWithDistance(coordinate, metersLat, metersLong)
        let tempSpan = tempRegion.span
        
        tempCoord.latitude = coordinate.latitude + tempSpan.latitudeDelta
        tempCoord.longitude = coordinate.longitude + tempSpan.longitudeDelta
        
        return tempCoord
    }
    
    class func setRadius(radius: Double,withCity city: CLLocationCoordinate2D,InMapView mapView: GMSMapView) {
        
        let range = MapUtil.translateCoordinate(coordinate: city, metersLat: radius * 2, metersLong: radius * 2)
        
        let bounds = GMSCoordinateBounds(coordinate: city, coordinate: range)
        
        let update = GMSCameraUpdate.fit(bounds, withPadding: 150.0)    // padding set to 5.0
        
        mapView.moveCamera(update)
        
        mapView.animate(toLocation: city) // animate to center
    }
}
