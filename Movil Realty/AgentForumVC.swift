 //
//  AgentForumVC.swift
//  Movil Realty
//
//  Created by BAPS on 4/11/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class AgentForumVC: UIViewController, UITableViewDelegate, UITableViewDataSource , UITextFieldDelegate, UISearchBarDelegate{
    
    
    
    
    
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var btnCreateSuggestion: UIButton!
    @IBOutlet weak var tblSuggestion: UITableView!
    @IBOutlet weak var txtSearchBar: UISearchBar!
    @IBOutlet weak var lblAgentForum: UILabel!
    @IBOutlet weak var lblPostIdea: UILabel!
    @IBOutlet weak var lblMySuggestion: UILabel!
    
    
    var suggestion_array = NSMutableArray()
    var suggestion_dict = NSDictionary()
    var selectedtag = Int()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.GetSuggestionApi()

        tblSuggestion.tableFooterView = UIView()


        /*---------- Search bar Layout & Delegate ----------*/
        
        txtSearchBar.delegate = self
        self.searchBarCutomize()
        
        
        /*---------- Table View Delegate/DataSource  ----------*/
        
        tblSuggestion.delegate = self
        tblSuggestion.dataSource = self
        

        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()

        
        
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        print("View WIll Appear")
        
        self.GetSuggestionApi()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

     // MARK: - All Actions
    
    @IBAction func selBackAct(_ sender: Any) {
        
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)

        
    }
    
    
    
    @IBAction func selCreateSuggestionAct(_ sender: Any) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "CreateSuggestionVC")
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    @IBAction func selDetailAct(_ sender: Any)
    {
        
        let data_dict = suggestion_array.object(at: (sender as AnyObject).tag) as! NSDictionary
        print((sender as AnyObject).tag)

        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AgentForumDetailVC")as! AgentForumDetailVC
        controller.SuggestionDetail_dict = data_dict
        self.navigationController?.pushViewController(controller, animated: true)

        
        
    }
    
    
    
    
    
    
     // MARK: - Table View Delegate Methods
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return suggestion_array.count
    }
    
    
   
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let suggestion_dict = suggestion_array.object(at: indexPath.row) as! NSDictionary
        
        print(suggestion_dict)
        
        let suggestion = tblSuggestion.dequeueReusableCell(withIdentifier: "SuggestionCellVC")as! SuggestionCell
        
        
        suggestion.btnDetail.tag = indexPath.row
        
        
        
        suggestion.lblSuggestion.text = "\(suggestion_dict.object(forKey: "topic")!)"
        
        if suggestion_dict.object(forKey: "like")as! Int > 0 {
            
            suggestion.lblLikeNumber.text = "\(suggestion_dict.object(forKey: "like")!)"
        }
        else{
            
            suggestion.lblLikeNumber.text = ""
            
        }
        
        return suggestion
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let data_dict = suggestion_array.object(at: indexPath.row) as! NSDictionary
        print(suggestion_dict)
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AgentForumDetailVC")as! AgentForumDetailVC
        controller.SuggestionDetail_dict = data_dict
        self.navigationController?.pushViewController(controller, animated: true)


    }


    
    

    
    
    
    // MARK: - Search Bar delegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        print("searchBarSearchButtonClicked")
        txtSearchBar.resignFirstResponder()
        self.GetSuggestionApi()

    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if txtSearchBar.isFirstResponder {
            if (txtSearchBar.textInputMode?.primaryLanguage == "emoji") || !((txtSearchBar.textInputMode?.primaryLanguage) != nil) {
                return false
            }
        }
        return true

    }
    
    
    // MARK: - Web Service Call

    func GetSuggestionApi()
    {
        if UserDefaults.standard.object(forKey: kLoginUserDic) != nil {
            
           btnCreateSuggestion.isEnabled = false
           btnClose.isEnabled = false
 
            
            
            let userDefaults = Foundation.UserDefaults.standard
            let dict = userDefaults.object(forKey: kLoginUserDic)as! NSDictionary
            print("DICT IS: \(dict)")
            let id = "\(dict.object(forKey: "id")!)"
            print("\(id)")
            
            var parameters : Any?
            
            
            SVProgressHUD.show()
            
            if txtSearchBar.text?.isEmpty == true
            {
                 parameters =
                    [GlobalMethods.METHOD_NAME: "forumListing","agent_id":"\(dict.object(forKey: "id")!)","searchText":""] as [String : Any]
            }
            else
            {
                 parameters =
                    [GlobalMethods.METHOD_NAME: "forumListing","agent_id":"\(dict.object(forKey: "id")!)","searchText":"\(txtSearchBar.text!)"] as [String : Any]
            }
            
            
            
            
            let globalMethodObj = GlobalMethods()
            
            globalMethodObj.callWebService(parameter: parameters as AnyObject!) { (response, error) in
                
                
                if error != nil
                {
                    SVProgressHUD.showError(withStatus: error?.localizedDescription)
                    
                    self.btnCreateSuggestion.isEnabled = true
                    self.btnClose.isEnabled = true
                }
                else
                {
                    
                    
                    self.btnCreateSuggestion.isEnabled = true
                    self.btnClose.isEnabled = true
                    
                    let dictResponse = response as! NSDictionary
                    let status = dictResponse.object(forKey: "status") as! Int
                    
                    if status != 0 {
                        
                        SVProgressHUD.dismiss()
                        self.btnCreateSuggestion.isEnabled = true
                        self.btnClose.isEnabled = true
                        
                        print(response)
                        
                        
                        let data_array = (response.object(forKey: "data"))as! NSArray
                        
                        
                        self.suggestion_array = data_array.mutableCopy() as! NSMutableArray
                        
                        print(self.suggestion_array)
                        
                        self.tblSuggestion.reloadData()
                        
                    }
                    else
                    {
                        SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                    }
                    
                }
                
                
            }

            
        }
        
        
        
    }
    
    
    
    
    
    // MARK: - Custom Methods

    func searchBarCutomize()
    {
        if let textFieldInsideSearchBar = self.txtSearchBar.value(forKey: "searchField") as? UITextField,
            let glassIconView = textFieldInsideSearchBar.leftView as? UIImageView {
            textFieldInsideSearchBar.setValue(OrangeThemeColor, forKeyPath: "_placeholderLabel.textColor")
            
            textFieldInsideSearchBar.font = UIFont.optimaRegular(Size: 15)
            
            glassIconView.image = glassIconView.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            glassIconView.tintColor = OrangeThemeColor
            textFieldInsideSearchBar.layer.borderColor = OrangeThemeColor.cgColor
            textFieldInsideSearchBar.layer.borderWidth = 1.0
            textFieldInsideSearchBar.layer.cornerRadius = 5.0
        }
        txtSearchBar.backgroundColor = UIColor.white
        txtSearchBar.backgroundImage = UIImage()
    }
    
    
//    func dismissKeyboard() {
//        
//        txtSearchBar.resignFirstResponder()
//        
//    }
//    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        txtSearchBar.resignFirstResponder()
    }
    
    
    
    
    
    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        lblAgentForum.text = strAgentForum_1
        lblPostIdea.text = strPostanIdea
        lblMySuggestion.text = strMySuggestionwould
        
        
    }

    
    
    
    
    
    
    
    /*
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
