//
//  signUpViewC.swift
//  Movil Realty
//
//  Created by Apple on 24/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import Alamofire

class signUpViewC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet var txtUsername: MRTextField!
    @IBOutlet var txtcompany: MRTextField!
    @IBOutlet var txtEmail: MRTextField!
    @IBOutlet var txtLicence: MRTextField!
    @IBOutlet var txtPassword: MRTextField!
    @IBOutlet var txtConfirmPass: MRTextField!
    @IBOutlet var txtphone: MRTextField!
    
    @IBOutlet weak var btnMovilAgent: UIButton!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet weak var viewCheckBox: UIView!
   
    var currentImage:UIImage!
    var movilAgenttag = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnSignUp.layer.cornerRadius = 5.0

        
        movilAgenttag = 0
        btnMovilAgent.backgroundColor = UIColor.clear
        
        btnMovilAgent.setImage(UIImage(named: "unchecked"), for: UIControlState.normal)
        
        txtcompany.isHidden = true
        txtLicence.isHidden = true
        txtphone.isHidden = true
        
        viewCheckBox.backgroundColor = UIColor.clear
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            
            
            print(self.txtUsername.frame.origin.y)

            
            
            self.txtEmail.frame = CGRect(x: self.txtcompany.frame.origin.x, y: self.txtUsername.frame.origin.y + self.txtEmail.frame.size.height + 13, width: self.txtEmail.frame.size.width, height: self.txtEmail.frame.size.height)
            
            self.txtPassword.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtEmail.frame.origin.y + self.txtEmail.frame.size.height + 13, width: self.txtPassword.frame.size.width, height: self.txtPassword.frame.size.height)
            
             self.txtConfirmPass.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtPassword.frame.origin.y + self.txtPassword.frame.size.height + 13, width: self.txtPassword.frame.size.width, height: self.txtPassword.frame.size.height)
            
            self.viewCheckBox.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtConfirmPass.frame.origin.y + self.txtConfirmPass.frame.size.height + 13, width: self.viewCheckBox.frame.size.width, height: self.viewCheckBox.frame.size.height)
            
            
            self.btnSignUp.frame = CGRect(x: self.btnSignUp.frame.origin.x  , y: self.viewCheckBox.frame.origin.y + self.viewCheckBox.frame.size.height + 13, width: self.btnSignUp.frame.size.width, height: self.btnSignUp.frame.size.height)
        
            
        })
        
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func selMovilAgentAct(_ sender: Any) {
        
        if movilAgenttag == 0
        {
            movilAgenttag = 1
            
            btnMovilAgent.setImage(UIImage(named: "checked"), for: UIControlState.normal)
        
            txtcompany.isHidden = false
            txtLicence.isHidden = false
            txtphone.isHidden = false
            
            
            DispatchQueue.main.async(execute: {() -> Void in
                
                print(self.txtUsername.frame.origin.y)
                self.txtcompany.frame = CGRect(x: self.txtcompany.frame.origin.x, y: self.txtUsername.frame.origin.y + self.txtUsername.frame.size.height + 13, width: self.txtcompany.frame.size.width, height: self.txtUsername.frame.size.height)
                
                self.txtEmail.frame = CGRect(x: self.txtcompany.frame.origin.x, y: self.txtcompany.frame.origin.y + self.txtcompany.frame.size.height + 13, width: self.txtcompany.frame.size.width, height: self.txtEmail.frame.size.height)
                
                self.txtLicence.frame = CGRect(x: self.txtcompany.frame.origin.x, y: self.txtEmail.frame.origin.y + self.txtEmail.frame.size.height + 13, width: self.txtcompany.frame.size.width, height: self.txtEmail.frame.size.height)
                
                self.txtPassword.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtLicence.frame.origin.y + self.txtEmail.frame.size.height + 13, width: self.txtPassword.frame.size.width, height: self.txtPassword.frame.size.height)
                
                self.txtConfirmPass.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtPassword.frame.origin.y + self.txtPassword.frame.size.height + 13, width: self.txtPassword.frame.size.width, height: self.txtPassword.frame.size.height)
                
                self.txtphone.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtConfirmPass.frame.origin.y + self.txtPassword.frame.size.height + 13, width: self.txtPassword.frame.size.width, height: self.txtPassword.frame.size.height)
                
                self.viewCheckBox.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtphone.frame.origin.y + self.txtConfirmPass.frame.size.height + 13, width: self.viewCheckBox.frame.size.width, height: self.viewCheckBox.frame.size.height)
                
                
                self.btnSignUp.frame = CGRect(x: self.btnSignUp.frame.origin.x  , y: self.viewCheckBox.frame.origin.y + self.viewCheckBox.frame.size.height + 13, width: self.btnSignUp.frame.size.width, height: self.btnSignUp.frame.size.height)
                
                
                
                
            })

            
            
            
            
            

        }
        else
        {
            movilAgenttag = 0
            btnMovilAgent.setImage(UIImage(named: "unchecked"), for: UIControlState.normal)

            txtcompany.isHidden = true
            txtLicence.isHidden = true
            txtphone.isHidden = true
            
            
            DispatchQueue.main.async(execute: {() -> Void in
                
                
                self.txtEmail.frame = CGRect(x: self.txtcompany.frame.origin.x, y: self.txtUsername.frame.origin.y + self.txtEmail.frame.size.height + 13, width: self.txtEmail.frame.size.width, height: self.txtEmail.frame.size.height)
                
                self.txtPassword.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtEmail.frame.origin.y + self.txtEmail.frame.size.height + 13, width: self.txtPassword.frame.size.width, height: self.txtPassword.frame.size.height)
                
                self.txtConfirmPass.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtPassword.frame.origin.y + self.txtPassword.frame.size.height + 13, width: self.txtPassword.frame.size.width, height: self.txtPassword.frame.size.height)
                
                self.viewCheckBox.frame = CGRect(x: self.txtPassword.frame.origin.x  , y: self.txtConfirmPass.frame.origin.y + self.txtConfirmPass.frame.size.height + 13, width: self.viewCheckBox.frame.size.width, height: self.viewCheckBox.frame.size.height)
                
                
                self.btnSignUp.frame = CGRect(x: self.btnSignUp.frame.origin.x  , y: self.viewCheckBox.frame.origin.y + self.viewCheckBox.frame.size.height + 13, width: self.btnSignUp.frame.size.width, height: self.btnSignUp.frame.size.height)
                
                
                
            })

            
        }
        
        
        
        
    }

    @IBAction func btnsignUpclick(_ sender: Any)
    {
        
        

        print(movilAgenttag)
        
        if movilAgenttag == 0
        {
            
            self.view.endEditing(true)
            if (txtUsername.text?.isBlank)!  {
                SVProgressHUD.showError(withStatus: msgUserNameRequire)
            }
                
            else if (txtEmail.text?.isBlank)!  {
                SVProgressHUD.showError(withStatus: msgEmaileRequire)
            }
            else if !(txtEmail.text?.isEmail)! {
                SVProgressHUD.showError(withStatus: msgInvalidEmail)
            }
            else if (txtPassword.text?.isBlank)! {
                SVProgressHUD.showError(withStatus: msgPasswordRequire)
            }
            else if (txtConfirmPass.text?.isBlank)! {
                SVProgressHUD.showError(withStatus: msgPassworConfirm)
            }
            else if (txtPassword.text! != txtConfirmPass.text!){
                SVProgressHUD.showError(withStatus: msgPasswordMatchConfirmPassword)
            }
                
            else
            {
                
                let global = GlobalMethods()
              
            
                let param =  [GlobalMethods.METHOD_NAME: "signup","name":"\(txtUsername.text!)","email":"\(txtEmail.text!)","password":"\(txtPassword.text!)","is_agent":"0"] as [String : Any]
                
                print(param)
                
                global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
                    
                    
                    if error != nil
                    {
                        SVProgressHUD.showInfo(withStatus: error?.description as String!)
                    }
                    else
                    {
                        
                        let dictResponse = Response as! NSDictionary
                        let status = dictResponse.object(forKey: "status") as! Int
                        
                        if status == 1
                        {
                            
                            let dataDict = (dictResponse as AnyObject).object(forKey: "data") as! NSDictionary
                            
                            print(dataDict)
                            //To save the string
                            let userDefaults = Foundation.UserDefaults.standard
                            
                            
                            userDefaults.set(dataDict, forKey: kLoginUserDic)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfileDetail"), object: nil)
                            let view1 = self.storyboard?.instantiateViewController(withIdentifier: "tabBarControllerVC") as! tabBarController
                            self.navigationController?.pushViewController(view1, animated: false)
                        }
                        else
                        {
                            SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                        }
                    }
                }
                
                
                
                
            }
            
        }
        else
        {
            
            
            self.view.endEditing(true)
            if (txtUsername.text?.isBlank)!  {
                SVProgressHUD.showError(withStatus: msgUserNameRequire)
            }
            else if (txtcompany.text?.isBlank)!  {
                SVProgressHUD.showError(withStatus: msgPasswordRequire)
            }
            else if (txtEmail.text?.isBlank)!  {
                SVProgressHUD.showError(withStatus: msgEmaileRequire)
            }
            else if !(txtEmail.text?.isEmail)! {
                SVProgressHUD.showError(withStatus: msgInvalidEmail)
            }
            else if (txtLicence.text?.isBlank)!  {
                SVProgressHUD.showError(withStatus: msgLiceanceNumberRequire)
            }
            else if (txtPassword.text?.isBlank)! {
                SVProgressHUD.showError(withStatus: msgPasswordRequire)
            }
            else if (txtConfirmPass.text?.isBlank)! {
                SVProgressHUD.showError(withStatus: msgPassworConfirm)
            }
            else if (txtphone.text?.isBlank)! {
                SVProgressHUD.showError(withStatus: msgPhoneNumberRequire)
            }
            else if !((txtphone.text?.characters.count)! >= 9) || !((txtphone.text?.characters.count)! < 16)
            {
                SVProgressHUD.showError(withStatus: msgInvalidPhone)
            }
            else if (txtPassword.text! != txtConfirmPass.text!)
            {
                SVProgressHUD.showError(withStatus: msgPasswordMatchConfirmPassword)
            }
            else
            {
                
                SVProgressHUD.show()
                
                
                let param =  [GlobalMethods.METHOD_NAME: "signup","name":"\(txtUsername.text!)","company_name":"\(txtcompany.text!)","email":"\(txtEmail.text!)","lic_number":"\(txtLicence.text!)","password":"\(txtPassword.text!)","phone":"\(txtphone.text!)","is_agent":"1"] as [String : Any]
                
                Alamofire.upload(multipartFormData: { (MultipartFormData) in
                    if self.currentImage != nil
                    {
                        
                        MultipartFormData.append(UIImageJPEGRepresentation(self.currentImage, 1.0)!, withName: "image", fileName: "file.png", mimeType: "image/png")
                    }
                    
                    for (key, value) in param
                    {
                        MultipartFormData.append(((value as! String).data(using: .utf8))!, withName: key)
                    }
                }, to: GlobalMethods.WEB_SERVICE_URL, encodingCompletion: { (result:SessionManager.MultipartFormDataEncodingResult) in
                    
                    switch result
                    {
                    case .success(let upload,_ , _):
                        upload.response
                            {
                                
                                [weak self] response in
                                guard self != nil else
                                {
                                    return
                                }
                                
                                do{
                                    let dict = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                                    let status = (dict as AnyObject).object(forKey: "status") as! Int
                                    
                                    if status != 0{
                                        
                            
                                        let dataDict = (dict as AnyObject).object(forKey: "data") as! NSDictionary
                                        
                                        print(dataDict)
                                        //To save the string
                                        let userDefaults = Foundation.UserDefaults.standard
                                        userDefaults.set(dataDict, forKey: kLoginUserDic)
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfileDetail"), object: nil)
                                    let view1 = self?.storyboard?.instantiateViewController(withIdentifier: "tabBarControllerVC") as! tabBarController
                                                                        self?.navigationController?.pushViewController(view1, animated: false)
                                    }
                                    else
                                    {
                                        SVProgressHUD.showInfo(withStatus: (dict as AnyObject).object(forKey: "message") as! String!)
                                    }
                                    
                                }
                                catch {
                                    
                                    let errorObj = error as NSError
                                    print("ERROR Code : ",errorObj.code)
                                    print("ERROR Code : ",errorObj.localizedDescription)
                                }
                                
                                
                        }
                    case .failure(let encodingError):
                        
                        
                        print("error:\(encodingError)")
                    }
                    
                    SVProgressHUD.dismiss()
                    print(result)
                })
                
                
                /*global.callWebService(parameter: param as AnyObject!) { (Response:AnyObject, error:NSError?) in
                 
                 SVProgressHUD.dismiss()
                 
                 if error != nil
                 {
                 
                 }
                 else
                 {
                 let dictResponse = Response as! NSDictionary
                 let status = dictResponse.object(forKey: "status") as! Int
                 
                 if status != 0{
                 
                 let dataDict = dictResponse.object(forKey: "data") as! NSDictionary
                 
                 print(dataDict)
                 //To save the string
                 let userDefaults = Foundation.UserDefaults.standard
                 userDefaults.set( dataDict, forKey: kLoginUserDic)
                 
                 let view1 = self.storyboard?.instantiateViewController(withIdentifier: "tabBarControllerVC") as! tabBarController
                 self.navigationController?.pushViewController(view1, animated: false)
                 }
                 else
                 {
                 SVProgressHUD.showInfo(withStatus: dictResponse.object(forKey: "message") as! String!)
                 }
                 }
                 }
                 */
            }
        }
    }
    
    @IBAction func btnBackClick(_ sender: UIButton)
    {
//        let view1 = self.storyboard?.instantiateViewController(withIdentifier: "tabBarControllerVC") as! tabBarController
//        self.navigationController?.pushViewController(view1, animated: false)
        self.view.endEditing(true)
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddImageClick(_ sender: UIButton)
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        
        let alert = UIAlertController(title: "Options", message: "Select any option to add image", preferredStyle: .actionSheet)
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            alert.addAction(UIAlertAction(title: "Camera roll", style: .default, handler: { (_ : UIAlertAction) in
                imagePicker.sourceType = .camera
                self.present(imagePicker, animated: true, completion: nil)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Gallary", style: .default, handler: { (_ : UIAlertAction) in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_ : UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK:- UIImagePickerController Delegate Methods -
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            currentImage = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITextField Method -
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if txtUsername == textField {
            textField.resignFirstResponder()
            txtcompany.becomeFirstResponder()
        }
        else if txtcompany == textField {
            textField.resignFirstResponder()
            txtEmail.becomeFirstResponder()
        }
        else if txtEmail == textField {
            textField.resignFirstResponder()
            txtLicence.becomeFirstResponder()
        }
        else if txtLicence == textField {
            textField.resignFirstResponder()
            txtPassword.becomeFirstResponder()
        }
        else if txtPassword == textField {
            textField.resignFirstResponder()
            txtConfirmPass.becomeFirstResponder()
        }
        else if txtConfirmPass == textField {
            textField.resignFirstResponder()
            txtphone.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        txtUsername.placeholder = strPlaceholderUsername
        txtPassword.placeholder = strPlaceholderPassword
        txtEmail.placeholder = strPlaceholderEmail
        txtphone.placeholder = strPlaceholderPhone
        txtcompany.placeholder = strPlaceholderCompany
        txtLicence.placeholder = strPlaceholderLicense
        txtConfirmPass.placeholder = strPlaceholderConfirmPassword

        btnSignUp.setTitle(strSignup, for: UIControlState.normal)
    }

    
    
    
    
    
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String {
    
    //To check text field or String is blank or not
    var isBlank: Bool {
        get {
            let trimmed = self.trimmingCharacters(in: NSCharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    //Validate Email
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: self as String, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
    
}
