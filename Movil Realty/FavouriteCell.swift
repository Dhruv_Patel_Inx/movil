//
//  FavouriteCell.swift
//  Movil Realty
//
//  Created by BAPS on 4/26/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class FavouriteCell: UITableViewCell {

    @IBOutlet var addresslabel:UILabel!
    @IBOutlet var typelabel:UILabel!
    @IBOutlet var pricelabel:UILabel!
    @IBOutlet var houseimageview:UIImageView!
    
    @IBOutlet var lblStatus: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
