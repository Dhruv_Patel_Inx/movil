//
//  residentDetailVC.swift
//  Movil Realty
//
//  Created by Apple on 17/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class residentDetailVC: UIViewController,UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet var tblDetailObj: UITableView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var btnDetail: UIButton!
    @IBOutlet var btnNeighbor: UIButton!
    @IBOutlet var lblDetail: UILabel!
   
    @IBOutlet weak var lblMapsDirection: UILabel!
    @IBOutlet var btnShare: UIButton!
    @IBOutlet var btnChat: UIButton!

    
    @IBOutlet weak var tblSchoolObj: UITableView!
    
    @IBOutlet var topDetailsView: UIView!
    
    
//    @IBOutlet weak var lblWalkScore: UILabel!
//    
//    
//    @IBOutlet weak var imgViewWalkScore: UIImageView!
//    
//    @IBOutlet weak var btnEdit: UIButton!
//    
    @IBOutlet var viewneighbour: UIView!
    
    @IBOutlet weak var viewMapView: GMSMapView!
    
    @IBOutlet weak var btnDirection: UIButton!
    @IBOutlet weak var btnStreetView: UIButton!
    
    @IBOutlet weak var btnNearby: UIButton!
    @IBOutlet weak var viewBottom: UIView!
    
//    @IBOutlet weak var lblDriving: UILabel!
//    @IBOutlet weak var lblTransist: UILabel!
//    
//    @IBOutlet weak var lblWalking: UILabel!
//    
//    @IBOutlet weak var lblCycling: UILabel!
//    
    
    
//    
//    @IBOutlet var viewImage: UIView!
//    @IBOutlet var imgPicture: UIImageView!
//    @IBOutlet var btnStreet: UIButton!
//    @IBOutlet var imgLogo: UIImageView!
//    @IBOutlet var lblScore: UILabel!
//    @IBOutlet var lblCare: UILabel!
//    @IBOutlet var lblRequrement: UILabel!
//    @IBOutlet var btnEdit: UIButton!
//    @IBOutlet var lblAddress: UILabel!
//    @IBOutlet var lblDetailAdd: UILabel!
//    
//    @IBOutlet var lblCarTime: UILabel!
//    @IBOutlet var lblBusTime: UILabel!
//    @IBOutlet var lblWalkTime: UILabel!
//    @IBOutlet var lblBycyTime: UILabel!
//    @IBOutlet var lblCopyright: UILabel!
    
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblListingInformation: UILabel!
    
    var selectedtag = Int()
    
    var property_dict = NSDictionary()
    
    var detailview_tag = Int()
    
    var panoView = GMSPanoramaView()
//    var lblMapdirection = UILabel()
    
    
    var currentLocation = CLLocation()
    var current_Location = CLLocationCoordinate2D()
    var locationManager = CLLocationManager()
    var path = GMSMutablePath()
    var marker_Position = CLLocationCoordinate2D()
    let apiURL = "https://maps.googleapis.com/maps/api/place/"
    let apiKey = "AIzaSyB9U-Ssv6A9Tt2keQtZyWMuadHoELYeGlk"
    
    var School_array = NSMutableArray()
    var School_dictionary = NSMutableDictionary()
    var time_duration = NSMutableArray()
    var places = [Place]()
    var PlaceDetail_array = NSMutableArray()
    var rectangle = GMSPolyline()
    //MARK: - UIView Life Cycle
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
//        /*-------- Walk Score Round Shape --------*/
//
//        lblWalkScore.layer.cornerRadius = lblWalkScore.frame.size.height / 2
//        lblWalkScore.clipsToBounds = true
//        
//        imgViewWalkScore.layer.cornerRadius = lblWalkScore.frame.size.height / 2
//        imgViewWalkScore.clipsToBounds = true

        
//        /*-------- Map&DirectionLabel Frame/Add/Hide--------*/
//        
//        lblMapdirection.frame = CGRect(x: 0, y: 114, width: self.view.frame.width, height: 50)
//        lblMapdirection.text = "MAP & DIRECTIONS"
//        lblMapdirection.textColor = UIColor(red: 184/255, green: 184/255, blue: 184/255, alpha: 1.0)
//        lblMapdirection.font = UIFont(name: "Optima", size: 14.0)
//        self.view.addSubview(lblMapdirection)
        
        
        /*-------- viewneighbour Frame/Add/Hide --------*/
        
        viewneighbour.frame = CGRect(x: 0, y: 64 + topDetailsView.frame.size.height + 30, width: self.view.frame.size.width, height:  self.view.frame.size.height - viewBottom.frame.size.height - 64 - topDetailsView.frame.size.height - 20 - 30)
        
        self.view.addSubview(viewneighbour)
        viewneighbour.isHidden = true
        
        
        
        
        /*-------- Setting Up view Detail / NeighbourHood --------*/
        
        if detailview_tag == 1
        {
            
            lblDescription.isHidden = false
            lblDetail.isHidden = false
            lblMapsDirection.isHidden = true

            viewneighbour.isHidden = true
            tblDetailObj.isHidden = false
            btnDetail.setTitleColor(OrangeThemeColor, for: UIControlState.normal)
            btnNeighbor.setTitleColor(UIColor(colorLiteralRed: 184/255, green: 184/255, blue: 184/255, alpha: 1.0), for: UIControlState.normal)
            
        }
        else
        {
            lblDescription.isHidden = true
            lblDetail.isHidden = true
            lblMapsDirection.isHidden = false
            
            viewneighbour.isHidden = false
            tblDetailObj.isHidden = true
            btnNeighbor.setTitleColor(OrangeThemeColor, for: UIControlState.normal)
            btnDetail.setTitleColor(UIColor(colorLiteralRed: 184/255, green: 184/255, blue: 184/255, alpha: 1.0), for: UIControlState.normal)
            
        }
        
        
//        /*-------- Button Layout --------*/
//
//        btnEdit.layer.cornerRadius = 2.0
//        btnEdit.layer.borderWidth = 1.0
//        btnEdit.layer.borderColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0).cgColor
//        
        

        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        selectedtag = 0
        
        btnChat.layer.cornerRadius = 5.0

        
        print(property_dict)
        print(property_dict.object(forKey: "lat")!)
        print(property_dict.object(forKey: "long")!)
        print(property_dict.object(forKey: "address")!)

        
        
        
        let latitude = (property_dict.object(forKey: "lat") as! NSString).doubleValue
        let longitude = (property_dict.object(forKey: "long") as! NSString).doubleValue

        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() == true {
            
            
            self.locationManager = CLLocationManager()
            locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startMonitoringSignificantLocationChanges()

            
        }
        
        
        /*-------- Map View Set --------*/

        let camera_Position = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude), zoom: 6.0)
        viewMapView.camera = camera_Position
        
        
        /*-------- Marker View Set --------*/

        marker_Position = CLLocationCoordinate2DMake(latitude, longitude)
        let marker = GMSMarker(position: marker_Position)
        marker.map = viewMapView
        
        
        /*-------- Pano View Set --------*/

        panoView = GMSPanoramaView.panorama(withFrame: viewMapView.frame,
                                                     nearCoordinate:marker_Position)

        panoView.camera = GMSPanoramaCamera(heading: 180, pitch: -10, zoom: 1)
        viewMapView.addSubview(panoView)
        panoView.isHidden = true
        
        
        
        
        /*-------- Buttton Layout Set --------*/

        tblSchoolObj.delegate = self
        tblSchoolObj.dataSource = self
        
        
//        /*-------- Buttton Layout Set --------*/
//        btnChat.layer.cornerRadius = 5.0
//        
//        btnEdit.layer.cornerRadius = 5.0;
//        btnEdit.layer.borderColor = btnEdit.titleLabel?.textColor.cgColor
//        btnEdit.layer.borderWidth = 1.0
//        
        
        /*-------- lblMapdirection Layout Set --------*/

//        lblMapdirection.isHidden = true
        
        
        
        
        
        
        
        
        
        /*-------- NearBy School Search Call --------*/
        
        self.SchoolDetailApiCall()

        
        
        
        /*-------- SetupView Call --------*/
        
        self.setupView()
        
        
        
        
        
        /*-------- SetUpValue Call --------*/
        
        self.SetUpValue()
        
        
        self.tblDetailObj.rowHeight = UITableViewAutomaticDimension;
        
        self.tblDetailObj.estimatedRowHeight = 36;
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - UITableView Delegate Methods
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if tableView == tblSchoolObj
        {
            
            return School_array.count
            
        }

        else
        {
            
            if selectedtag == 0
            {
                return 8
            }
            else
            {
                return 23
            }
            
        }
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if tableView == tblSchoolObj
        {
            let schoolcell = tblSchoolObj.dequeueReusableCell(withIdentifier: "SchoolDetailCellVC")as! SchoolDetailCell
            
            let currentDict = School_array.object(at: indexPath.row) as! NSDictionary
 
            
            if School_array.count > 0
            {
                
                schoolcell.lblSchoolTitle.text = "\(currentDict.object(forKey: "SchoolName")!)"
                schoolcell.lblSchoolSubTitle.text = "\(currentDict.object(forKey: "SchoolAddress")!)"

            }
            
            
            
            
            return schoolcell
        }
        
        else
        {
            
            let properyCell = tblDetailObj.dequeueReusableCell(withIdentifier: "propertyDetailCell")as! propertyDetailCell
            
            
            if indexPath.section == 0
            {
                
                print("My Dict \(property_dict)")
                
                if indexPath.row == 0
                {
                    properyCell.lblKey.text = "List Price:"
                    properyCell.lblValue.text = "$\(property_dict.object(forKey: "list_price")!)"
                }
                else if indexPath.row == 1
                {
                    properyCell.lblKey.text = "Original Price:"
                    properyCell.lblValue.text = "$\(property_dict.object(forKey: "original_price")!)"
                }
                else if indexPath.row == 2
                {
                    properyCell.lblKey.text = "Listing #:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "listing")!)"
                }
                else if indexPath.row == 3
                {
                    properyCell.lblKey.text = "Status:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "status")!)"
                }
                else if indexPath.row == 4
                {
                    properyCell.lblKey.text = "Category:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "category")!)"
                }
                else if indexPath.row == 5
                {
                    properyCell.lblKey.text = "Class:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "class")!)"
                }
                else if indexPath.row == 6
                {
                    properyCell.lblKey.text = "Property Type:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "property_type")!)"
                }
                else if indexPath.row == 7
                {
                    properyCell.lblKey.text = "Subdivision:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "Subdivision")!)"
                }
                else if indexPath.row == 8
                {
                    properyCell.lblKey.text = "Area Sub:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "Area_Sub")!)"
                }
                else if indexPath.row == 9
                {
                    properyCell.lblKey.text = "Total Living Area:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "Total_Living_Area")!)"
                }
                else if indexPath.row == 10
                {
                    properyCell.lblKey.text = "Bedrooms:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "bedroom")!)"
                }
                else if indexPath.row == 11
                {
                    properyCell.lblKey.text = "Full Bathrooms:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "full_bathroom")!)"
                }
                else if indexPath.row == 12
                {
                    properyCell.lblKey.text = "New Const.:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "New_Const")!)"
                }
                else if indexPath.row == 13
                {
                    properyCell.lblKey.text = "Approximate Acres:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "Approximate_Acres")!)"
                }
                else if indexPath.row == 14
                {
                    properyCell.lblKey.text = "Price:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "price")!)"
                }
                else if indexPath.row == 15
                {
                    properyCell.lblKey.text = "Sq.Feet:"
                  //  properyCell.lblValue.text = "\(property_dict.object(forKey: "square_feet")!)"
                }
                else if indexPath.row == 16
                {
                    properyCell.lblKey.text = "Total Bathrooms:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "total_bathroom")!)"
                }
                else if indexPath.row == 17
                {
                    properyCell.lblKey.text = "Half Bathrooms:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "half_bathroom")!)"
                }
                else if indexPath.row == 18
                {
                    properyCell.lblKey.text = "Status:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "status")!)"
                }
                else if indexPath.row == 19
                {
                    properyCell.lblKey.text = "Description:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "description")!)"
                }
                else if indexPath.row == 20
                {
                    properyCell.lblKey.text = "Days On:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "days_on")!)"
                }
                else if indexPath.row == 21
                {
                    properyCell.lblKey.text = "Address:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "address")!)"
                }
                else if indexPath.row == 22
                {
                    properyCell.lblKey.text = "Agent Remark:"
                    properyCell.lblValue.text = "\(property_dict.object(forKey: "agent_remark")!)"
                }
                
            }
            
            return properyCell

        }
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
     {
        
        
        if tableView == tblDetailObj
        {
            
            let  footercell = tblDetailObj.dequeueReusableCell(withIdentifier: "ResidentFooterCellVC") as! ResidentFooterCell

            
            if selectedtag == 0
            {
                
                footercell.btnPropertyDetail.setTitle(strSEEFULLDETAILS, for: UIControlState.normal)
            }
            else
            {
                footercell.btnPropertyDetail.setTitle(strHIDEFULLDETAILS, for: UIControlState.normal)
                
            }
            
            
            footercell.backgroundColor = UIColor.white
            footercell.btnPropertyDetail.isHidden = false
            footercell.btnPropertyDetail.layer.cornerRadius = 5.0
            footercell.btnPropertyDetail.layer.borderWidth = 1.0
            footercell.btnPropertyDetail.layer.borderColor = OrangeThemeColor.cgColor
            
            
            return footercell
        }
        
        else
        {
            
            return nil
            
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        
        
        if tableView == tblDetailObj
        {
         
            return 35.0
            
        }
        else
        {
            return 0.0
        }
        
        
    }

    
    
    
    
    //MARK: - UIView Action Methods
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
        
    
    @IBAction func btnDetailClick(_ sender: Any) {
        
        
//        DispatchQueue.main.async(execute: {() -> Void in
//            self.view.endEditing(true)
//        })
        
        lblMapsDirection.isHidden = true
        lblDescription.isHidden = false
        lblDetail.isHidden = false

        
        viewneighbour.isHidden = true
        tblDetailObj.isHidden = false
        
        btnDetail.setTitleColor(OrangeThemeColor, for: UIControlState.normal)
        btnNeighbor.setTitleColor(UIColor(colorLiteralRed: 184/255, green: 184/255, blue: 184/255, alpha: 1.0), for: UIControlState.normal)
        

        

    }
    
    @IBAction func btnNeighborClick(_ sender: Any) {
        
        
        
//        DispatchQueue.main.async(execute: {() -> Void in
//            self.view.endEditing(true)
//        })
        
        lblMapsDirection.isHidden = false
        lblDescription.isHidden = true
        lblDetail.isHidden = true


        viewneighbour.isHidden = false
        tblDetailObj.isHidden = true
        btnNeighbor.setTitleColor(OrangeThemeColor, for: UIControlState.normal)
        btnDetail.setTitleColor(UIColor(colorLiteralRed: 184/255, green: 184/255, blue: 184/255, alpha: 1.0), for: UIControlState.normal)


    }
    
    @IBAction func btnShareClick(_ sender: Any) {
    }
    @IBAction func btnChatClick(_ sender: Any) {
    }
    
    
    @IBAction func selPropertyDetailAct(_ sender: Any) {
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        
        
        
        
        if selectedtag == 0
        {
            selectedtag = 1
        }
        else
        {
            selectedtag = 0
        }
        
        tblDetailObj.reloadData()
        
    }

    
    
    @IBAction func selDirectionAct(_ sender: Any) {
        
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        viewMapView.clear()
        
        /*-------- Draw Path --------*/
        
        
        
        let marker = GMSMarker(position: marker_Position)
        marker.map = viewMapView
        
        viewMapView.isMyLocationEnabled = true
        path.add(marker_Position)
        path.add(current_Location)
        
        rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 2.0
        rectangle.map = viewMapView
    
        
    
    
        viewMapView.isHidden = false
        panoView.isHidden = true

    }
    
    
    @IBAction func selStreetViewAct(_ sender: Any) {
        
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.view.endEditing(true)
        })
        
        
        panoView.isHidden = false

        
    }
    
    @IBAction func selNearByAct(_ sender: Any) {
        
        
        rectangle.map = nil
        
        print("Array")
        
        panoView.isHidden = true
        print(PlaceDetail_array)
        
        
        if PlaceDetail_array.count == 0
        {
            self.SearchNearApiCall()

        }
        else
        {
            DrawMarkers()
        }
        
        
        

    }
    
    
    
    
    
    
    // MARK: - CLLocation Manager delegate Method

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    
    {
//        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
//        print(locValue)

        if locationManager.location != nil
        {
            current_Location = manager.location!.coordinate
            print(current_Location)
        }
        
        
    }

    
    
    
    // MARK: - Web Service call Method

    
    func SchoolDetailApiCall()
    {
        
        SVProgressHUD.show()
        print("Load pois")
        let latitude = (property_dict.object(forKey: "lat") as! NSString).doubleValue
        let longitude = (property_dict.object(forKey: "long") as! NSString).doubleValue
        
        
      
        
        let uri = apiURL + "nearbysearch/json?location=\(latitude),\(longitude)&radius=\(5000)&sensor=true&types=school&key=\(apiKey)"
        
        
        let url = URL(string: uri)
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let dataTask = session.dataTask(with: url!) { data, response, error in

            
            DispatchQueue.main.async(execute: {() -> Void in
                SVProgressHUD.dismiss()
            })

            if let error = error {
                print(error)
            } else if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    

                    
                    print(data!)
                    print(response!)
                    
                    
                    do {
                        let responseObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        let results = responseObject["results"] as? Array<NSDictionary>
                        
                        
                        for placeDict in results! {
                            let name = placeDict.object(forKey: "name") as! String
                            let address = placeDict.object(forKey: "vicinity") as! String
                            print(name)
                            print(address)
                            let temp_dict = NSMutableDictionary()
                            
                           temp_dict.setValue(name, forKey: "SchoolName")
                           temp_dict.setValue(address, forKey: "SchoolAddress")
                            
                            self.School_array.add(temp_dict)
                            
                            
                            DispatchQueue.main.async(execute: {() -> Void in
                                
                                self.tblSchoolObj.reloadData()
                                
                            })
                            
                            //                            let latitude = placeDict.value(forKeyPath: "geometry.location.lat") as! CLLocationDegrees
                            //                            let longitude = placeDict.value(forKeyPath: "geometry.location.lng") as! CLLocationDegrees
                            //                            let reference = placeDict.object(forKey: "reference") as! String
                            
                            //                            let location = CLLocation(latitude: latitude, longitude: longitude)
                            //                            let place = Place(location: location, reference: reference, name: name, address: address)
                            //
                            //                            self.places.append(place)
                            //                            let annotation = PlaceAnnotation(location: place.location!.coordinate, title: place.placeName)
                            
                        }
                        
                    }
                    catch _ as NSError {
                    }
                }
            }
        }
        
        dataTask.resume()
    }
    
    
    func SearchNearApiCall()
    {
        
        
        SVProgressHUD.show()

        print("Load pois")
        let latitude = (property_dict.object(forKey: "lat") as! NSString).doubleValue
        let longitude = (property_dict.object(forKey: "long") as! NSString).doubleValue
        
        
        
        
        let uri = apiURL + "nearbysearch/json?location=\(latitude),\(longitude)&radius=\(5000)&sensor=true&key=\(apiKey)"
        
        
        let url = URL(string: uri)
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let dataTask = session.dataTask(with: url!) { data, response, error in
           
            DispatchQueue.main.async(execute: {() -> Void in
                SVProgressHUD.dismiss()
            })

            
            if let error = error {
                print(error)
            } else if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    
                    
                    
                    print(data!)
                    print(response!)
                    
                    do {
                        let responseObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        let results = responseObject["results"] as? Array<NSDictionary>
                        
                        for placeDict in results! {
                            
                            self.PlaceDetail_array.add(placeDict)
                            
                        }
                       
                        self.DrawMarkers()

                        
                    }
                    catch _ as NSError {
                    }
                }
            }
        }
        
        dataTask.resume()

    }
    
    
    
    
    
    
    
    
    /* ----------------------------- API FOR DISTANCE ----------------------------------*/
//    
//    func DistanceApiCall()
//    {
//        
//        var travel_mode = ""
//        var time_taken = NSMutableString()
//        
//        print("Load pois")
//        let latitude = (property_dict.object(forKey: "lat") as! NSString).doubleValue
//        let longitude = (property_dict.object(forKey: "long") as! NSString).doubleValue
//        let cur_latitude = current_Location.latitude
//        let cur_longitude = current_Location.longitude
//
//        
//        print(40.759211)
//        print(-73.984638)
//        print(cur_latitude)
//        print(cur_longitude)
//
//        
//        
//        for i in 0...3 {
//            
//            
//            
//            if i == 0
//            {
//                travel_mode = "driving"
//            }
//            else if i == 1
//            {
//                travel_mode = "transit"
//
//            }
//            else if i == 2
//            {
//                travel_mode = "walking"
//
//            }
//            else
//            {
//                travel_mode = "bicycling"
//            }
//            
//            
//            let apiUrl_1 = "https://maps.googleapis.com/maps/api/"
//            
//            
//            let uri = apiUrl_1 + "directions/json?origin=\(40.759211),\(-73.984638)&destination=\(latitude),\(longitude)&key=AIzaSyBtW961fdR9brcU6T80jLZQ3oL2z4mrtQU&sensor=false&mode=\(travel_mode)"
//            
//            
//            
//            let url = URL(string: uri)
//            let session = URLSession(configuration: URLSessionConfiguration.default)
//            let dataTask = session.dataTask(with: url!) { data, response, error in
//                if let error = error {
//                    print(error)
//                } else if let httpResponse = response as? HTTPURLResponse {
//                    if httpResponse.statusCode == 200 {
//                        print(data!)
//                        print(response!)
//                        
//                        do {
//                            let responseObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
//                            
//                            
////                            print("responseObject:\(responseObject)")
//                            
//                            let results = responseObject["routes"] as? NSArray
//                            let datadict = results?.object(at: 0) as! NSDictionary
////                            print("datadict:\(datadict)")
//                            
//                            
//                            let legs = datadict.object(forKey: "legs")as! NSArray
////                            print("legs: \(legs)")
//                            
//                            let temp_dict = legs.object(at: 0)as! NSDictionary
////                            print("temp_dict: \(temp_dict)")
//                            
//                            
//                            let duration = temp_dict.object(forKey: "duration")as! NSDictionary
////                            print("duration: \(duration)")
//                            
//                            let time_taken = duration.object(forKey: "text")as! NSString
////                            print("time_taken: \(time_taken)!")
//
//                            self.time_duration.add(time_taken)
//                            
//                            print(self.time_duration)
////                            
////                            if i == 0
////                            {
//////                                print(time_taken)
////                                self.lblDriving.text = "\(time_taken)!"
////                            }
////                            else if i == 1
////                            {
////                                self.lblTransist.text = "\(time_taken)!"
////                                
////                            }
////                            else if i == 2
////                            {
////                                self.lblWalking.text = "\(time_taken)!"
////                                
////                            }
////                            else
////                            {
////                                self.lblCycling.text = "\(time_taken)!"
////                            }
////
////                        }
////                        catch _ as NSError {
////                        }
////                    }
////                }
////            }
////            dataTask.resume()
////            
////            
//            
//            
//            
//            
//            
//            
//        }
//        
//        
//        
//        
//        
//        
//        
//        
//        
//    
//    }
                    /* ----------------------------- API FOR DISTANCE ----------------------------------*/

//    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
//        
//        current_Location = manager.location!.coordinate
//        print(current_Location)
//    }
    
    
    
    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        titleLabel.text = strResidentialActive
        lblDescription.text = strDETAILS
        lblListingInformation.text = strdayson
        
        
        btnNeighbor.setTitle(strNEIGHBORHOOD, for: UIControlState.normal)
        btnChat.setTitle(strChatAboutProperty, for: UIControlState.normal)
        btnDetail.setTitle(strDETAILS, for: UIControlState.normal)
        
    }

    
    
    
    
    
    
    
    
    
    
    
    //MARK: - UIView Fuction Methods
    
    func setupView()
    {
        let desc = property_dict.object(forKey: "description")!
        lblDetail.text = "\(desc)"

        
        
        
        
//        lblLastprise.text = "$\(property_dict.object(forKey: "list_price")!)"
//        lblOriginalPrise.text = "$\(property_dict.object(forKey: "original_price")!)"
//        
//         let listing = property_dict.object(forKey: "listing")!
//        lblListing.text = "\(listing)"
//        
//        let status = property_dict.object(forKey: "status")!
//        lblStatus.text = "\(status)"
//        
//        let classv = property_dict.object(forKey: "class")!
//        lblCategory.text = "\(classv)"
//        
//        
//        lblClass.text = "\(classv)"
//        
//        let type = property_dict.object(forKey: "property_type")!
//        lblPropertyType.text = "\(type)"
//        
//        
//        let subDiv = property_dict.object(forKey: "Subdivision")!
//        lblSubDiv.text = "\(subDiv)"
//        
//        let subArea = property_dict.object(forKey: "Area_Sub")!
//        lblAreaSub.text = "\(subArea)"
//        
//        let totalArea = property_dict.object(forKey: "Total_Living_Area")!
//        lblTlivArea.text = "\(totalArea)"
//        
//        let bed = property_dict.object(forKey: "bedroom")!
//        lblBedroom.text = "\(bed)"
//        
//        let fullBath = property_dict.object(forKey: "full_bathroom")!
//        lblBathroom.text = "\(fullBath)"
//        
//        
//        let newCons = property_dict.object(forKey: "New_Const")!
//        lblNewCon.text = "\(newCons)"
//        
//        let acers = property_dict.object(forKey: "Approximate_Acres")!
//        lblAcres.text = "\(acers)"
        
        
       
    }
    
    
    func DrawMarkers()
    {
        DispatchQueue.main.async(execute: {() -> Void in

            
            var bounds = GMSCoordinateBounds()
            
            
            
            
            
        
            var numberOfMarkers = Int()
            numberOfMarkers = self.PlaceDetail_array.count - 1
            
            print(self.PlaceDetail_array)
            
            for i in 0...numberOfMarkers
            {
                
//                let marker_1 = GMSMarker()
                
                
                var Position = CLLocationCoordinate2D()
                
                
                
                let tempdict = self.PlaceDetail_array.object(at: i)
                
                
                let name = (tempdict as AnyObject).object(forKey: "name") as! String
                let address = (tempdict as AnyObject).object(forKey: "vicinity") as! String
                let latitude = (tempdict as AnyObject).value(forKeyPath: "geometry.location.lat") as! CLLocationDegrees
                let longitude = (tempdict as AnyObject).value(forKeyPath: "geometry.location.lng") as! CLLocationDegrees
                
                print(name)
                print(address)
                print(latitude)
                print(longitude)
                
                Position = CLLocationCoordinate2DMake(latitude, longitude)
                let marker_1 = GMSMarker(position: Position)

                
                
                //            Position = CLLocationCoordinate2DMake(latitude, longitude)
//                marker_1.position = CLLocationCoordinate2D(latitude: latitude, longitude: latitude)
                
                bounds = bounds.includingCoordinate(marker_1.position)

                
                marker_1.title = "\(name)"
                marker_1.snippet = "\(address)"
                marker_1.map = self.viewMapView
            }
      
            
            
            
            
            
            
            self.viewMapView.animate(with: GMSCameraUpdate.fit(bounds))
        
        })
        
        
    }
    
    
    
    
}
