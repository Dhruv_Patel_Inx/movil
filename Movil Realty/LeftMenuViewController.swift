//
//  LeftViewController.swift
//  Movil Realty
//
//  Created by Apple on 17/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit


let keymenu = "Menu"
let keyImage = "Mimage"

class LeftMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet var profileimage1: UIImageView!
    @IBOutlet var profileimage2: UIImageView!
    @IBOutlet var starimageview: UIImageView!
    @IBOutlet var carlabel: UILabel!
    @IBOutlet var mainnamelabel: UILabel!
    @IBOutlet var addresslabel: UILabel!
    @IBOutlet weak var table:UITableView!
    
    var agentOrUserIsLogin = Bool()
    
    var seletedtag = Int()
    
    var arrayMenu = [NSDictionary]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        agentOrUserIsLogin = false
        
        if UserDefaults.standard.object(forKey: kLoginUserDic) != nil
        {
            let dict = UserDefaults.standard.object(forKey: kLoginUserDic) as! NSDictionary
            
            
            print("\(dict)")
            
            
            carlabel.text = dict.object(forKey: "user_name") as! String?
            
           
            let temp_isAgent = dict.object(forKey: "is_agent")!
            
            let isAgent = "\(temp_isAgent)"
            
            if isAgent.isEqual("0")
            {
               // profileimage2.isHidden = true
                addresslabel.text = ""
                mainnamelabel.text = "Movil Reality"
            }
            else
            {
                agentOrUserIsLogin = true
                addresslabel.text = dict.object(forKey: "address") as! String?
                mainnamelabel.text = dict.object(forKey: "company") as! String?
                
                let image : UIImage = UIImage(named:"car")!
                profileimage2.image = image
                
                if dict.object(forKey: "pic") != nil
                {
                    
                    let urlStr = dict.object(forKey: "pic") as! String
            
                    if urlStr.characters.count != 0
                    {

                        profileimage1.sd_setImage(with: URL(string:dict.object(forKey: "pic") as! String)!)
                    }
                }
            }
            
        }

        arrayMenu.append([keymenu:"Map Search",keyImage:"map_sel"])
        arrayMenu.append([keymenu:"Search Nearby",keyImage:"menu-search"])
        arrayMenu.append([keymenu:"Find an Agent",keyImage:"menu-fagent"])
        arrayMenu.append([keymenu:"My Favorites",keyImage:"menu-favorite"])
        arrayMenu.append([keymenu:"Saved Searches",keyImage:"menu-search"])

        arrayMenu.append([keymenu:"My Profile",keyImage:"menu-myprof"])
        
        arrayMenu.append([keymenu:"My Home",keyImage:"menu-home"])
        arrayMenu.append([keymenu:"Schedule an Appointment",keyImage:"menu-appoiment"])
        
        if(agentOrUserIsLogin == true)
        {
            arrayMenu.append([keymenu:"Agent Forum",keyImage:"menu-userprof"])
        }
        
        arrayMenu.append([keymenu:"Mortage Calculator",keyImage:"menu-calc"])
        
        
        arrayMenu.append([keymenu:"Share my app",keyImage:"menu-share"])
        
        arrayMenu.append([keymenu:"Settings",keyImage:"menu-setting"])

       seletedtag = 0

        NotificationCenter.default.addObserver(self, selector: #selector(self.updateProfileDetail), name: NSNotification.Name(rawValue: "UpdateProfileDetail"), object: nil)
        
        
        table.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    
    func updateProfileDetail()
    {
        
        
        if UserDefaults.standard.object(forKey: kLoginUserDic) != nil
        {
        
        
        let dict = UserDefaults.standard.object(forKey: kLoginUserDic) as! NSDictionary
        carlabel.text = dict.object(forKey: "user_name") as! String?
      
        
            let temp_isAgent = dict.object(forKey: "is_agent")!
            
            let isAgent = "\(temp_isAgent)"
            
            if isAgent.isEqual("0")
            {
               // profileimage2.isHidden = true
            
                addresslabel.text = ""
                mainnamelabel.text = "Movil Reality"
            }
            else
            {
                agentOrUserIsLogin = true
                addresslabel.text = dict.object(forKey: "address") as! String?
                mainnamelabel.text = dict.object(forKey: "company") as! String?
                let image : UIImage = UIImage(named:"car")!
                
                profileimage2.image = image
                if dict.object(forKey: "pic") != nil
                {
                    
                    
                    
                  let urlStr = dict.object(forKey: "pic") as! String
                    
                
                    if urlStr.characters.count != 0
                    {
                         self.profileimage1.sd_setImage(with: URL(string:dict.object(forKey: "pic") as! String)!)
                    }
                }
            }
            
        }
        else
        {
            carlabel.text = "Guest"
            addresslabel.text = ""
            mainnamelabel.text = "Movil Reality"
            
            let image : UIImage = UIImage(named:"car")!
            
            profileimage2.image = image
            agentOrUserIsLogin = false
        }
        self.UpdateTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        profileimage1.layer.cornerRadius = profileimage1.frame.size.height/2
        profileimage2.layer.cornerRadius = profileimage2.frame.size.height/2
        starimageview.layer.cornerRadius = starimageview.frame.size.height/2
        
        profileimage1.clipsToBounds = true
        profileimage2.clipsToBounds = true
        starimageview.clipsToBounds = true
        
        starimageview.layer.borderWidth = 2.0
        starimageview.layer.borderColor = UIColor.white.cgColor
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        print("view Will Appear")
    }
    
    func UpdateTableView()
    {
        
        arrayMenu.removeAll()
        
        arrayMenu.append([keymenu:"Map Search",keyImage:"map_sel"])
        
        arrayMenu.append([keymenu:"Search Nearby",keyImage:"menu-search"])
        
        arrayMenu.append([keymenu:"Find an Agent",keyImage:"menu-fagent"])
        
        arrayMenu.append([keymenu:"My Favorites",keyImage:"menu-favorite"])
        
        arrayMenu.append([keymenu:"Saved Searches",keyImage:"menu-search"])

        arrayMenu.append([keymenu:"My Profile",keyImage:"menu-myprof"])
        
        arrayMenu.append([keymenu:"My Home",keyImage:"menu-home"])
        
        arrayMenu.append([keymenu:"Schedule an Appointment",keyImage:"menu-appoiment"])
        
        if(agentOrUserIsLogin == true)
        {
            arrayMenu.append([keymenu:"Agent Forum",keyImage:"menu-userprof"])
        }
        
        arrayMenu.append([keymenu:"Mortage Calculator",keyImage:"menu-calc"])
        
        arrayMenu.append([keymenu:"Share my app",keyImage:"menu-share"])
        
        arrayMenu.append([keymenu:"Settings",keyImage:"menu-setting"])
        
//        seletedtag = 4
        
        table.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayMenu.count;
    }
    

  
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let tablecell = tableView.dequeueReusableCell(withIdentifier: "Table_Cell", for:indexPath)
        tablecell.imageView?.image = UIImage(named: arrayMenu[indexPath.row][keyImage] as! String)
        tablecell.textLabel?.text = arrayMenu[indexPath.row][keymenu] as? String
        
        if seletedtag == indexPath.row
        {
            tablecell.textLabel?.textColor = OrangeThemeColor
        }
        else
        {
            tablecell.textLabel?.textColor = UIColor.black
        }
        
        return tablecell
    }
 
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        switch indexPath.row {
            
        case 0:
            appdel.tabbar.selectedIndex = 0
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: appdel.tabbar, withSlideOutAnimation: false, andCompletion: nil)
            break
        case 1:
            
            let searchNear = self.storyboard?.instantiateViewController(withIdentifier: "SearchNearVC") as! SearchNearViewController
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: searchNear, withSlideOutAnimation: false, andCompletion: nil)
            
            break
            
        case 2:
            appdel.tabbar.selectedIndex = 2
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: appdel.tabbar, withSlideOutAnimation: false, andCompletion: nil)
            break
            
        case 3:
            
            let favourite = self.storyboard?.instantiateViewController(withIdentifier: "FavouriteVC") as! FavouriteViewController
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: favourite, withSlideOutAnimation: false, andCompletion: nil)
            
            break
            
            
        case 4:
            
            if(agentOrUserIsLogin == true)
            {

                
                let savedSearch = self.storyboard?.instantiateViewController(withIdentifier: "SavedSearchVC") as! SavedSearchVC
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: savedSearch, withSlideOutAnimation: false, andCompletion: nil)
            }
            else
            {
                
                let userDefaults1 = Foundation.UserDefaults.standard
                userDefaults1.removeObject(forKey: kNavigation)
                userDefaults1.set("SavedSearch", forKey: kNavigation)
                userDefaults1.synchronize()
                print(userDefaults1.object(forKey: kNavigation)!)
                
                
                
                
                let signIn = self.storyboard?.instantiateViewController(withIdentifier: "signInViewC") as! signInViewC
                
                
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: signIn, withSlideOutAnimation: false, andCompletion: nil)
            }

            break
    
            
        case 5:
            
            if UserDefaults.standard.object(forKey: kLoginUserDic) != nil
            {
                // exist
                
                let profile = self.storyboard?.instantiateViewController(withIdentifier: "myProfileVC") as! myProfileVC
                
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: profile, withSlideOutAnimation: false, andCompletion: nil)
                
                
            }
            else {
                // not exist
                
                
                let userDefaults1 = Foundation.UserDefaults.standard
                userDefaults1.removeObject(forKey: kNavigation)
                userDefaults1.set("MyProfile", forKey: kNavigation)
                userDefaults1.synchronize()
                print(userDefaults1.object(forKey: kNavigation)!)
                
                
                
                let Obj_signInViewC = self.storyboard?.instantiateViewController(withIdentifier: "signInViewC")
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: Obj_signInViewC, withSlideOutAnimation: false, andCompletion: nil)
                
                
            }
            
            break
            
        case 6:
            
            if(agentOrUserIsLogin == true)
            {
                
                let dict = UserDefaults.standard.object(forKey: kLoginUserDic)as! NSDictionary
                
                let id = "\(dict.object(forKey: "is_agent")!)"
                
                if id.isEqual("0")
                {
                    let SearchVCAgent = self.storyboard?.instantiateViewController(withIdentifier: "SearchControllerVC") as! SearchController
                    
                    SlideNavigationController.sharedInstance().popToRootAndSwitch(to: SearchVCAgent, withSlideOutAnimation: false, andCompletion: nil)
                }
                else
                {
                    let SearchVCAgent = self.storyboard?.instantiateViewController(withIdentifier: "SearchControllerAgentVC") as! SearchControllerAgentVC
                    
                    SlideNavigationController.sharedInstance().popToRootAndSwitch(to: SearchVCAgent, withSlideOutAnimation: false, andCompletion: nil)
                }
            }
            else
            {
            
                let userDefaults1 = Foundation.UserDefaults.standard
                userDefaults1.removeObject(forKey: kNavigation)
                userDefaults1.set("MyHome", forKey: kNavigation)
                userDefaults1.synchronize()
                print(userDefaults1.object(forKey: kNavigation)!)
                
                
                
                let signIn = self.storyboard?.instantiateViewController(withIdentifier: "signInViewC") as! signInViewC
                
                
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: signIn, withSlideOutAnimation: false, andCompletion: nil)
            }
            
            
            break
            
            
        case 7:
            
            if(agentOrUserIsLogin == true)
            {
                let schedule = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleVC") as! ScheduleViewController
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: schedule, withSlideOutAnimation: false, andCompletion: nil)
            }
            else
            {
                
                let userDefaults1 = Foundation.UserDefaults.standard
                userDefaults1.removeObject(forKey: kNavigation)
                userDefaults1.set("Schedual", forKey: kNavigation)
                userDefaults1.synchronize()
                print(userDefaults1.object(forKey: kNavigation)!)
                

                
                let signIn = self.storyboard?.instantiateViewController(withIdentifier: "signInViewC") as! signInViewC
               
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: signIn, withSlideOutAnimation: false, andCompletion: nil)
            

            }
            
            
            break
            
        case 8:
            
            
            if(agentOrUserIsLogin == true)
            {
                
                let mortage = self.storyboard?.instantiateViewController(withIdentifier: "AgentForumVC") as! AgentForumVC
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: mortage, withSlideOutAnimation: false, andCompletion: nil)
            }
            else
            {
                let mortage = self.storyboard?.instantiateViewController(withIdentifier: "MortageVC") as! MortageViewController
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: mortage, withSlideOutAnimation: false, andCompletion: nil)
            }
            
            break
            
            
        case 9:
            
            if(agentOrUserIsLogin == true)
            {
                
                let mortage = self.storyboard?.instantiateViewController(withIdentifier: "MortageVC") as! MortageViewController
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: mortage, withSlideOutAnimation: false, andCompletion: nil)
            }
            else
            {
                let Obj_sharemyappVC = self.storyboard?.instantiateViewController(withIdentifier: "ShareMyAppControllerVC")
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: Obj_sharemyappVC, withSlideOutAnimation: false, andCompletion: nil)
            }
            
            break
            
        case 10:
            
            if(agentOrUserIsLogin == true)
            {
                let Obj_sharemyappVC = self.storyboard?.instantiateViewController(withIdentifier: "ShareMyAppControllerVC")
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: Obj_sharemyappVC, withSlideOutAnimation: false, andCompletion: nil)
            }
            else
            {
                let setting = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsViewController
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: setting , withSlideOutAnimation: false, andCompletion: nil)
            }
            break
            
        case 11:
            
            if(agentOrUserIsLogin == true)
            {
                let setting = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsViewController
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: setting , withSlideOutAnimation: false, andCompletion: nil)
            }
            else
            {
                SlideNavigationController.sharedInstance().toggleLeftMenu()
            }
        default:
            SlideNavigationController.sharedInstance().toggleLeftMenu()
        }
        
        seletedtag = indexPath.row
        
        table.reloadData()
        
    }
    
}
