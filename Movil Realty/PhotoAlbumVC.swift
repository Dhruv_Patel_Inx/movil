//
//  PhotoAlbumVC.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 12/04/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class PhotoAlbumVC: UIViewController, ASCollectionViewDataSource, ASCollectionViewDelegate {

    @IBOutlet var collectionView: ASCollectionView!
    
    
    @IBOutlet var imgSelView: UIImageView!
    @IBOutlet var blurView: UIView!
    @IBOutlet var lblTotaImage: UILabel!
    var imgArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        collectionView.delegate = self
        collectionView.asDataSource = self
        
        lblTotaImage.text = NSString(format: "%ld",(imgArray.count)) as String
        
        self.blurView.isHidden = true
        self.imgSelView.isHidden = true
        
        if !UIAccessibilityIsReduceTransparencyEnabled()
        {
            self.blurView.backgroundColor = UIColor.clear
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = self.blurView.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.blurView.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        }
        else
        {
            self.blurView.backgroundColor = UIColor.black
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: ASCollectionViewDataSource
    
    func numberOfItemsInASCollectionView(_ asCollectionView: ASCollectionView) -> Int {
        return imgArray.count
    }
    
    func collectionView(_ asCollectionView: ASCollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell
    {
        let gridCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GridCell
        
        let imagUrlString = imgArray.object(at: indexPath.row) as! String
        
        let url = URL(string: imagUrlString)
        let placeHolderImage = "propertyList"
        let placeimage = UIImage(named: placeHolderImage)
        gridCell.imageView.sd_setImage(with: url, placeholderImage: placeimage)
        
        gridCell.btnImg.tag =  indexPath.row
       // gridCell.btnImg.addTarget(self, action: Selector(("buttonClick")), for: UIControlEvents.touchUpInside)
        
        gridCell.btnImg.addTarget(self, action: #selector(imgSelAct(sender:)), for: .touchUpInside)
        
        
        return gridCell
    }
    
    
    
    func collectionView(_ asCollectionView: ASCollectionView, parallaxCellForItemAtIndexPath indexPath: IndexPath) -> ASCollectionViewParallaxCell {
        let parallaxCell = collectionView.dequeueReusableCell(withReuseIdentifier: "parallaxCell", for: indexPath) as! ParallaxCell
        
        let imagUrlString = imgArray.object(at: indexPath.row) as! String
        
        parallaxCell.imgbtn.tag =  indexPath.row
        parallaxCell.imgbtn.addTarget(self, action: #selector(imgSelAct(sender:)), for: .touchUpInside)
        
       
        parallaxCell.updateParallaxURL(imagUrlString as NSString)
        
        
 
        return parallaxCell
    }
 
    func imgSelAct(sender:UIButton)
    {
       // print(sender.tag)
        
        self.blurView.isHidden = false
        self.imgSelView.isHidden = false
        
        
        let imagUrlString = imgArray.object(at: sender.tag) as! String
        
        let url = URL(string: imagUrlString)
        let placeHolderImage = "propertyList"
        let placeimage = UIImage(named: placeHolderImage)
        self.imgSelView.sd_setImage(with: url, placeholderImage: placeimage)
        
        lblTotaImage.text = NSString(format: "%ld/%ld",sender.tag+1,(imgArray.count)) as String
    }
    

    @IBAction func backAct(_ sender: Any)
    {
        
        if(self.blurView.isHidden == true)
        {
             _ = self.navigationController?.popViewController(animated: true)
        }
        else
        {
            self.blurView.isHidden = true
            self.imgSelView.isHidden = true
             lblTotaImage.text = NSString(format: "%ld",(imgArray.count)) as String
        }
       
    }
}
class GridCell: UICollectionViewCell {
    
    @IBOutlet var label: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var btnImg: UIButton!
}

class ParallaxCell: ASCollectionViewParallaxCell {
    
    @IBOutlet var imgbtn: UIButton!
    @IBOutlet var label: UILabel!
    
}

