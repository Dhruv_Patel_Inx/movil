//
//  PropertyViewController.swift
//  Movil Realty
//
//  Created by Apple on 17/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

import SDWebImage

class PropertyViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet var propertytable:UITableView!
    @IBOutlet var btnSort:UIButton!
    
    @IBOutlet var imgSort:UIImageView!
    
    @IBOutlet var lblResult: UILabel!
    
    @IBOutlet weak var btnReset: UIButton!
    
    @IBOutlet weak var barItemProperties: UITabBarItem!
    
    
    var arrayOption = [String]()
    
    var DipslaypropertyArray: NSMutableArray!
    var seletedtag = Int()
    
    // MARK: - UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayOption = ["Prise, High to low","Prise,Low to High","Listing Date, Newest to Oldest","Square Feet, High to Low","Square Feet, Low to High"]
        
        seletedtag = 100
        propertytable.dataSource =  self
        propertytable.delegate = self
        // Do any additional setup after loading the view.
        
       // self.navigationController?.navigationBar.topItem?.title = "Map Search"
         self.navigationController?.isNavigationBarHidden = true
        
          
        lblResult.text = String(format:"Result %d",appdel.arrayProparty.count)
        
        DipslaypropertyArray = appdel.arrayProparty
        
        
        
        /*---------- Call SetUpValue ---------*/
        
        self.SetUpValue()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    // MARK: - UITableView Delegate Methods
    
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        if btnSort.tag == 1 {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if btnSort.tag == 1 {
            if section == 0  {
                return arrayOption.count
            }
            return DipslaypropertyArray.count
        }
        return DipslaypropertyArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if btnSort.tag == 1 {
            if indexPath.section == 0  {
                return 44
            }
            return 340
        }
        return 340
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if btnSort.tag == 1 && indexPath.section == 0
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "CellP")
            if cell == nil {
                cell = UITableViewCell(style: .default, reuseIdentifier: "CellP")
                cell?.selectionStyle = .none
            }
            
            cell?.textLabel?.text = "\(arrayOption[indexPath.row])"
            cell?.textLabel?.font = UIFont.init(name: "Optima-Regular", size: 15.0)
            if seletedtag == indexPath.row {
                cell?.textLabel?.textColor = OrangeThemeColor
            }
            else
            {
                cell?.textLabel?.textColor = UIColor.black
            }
            return cell!
        }
        else
        {
            
            let propcell: PropertyCell! = propertytable.dequeueReusableCell(withIdentifier: "PropertyCell", for: indexPath) as? PropertyCell
            
            let currentDict = DipslaypropertyArray.object(at: indexPath.row) as! NSDictionary
            
            print(currentDict)
            
            
            
            let address = currentDict.object(forKey: "L_Address")!
            let price = currentDict.object(forKey: "L_AskingPrice")!
            
            let status = currentDict.object(forKey: "L_Status")!
            
            var totalbath = ""
            var totalbed = ""
            var sqft = ""
            
            
            if currentDict.object(forKey: "LM_Int1_14") != nil
            {
                totalbath = "\(currentDict.object(forKey: "LM_Int1_14")!)"
                print(totalbath)
                
            }
            else
            {
                totalbath = ""
            }
            
            
            
            if currentDict.object(forKey: "LM_Int1_1") != nil
            {
                
                totalbed = "\(currentDict.object(forKey: "LM_Int1_1")!)"
                print(totalbed)
                
            }
            else
            {
                totalbed = ""
                
            }
            
            
            if currentDict.object(forKey: "LM_Dec_1") != nil
            {
                
                sqft = "\(currentDict.object(forKey: "LM_Dec_1")!)"
                print(sqft)
                
            }
            else
            {
                sqft = ""
                
            }
            
            
            
            if (status as AnyObject) .isEqual("1") {
                propcell?.lblStatus.text = "\u{2022} Active"
                propcell.lblStatus.textColor = UIColor(red: 0/255, green: 147/255, blue: 51/255, alpha: 1.0)
            }
            else if (status as AnyObject) .isEqual("2")
            {
                propcell.lblStatus.text = "\u{2022} Contigent"
                propcell.lblStatus.textColor = UIColor(red: 255/255, green: 102/255, blue: 30/255, alpha: 1.0)
            }
            else if (status as AnyObject) .isEqual("3")
            {
                propcell.lblStatus.text = "\u{2022} Pending"
                propcell.lblStatus.textColor = UIColor(red: 224/255, green: 200/255, blue: 39/255, alpha: 1.0)
            }
            else
            {
                propcell.lblStatus.text = "\u{2022} Closed"
                propcell.lblStatus.textColor = UIColor(red: 221/255, green: 0/255, blue: 0/255, alpha: 1.0)
            }
            //            propcell?.lblStatus.text = "\(status)"
            propcell?.addresslabel.text =  "\(address)"
            propcell?.pricelabel.text = "$\(price)"
            propcell.typelabel.text = "\(totalbed) bd | \(totalbath) ba | \(sqft) sf"
            

            
            
            if currentDict.object(forKey: "path") is NSNull
            {
                let placeHolderImage = "propertyList"
                propcell.houseimageview.image = UIImage(named: placeHolderImage)
            }
            else
            {
                let imagUrlString = currentDict.object(forKey: "path") as! String
                
                
                let url = URL(string: imagUrlString)
                let placeHolderImage = "propertyList"
                let placeimage = UIImage(named: placeHolderImage)
                propcell.houseimageview.sd_setImage(with: url, placeholderImage: placeimage)

            }
            
                        //            propcell?.houseimageview.sd_setImage(with: url, placeholderImage: placeimage)
            
            return propcell!
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(indexPath.section == 0 && btnSort.tag == 1)
        {
            self.sortingSelect(selectedIndex: indexPath.row)
        }
        else
        {
            
            let passDict = DipslaypropertyArray.object(at: indexPath.row) as! NSDictionary
            
            let p_id = passDict.object(forKey: "L_ListingID")!
            
            let residentVC = self.storyboard?.instantiateViewController(withIdentifier: "ResidentActiveVC") as! ResidentActiveVC
            
            residentVC.propertyId = p_id as! String
            
            self.navigationController?.pushViewController(residentVC, animated: true)
        }
    }

    // MARK: - UIView Action Method
    @IBAction func btnSortClick(_ sender:UIButton)
    {
        if sender.tag == 0
        {
            btnSort.setTitleColor(OrangeThemeColor, for: UIControlState.normal)
            
            imgSort.image = UIImage(named:"DownOrange")
            
            
             //add menu
            sender.tag = 1
            propertytable.insertSections(IndexSet.init(integer: 0), with: .bottom)
            let indexPath = IndexPath(row: 0, section: 0)
            propertytable.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: true)
                        
        }
        else
        {
            btnSort.setTitleColor(UIColor.black, for: UIControlState.normal)
            imgSort.image = UIImage(named:"DownBlack")
            sender.tag = 0
            propertytable.deleteSections(IndexSet.init(integer: 0), with: .top)
            
        }
    }
    @IBAction func btnResetClick(_sender:UIButton)
    {
        
    }
    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
    @IBAction func selInfoAct(_ sender: AnyObject)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MoreinfoVC")
        self.present(controller, animated: true, completion: nil)
        
    }

    // MARK: - UIView Function
    func sortingSelect(selectedIndex:Int)
    {
        switch selectedIndex {
        case 0:
            let sorted =  (DipslaypropertyArray as NSArray).sorted(by: { (($0 as! NSDictionary).object(forKey: "L_AskingPrice") as! NSString).floatValue > (($1 as! NSDictionary).object(forKey: "L_AskingPrice") as! NSString).floatValue })
            DipslaypropertyArray.removeAllObjects()
            DipslaypropertyArray.addObjects(from: sorted)
            break
        case 1:
            let sorted =  (DipslaypropertyArray as NSArray).sorted(by: { (($0 as! NSDictionary).object(forKey: "L_AskingPrice") as! NSString).floatValue < (($1 as! NSDictionary).object(forKey: "L_AskingPrice") as! NSString).floatValue })
            DipslaypropertyArray.removeAllObjects()
            DipslaypropertyArray.addObjects(from: sorted)
            break
        case 2:
            let sortedArray = (DipslaypropertyArray as NSArray).sortedArray(comparator: {
                (obj1, obj2) -> ComparisonResult in
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let date1 = dateFormatter.date(from: (obj1 as! NSDictionary).object(forKey: "L_ListingDate") as! String)
                let data2 = dateFormatter.date(from: (obj2 as! NSDictionary).object(forKey: "L_ListingDate") as! String)
                let result = date1?.compare(data2! as Date)
                return result!
            })
            DipslaypropertyArray.removeAllObjects()
            DipslaypropertyArray.addObjects(from: sortedArray)
            break
        case 3:
            
            print("\(DipslaypropertyArray as NSArray)")
            
            let sorted =  (DipslaypropertyArray as NSArray).sorted(by: { (($0 as! NSDictionary).object(forKey: "LM_Dec_1") as! Int) > (($1 as! NSDictionary).object(forKey: "LM_Dec_1") as! Int) })
            DipslaypropertyArray.removeAllObjects()
            DipslaypropertyArray.addObjects(from: sorted)

            break
        case 4:
            let sorted =  (DipslaypropertyArray as NSArray).sorted(by: { (($0 as! NSDictionary).object(forKey: "LM_Dec_1") as! Int) < (($1 as! NSDictionary).object(forKey: "LM_Dec_1") as! Int) })
            DipslaypropertyArray.removeAllObjects()
            DipslaypropertyArray.addObjects(from: sorted)
            break
        default:
            break
        }
        
        seletedtag = selectedIndex
        propertytable.reloadData()
        btnSortClick(btnSort)
    }
    
    
    
    // MARK: - SetUpValue Method
    
    
    func SetUpValue()
    {
        btnReset.setTitle(strReset, for: UIControlState.normal)
        barItemProperties.title = strProperties
    }
    
    
    
    
    
    

}
