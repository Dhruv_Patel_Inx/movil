//
//  SettingsViewController.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 14/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tblSetting: UITableView!
    
    @IBOutlet weak var btnNext: UIButton!
    var temp = Int()
    var sectiontitle_array = NSArray()
    var rowtitle1_array = NSArray()
    var rowtitle2_array = NSArray()
    var rowtitle3_array = NSArray()
    var rowtitle4_array = NSArray()
    
    
    //MARK: - UIView Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        temp = 0
        
        /*-------- Set Array --------*/
        
        sectiontitle_array = ["Map Style","Notification","Help & Feedback","Legal Information"]
        rowtitle1_array = ["Classic","Settelite"]
        rowtitle2_array = ["Saved Searches","Saved Homes","App Features & Updates"]
        rowtitle3_array = ["Rate this App"]
        rowtitle4_array = ["Privacy Policy","Terms of Use","Mobile Choices","Third Party Licenses & Notices"]
        
        
                
        /*-------- Table View delegate --------*/
        
        tblSetting.delegate = self
        tblSetting.dataSource = self

        self.setUpSettingView()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - All Actions
    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
   
    @IBAction func selNextAct(_ sender: Any) {
        

        
        if (sender as AnyObject).tag == 0{
            
            
            
            
            let userDefaults1 = Foundation.UserDefaults.standard
            print("\(userDefaults1.object(forKey: kMapType)!)")
            var check = userDefaults1.object(forKey: kMapType)as! String
            userDefaults1.removeObject(forKey: kMapType)
            userDefaults1.synchronize()

            
            
            if check.isEqual("0") {
                
                check = "1"
            }
            
            let userDefaults = Foundation.UserDefaults.standard
            userDefaults.set(check, forKey: kMapType)
            userDefaults.synchronize()

            
        }
        
        else{
            
            let userDefaults1 = Foundation.UserDefaults.standard
            print("\(userDefaults1.object(forKey: kMapType)!)")
            var check = userDefaults1.object(forKey: kMapType)as! String
            userDefaults1.removeObject(forKey: kMapType)
            userDefaults1.synchronize()




            if check.isEqual("1") {
                
                        check = "0"
                
            }
            
            
            let userDefaults = Foundation.UserDefaults.standard
            userDefaults.set(check, forKey: kMapType)
            userDefaults.synchronize()

            

        }
        
       tblSetting.reloadData()
    }
    
    @IBAction func selSwitchAct(_ sender: Any) {
        
        if (sender as AnyObject).tag == 0
        {
            
            print("SAVEDSEARCH")
            
            let userDefaults2 = Foundation.UserDefaults.standard
            var check = userDefaults2.object(forKey: kSavedSearch)as! String
            
            userDefaults2.removeObject(forKey: kSavedSearch)
            userDefaults2.synchronize()
            

            if check.isEqual("0")
            {
                
                check = "1"
                
            }
            else
            {
                
                check = "0"
                
            }
            
            //Create Map Type Default
            let userDefaults1 = Foundation.UserDefaults.standard
            userDefaults1.set(check, forKey: kSavedSearch)
            userDefaults1.synchronize()
            
        }
            
        else if (sender as AnyObject).tag == 1 {
            
            print("SAVEDHOME")
            let userDefaults2 = Foundation.UserDefaults.standard
            var check = userDefaults2.object(forKey: kSavedHomes)as! String
            
            userDefaults2.removeObject(forKey: kSavedHomes)
            userDefaults2.synchronize()
            
            
            if check.isEqual("0")
            {
                
                check = "1"
                
            }
            else
            {
                
                check = "0"
                
            }
            
            //Create Map Type Default
            let userDefaults1 = Foundation.UserDefaults.standard
            userDefaults1.set(check, forKey: kSavedHomes)
            userDefaults1.synchronize()
            
        }
        else
        {
            print("APPFEATURES")
            
            let userDefaults2 = Foundation.UserDefaults.standard
            var check = userDefaults2.object(forKey: kAppFeatures)as! String
            
            userDefaults2.removeObject(forKey: kAppFeatures)
            userDefaults2.synchronize()
            
            
            if check.isEqual("0")
            {
                
                check = "1"
                
            }
            else
            {
                
                check = "0"
                
            }
            
            //Create Map Type Default
            let userDefaults1 = Foundation.UserDefaults.standard
            userDefaults1.set(check, forKey: kAppFeatures)
            userDefaults1.synchronize()
            
        }
        
        tblSetting.reloadData()

        
    }
    
    // MARK: - Table View Delegate Methods
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0 {
            
            return 2
        }
        else if section == 1{
            
            return 3
        }
        else if section == 2{
            
            return 1
        }
        else{
            
            return 4
        }
        
    }
    
    
   
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let settingcell = tblSetting.dequeueReusableCell(withIdentifier: "SettingCellVC")as! SettingCell
        
        tblSetting.sectionFooterHeight = 5.0
        
        if indexPath.section == 0
        {
            
            settingcell.lblText.text = "\(rowtitle1_array[indexPath.row])"
            settingcell.`switch`.isHidden = true
            settingcell.btnNext.isHidden = false
            
            
          
            settingcell.btnNext.tag = indexPath.row
            
            
            let userDefaults2 = Foundation.UserDefaults.standard
            let check = userDefaults2.object(forKey: kMapType)as! String
            
//             settingcell.btnNext.setTitle("UNCHEK", for: UIControlState.normal)
//             settingcell.btnNext.setTitleColor(UIColor.blue, for: UIControlState.normal)
               settingcell.btnNext.setImage(#imageLiteral(resourceName: "Darktick"), for: UIControlState.normal)
            
            
           
                if check.isEqual("0")
                    
                {
                    if(indexPath.row == 0)
                    {
//                        settingcell.btnNext.setTitle("CHEK", for: UIControlState.normal)
                        settingcell.btnNext.setImage(#imageLiteral(resourceName: "Darktick"), for: UIControlState.normal)
                        
                    }
                    else
                    {
//                        settingcell.btnNext.setTitle("UNCHEK", for: UIControlState.normal)
                        settingcell.btnNext.setImage(#imageLiteral(resourceName: "Orangetick"), for: UIControlState.normal)

                    }
                    
                }
                else
                {
                    if(indexPath.row == 0)
                    {
//                        settingcell.btnNext.setTitle("UNCHEK", for: UIControlState.normal)
                        settingcell.btnNext.setImage(#imageLiteral(resourceName: "Orangetick"), for: UIControlState.normal)
                    }
                    else
                    {
//                        settingcell.btnNext.setTitle("CHEK", for: UIControlState.normal)
                        settingcell.btnNext.setImage(#imageLiteral(resourceName: "Darktick"), for: UIControlState.normal)

                    }
                }
          
            
            
            
        }
        else if indexPath.section == 1
        {
            
            settingcell.lblText.text = "\(rowtitle2_array[indexPath.row])"
            settingcell.`switch`.isHidden = false
            settingcell.btnNext.isHidden = true
            
            if indexPath.row == 0
            {
                
                
                
                    
                    let userDefaults2 = Foundation.UserDefaults.standard
                    let check = userDefaults2.object(forKey: kSavedSearch)as! String
                
                
                
                    
                    if check.isEqual("1")
                    {
                        
                        settingcell.`switch`.isOn = true
                        print("Saved Search ON\(check)")
                        
                        
                    }
                    else{
                        
                        settingcell.`switch`.isOn = false
                        print("Saved Search OFF\(check)")
                        
                    }
                    
                
                
            }
            else if indexPath.row == 1
            {
                
                    let userDefaults3 = Foundation.UserDefaults.standard
                    let check = userDefaults3.object(forKey: kSavedHomes)as! String
                    
                    if check.isEqual("1"){
                        
                        settingcell.`switch`.isOn = true
                        print("SavedHomes ON\(check)")
                        
                    }
                    else{
                        
                        settingcell.`switch`.isOn = false
                        print("SavedHomes OFF\(check)")
                        
                    }
                    
                    
                
            }
            else
            {
                
                let userDefaults4 = Foundation.UserDefaults.standard
                let check = userDefaults4.object(forKey: kAppFeatures)as! String
                
                if check.isEqual("1")
                {
                    
                    settingcell.`switch`.isOn = true
                    print("AppFeatures ON\(check)")
                    
                }
                else{
                    
                    settingcell.`switch`.isOn = false
                    print("AppFeatures OFF\(check)")
                    
                }
                
                
            }
            
            
            
            //            settingcell.`switch`.isOn = true
            settingcell.`switch`.tag = indexPath.row
            
        }
        else if indexPath.section == 2
        {
            settingcell.lblText.text = "\(rowtitle3_array[indexPath.row])"
            settingcell.`switch`.isHidden = true
            settingcell.btnNext.isHidden = false
            
        }
            
        else{
            settingcell.lblText.text = "\(rowtitle4_array[indexPath.row])"
            settingcell.`switch`.isHidden = true
            settingcell.btnNext.isHidden = false
            
        }
        return settingcell
        
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 4
        
    }
    
//   func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
//   {
//    
//  
//    
//    return "\(sectiontitle_array.object(at: section))"
//    
//    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let  headercell = tableView.dequeueReusableCell(withIdentifier: "SettingHeaderCellVC") as! SettingHeaderCell
        
        headercell.lblHeader.text = "\(sectiontitle_array.object(at: section))"
        
        
//        headerCell.section_title.text = table_data[section].section
//        headerCell.backgroundColor =  UIColor(red:72/255,green:141/255,blue:200/255,alpha:0.9)
        
        return headercell
        
    }
    
  
//   func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
//   {
//    
//    
//    let header = view as! UITableViewHeaderFooterView
//    header.backgroundView?.backgroundColor = UIColor.clear
//    
//    let label = UILabel()
//    label.frame = CGRect(x: 14, y: 0, width: 250, height: 44)
//    
//    
//    
//    label.text = "\(sectiontitle_array.object(at: section))"
//    label.font = UIFont.optimaBold(Size: 17)
//    header.addSubview(label)
//    label.isHidden = false
//    
//    
//    }
//    
//    func tableView(_ tableView: UITableView, didEndDisplayingHeaderView view: UIView, forSection section: Int){
//        temp += 1
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        tblSetting.tableHeaderView?.backgroundColor = UIColor.white
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        tblSetting.tableFooterView?.backgroundColor = UIColor.gray
        return 3.0
    }


    //MARK: - UIView Functions
    func setUpSettingView()
    {
        if UserDefaults.standard.object(forKey: kMapType) == nil
        {
            //Create Map Type Default
            let userDefaults1 = Foundation.UserDefaults.standard
            userDefaults1.set("1", forKey: kMapType)
            userDefaults1.synchronize()
            
            //Create Saved Search Default
            let userDefaults2 = Foundation.UserDefaults.standard
            userDefaults2.set("1", forKey: kSavedSearch)
            userDefaults2.synchronize()
            
            //Create Saved Homes Default
            let userDefaults3 = Foundation.UserDefaults.standard
            userDefaults3.set("1", forKey: kSavedHomes)
            userDefaults3.synchronize()
            
            
            //Create App Features & Update Default
            let userDefaults4 = Foundation.UserDefaults.standard
            userDefaults4.set("1", forKey: kAppFeatures)
            userDefaults4.synchronize()
            
        }
        
    }
    
  
}
