//
//  FavouriteViewController.swift
//  Movil Realty
//
//  Created by Trivedi Sagar on 14/03/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class FavouriteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var Favourite_Array = NSMutableArray()
    
    var Propertydetail_Array = NSMutableArray()
    var Property_ID = ""
    
    
    @IBOutlet weak var tblFavouriteObj: UITableView!
    
    @IBOutlet weak var lblFavourite: UILabel!
    
    
    // MARK: - View Life Cycle Methods
    

    override func viewWillAppear(_ animated: Bool) {
        
        
        if UserDefaults.standard.object(forKey: kMyFavouriteDic) != nil
        {
            let userDefaults = Foundation.UserDefaults.standard
            
            Favourite_Array = (userDefaults.object(forKey: kMyFavouriteDic) as! NSArray).mutableCopy() as! NSMutableArray
            
            print(Favourite_Array)
            
            self.GetAllProperty()
            
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        if UserDefaults.standard.object(forKey: kMyFavouriteDic) != nil
//        {
//            let userDefaults = Foundation.UserDefaults.standard
//            
//            Favourite_Array = (userDefaults.object(forKey: kMyFavouriteDic) as! NSArray).mutableCopy() as! NSMutableArray
//            
//            print(Favourite_Array)
//            
//            self.GetAllProperty()
//            
//        }
        
        
        tblFavouriteObj.tableFooterView = UIView()

        
        
        tblFavouriteObj.delegate = self
        tblFavouriteObj.dataSource = self
        
        /*-------- SetUpValue call --------*/
        
        self.SetUpValue()
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - All Actions

    
    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
    
    
    // MARK: - Table View delegate Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return Favourite_Array.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
            
            let favouritecell = tblFavouriteObj.dequeueReusableCell(withIdentifier: "FavouriteCellVC")as! FavouriteCell
        
        
        if UserDefaults.standard.object(forKey: kMyFavouriteDic) != nil
        
        {
        
            let currentDict = Propertydetail_Array.object(at: indexPath.row) as! NSDictionary
            
            let address = currentDict.object(forKey: "L_Address")!
            let price = currentDict.object(forKey: "L_AskingPrice")!
            
            let status = currentDict.object(forKey: "L_Status")!
            
//            let imagUrlString = currentDict.object(forKey: "path") as! String
            
            if currentDict.object(forKey: "path") is NSNull
            {
                let placeHolderImage = "propertyList"
                favouritecell.houseimageview.image = UIImage(named: placeHolderImage)
            }
            else
            {
                let imagUrlString = currentDict.object(forKey: "path") as! String
                
                
                let url = URL(string: imagUrlString)
                let placeHolderImage = "propertyList"
                let placeimage = UIImage(named: placeHolderImage)
                favouritecell.houseimageview.sd_setImage(with: url, placeholderImage: placeimage)
                
            }
            
            
            
            
            
            
            
            
            
            
//            let totalbath = currentDict.object(forKey: "LM_Int1_14")!
//            let totalbed = currentDict.object(forKey: "LM_Int1_1")!
//            let sqft = currentDict.object(forKey: "LM_Dec_1")!
//            
            
            
            
            var totalbath = ""
            var totalbed = ""
            var sqft = ""
            var id = ""
            
            
            if currentDict.object(forKey: "LM_Int1_14") != nil
            {
                totalbath = "\(currentDict.object(forKey: "LM_Int1_14")!)"
                print(totalbath)
                
            }
            else
            {
                totalbath = ""
            }
            
            
            
            if currentDict.object(forKey: "LM_Int1_1") != nil
            {
                
                totalbed = "\(currentDict.object(forKey: "LM_Int1_1")!)"
                print(totalbed)
                
            }
            else
            {
                totalbed = ""
                
            }
            
            
            if currentDict.object(forKey: "LM_Dec_1") != nil
            {
                
                sqft = "\(currentDict.object(forKey: "LM_Dec_1")!)"
                print(sqft)
                
            }
            else
            {
                sqft = ""
                
            }
            

            
            
            
            
            
            
            
            
            
            
            
            if (status as AnyObject) .isEqual("1") {
                favouritecell.lblStatus.text = "\u{2022} Active"
                favouritecell.lblStatus.textColor = UIColor(red: 0/255, green: 147/255, blue: 51/255, alpha: 1.0)
            }
            else if (status as AnyObject) .isEqual("2")
            {
                favouritecell.lblStatus.text = "\u{2022} Contigent"
                favouritecell.lblStatus.textColor = UIColor(red: 255/255, green: 102/255, blue: 30/255, alpha: 1.0)
            }
            else if (status as AnyObject) .isEqual("3")
            {
                favouritecell.lblStatus.text = "\u{2022} Pending"
                favouritecell.lblStatus.textColor = UIColor(red: 224/255, green: 200/255, blue: 39/255, alpha: 1.0)
            }
            else
            {
                favouritecell.lblStatus.text = "\u{2022} Closed"
                favouritecell.lblStatus.textColor = UIColor(red: 221/255, green: 0/255, blue: 0/255, alpha: 1.0)
            }
            //            propcell?.lblStatus.text = "\(status)"
            favouritecell.addresslabel.text =  "\(address)"
            favouritecell.pricelabel.text = "$\(price)"
            favouritecell.typelabel.text = "\(totalbed) bd | \(totalbath) ba | \(sqft) sf"
            
            
            
//            let url = URL(string: imagUrlString)
//            let placeHolderImage = "propertyList"
//            let placeimage = UIImage(named: placeHolderImage)
//            favouritecell.houseimageview.sd_setImage(with: url, placeholderImage: placeimage)
            //            propcell?.houseimageview.sd_setImage(with: url, placeholderImage: placeimage)
            
        }
        
            return favouritecell
       
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        
        
            let passDict = Propertydetail_Array.object(at: indexPath.row) as! NSDictionary
            
            let p_id = passDict.object(forKey: "L_ListingID")!
        
            print(p_id)
        
            let residentVC = self.storyboard?.instantiateViewController(withIdentifier: "ResidentActiveVC") as! ResidentActiveVC
            
            residentVC.propertyId = p_id as! String
            
            self.navigationController?.pushViewController(residentVC, animated: true)
        
        
        
        
    }
    

    
    
    // MARK: - Custom Method

    
    func GetAllProperty()
    {
        
        
        if Propertydetail_Array.count != 0
        {
            Propertydetail_Array.removeAllObjects()
            
        }
        
        
        
        
        let Property_count = appdel.arrayProparty.count - 1
        
        for i in 0...Property_count
        {
            let currentDict = appdel.arrayProparty.object(at: i) as! NSDictionary
            
            let id = currentDict.object(forKey: "L_ListingID")as! NSString
            
            if Favourite_Array.contains(id)
            {
                print(id)
                
                Propertydetail_Array.add(currentDict)
                
            }
            
            
            print(Propertydetail_Array)
        }
        
        tblFavouriteObj.reloadData()
        
        
        
    }
    
    
    
    
    // MARK: - SetUpValue Method
    
    func SetUpValue()
    {
        lblFavourite.text = strFavourite
    }

    
    
    
    
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
