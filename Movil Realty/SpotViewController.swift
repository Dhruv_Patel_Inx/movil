//
//  SpotViewController.swift
//  Movil Realty
//
//  Created by Apple on 17/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class SpotViewController: ARViewController,AnnotationViewDelegate {

    @IBOutlet var btnMenu: UIButton!
    @IBOutlet var infoView:UIView!
    var infoBackView:UIView!
    var currentAnno:AnnotationView!
    
    @IBOutlet var infoImage: UIImageView!
    @IBOutlet var infolblPrise: UILabel!
    @IBOutlet var infolblStatus: UILabel!
    @IBOutlet var infolblAddress: UILabel!
    @IBOutlet var infolblCity: UILabel!
    @IBOutlet var infolblBR: UILabel!
    @IBOutlet var infolblBA: UILabel!
    @IBOutlet var infolblYB: UILabel!
    @IBOutlet var infolblSF: UILabel!
    @IBOutlet var infoBtnDetail: UIButton!

    var id = String()
    
       override func viewDidLoad() {
        
        super.viewDidLoad()
        infoImage.layer.cornerRadius = 2.0
        infoImage.layer.borderWidth = 1.0
        infoImage.clipsToBounds = true
        infoImage.layer.borderColor = UIColor.white.cgColor
        infolblSF.adjustsFontSizeToFitWidth = true
        var places = [Place]()
        
        for index in 0..<appdel.arrayProparty.count
        {
            
            print(appdel.arrayProparty .object(at: index))
            
            let lat = (appdel.arrayProparty.object(at: index) as! NSDictionary).object(forKey: "LMD_MP_Latitude")!
            
            let long = (appdel.arrayProparty.object(at: index) as! NSDictionary).object(forKey: "LMD_MP_Longitude")!
            
            let location = CLLocation(latitude: CLLocationDegrees((lat as! NSString).floatValue), longitude: CLLocationDegrees((long as! NSString).floatValue))
            let currentDict = appdel.arrayProparty.object(at: index) as! NSDictionary
            
            let place = Place(location: location, reference: currentDict.object(forKey: "L_Address") as! String, name: "$\(currentDict.object(forKey: "L_AskingPrice")!)" , address: currentDict.object(forKey: "L_NumAcres") as! String)
            place.tag = index
            
            places.append(place)
        }

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        self.uiOptions.closeButtonEnabled = false
        self.maxDistance = 0
        self.maxVisibleAnnotations = 30
        self.maxVerticalLevel = 5
        self.headingSmoothingFactor = 0.05
        self.dataSource = self
        self.trackingManager.userDistanceFilter = 25
        self.trackingManager.reloadDistanceFilter = 75
        self.setAnnotations(places)
        self.uiOptions.debugEnabled = false
        
        allocInfoBackView()
    }
    override func viewDidAppear(_ animated: Bool)
    {
        if !UIImagePickerController.isSourceTypeAvailable(.camera)
        {
            let alert = UIAlertController(title: "Warning!", message: "No camera available", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        self.view.bringSubview(toFront:  btnMenu.superview!)
    }
    @IBAction func btnDetailClick(_ sender: Any)
    {
        
       let residentVC = self.storyboard?.instantiateViewController(withIdentifier: "ResidentActiveVC") as! ResidentActiveVC
        
        residentVC.propertyId = id
        
        self.navigationController?.pushViewController(residentVC, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- AnnotationView Delegate Method
    
    func didTouch(annotationView: AnnotationView)
    {
        annotationView.backgroundColor = UIColor.init(colorLiteralRed: 255/255, green: 131/255, blue: 81/255, alpha: 0.8)
        addInfoView()
        currentAnno = annotationView
        currentAnno.backgroundColor = UIColor.init(red: 255/255, green: 102/255, blue: 30/255, alpha: 0.5)

        if let annotation = currentAnno.annotation as? Place {
            let currentDict = appdel.arrayProparty.object(at: annotation.tag!) as! NSDictionary
//            let price = (appdel.arrayProparty.object(at: annotation.tag!) as! NSDictionary).object(forKey: "")
            let status = currentDict.object(forKey: "L_Status")!
            
            
            let placeHolderImage = "propertyList"
            let placeimage = UIImage(named: placeHolderImage)
            if isNSNull(value: currentDict.object(forKey: "path") as AnyObject?){
                let imagUrlString = currentDict.object(forKey: "path") as! String
                let url = URL(string: imagUrlString)
                
                infoImage.sd_setImage(with: url, placeholderImage: placeimage)
            }
            else{
                infoImage.image = placeimage
            }
            
            if (status as AnyObject) .isEqual("1") {
                
                infolblStatus.text = "Active"
            }
            else
            {
                infolblStatus.text = "Pending"
            }

            infolblPrise.text = "$\(currentDict.object(forKey: "L_AskingPrice")!)"
            infolblCity.text = "\(currentDict.object(forKey: "L_City")!)"
            infolblAddress.text = "\(currentDict.object(forKey: "L_Address")!)"
            //infolblBA.text = "BA : \(currentDict.object(forKey: "LM_Int1_14")!)"
            //infolblBR.text = "BR : \(currentDict.object(forKey: "LM_Int1_1")!)"
            infolblSF.text = "SF : \(currentDict.object(forKey: "LM_Dec_1")!)"
            infolblYB.text = "YB : Empty"
            id = "\(currentDict.object(forKey: "L_ListingID")!)"
            
            
            
            
            
//            propcell.houseimageview.sd_setImage(with: url, placeholderImage: placeimage)

            print(currentDict)
        }

    }
    
    func allocInfoBackView() {
        infoBackView = UIView(frame: CGRect(x: 0, y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height-64))
        infoView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 124)
        infoBackView.addSubview(infoView)
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(gesture:)))
        infoBackView.addGestureRecognizer(tapgesture)
    }
    
    func handleTap(gesture:UITapGestureRecognizer)  {
            removeInfoView()
            currentAnno.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    func addInfoView()  {
        self.view.addSubview(infoBackView)
        infoBackView.frame = CGRect(x: 0, y: 64, width: self.view.frame.size.width, height: infoBackView.frame.size.height)
                UIView.animate(withDuration: 0.30, animations: {
            self.infoBackView.alpha = 1.0
        })
    }
    
    func removeInfoView()  {
        UIView.animate(withDuration: 0.30, animations: {
            self.infoBackView.alpha = 1.0
        }) { (_:Bool) in
            self.infoBackView.removeFromSuperview()
        }
    }

    @IBAction func clickOnBackbtn(_ sender: AnyObject)
    {
        SlideNavigationController.sharedInstance().leftMenuSelected(sender)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
extension SpotViewController: ARDataSource {
    func ar(_ arViewController: ARViewController, viewForAnnotation: ARAnnotation) -> ARAnnotationView {
        let annotationView = AnnotationView()
        annotationView.annotation = viewForAnnotation
        annotationView.delegate = self
        annotationView.frame = CGRect(x: 0, y: 0, width: 140, height: 62)
        
        return annotationView
    }
}

extension UIViewController{
    func isNSNull(value : AnyObject?) -> Bool {
        if value is NSNull {
            return false
        } else {
            return true
        }
    }
}
