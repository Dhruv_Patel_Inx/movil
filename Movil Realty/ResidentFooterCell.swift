//
//  ResidentFooterCell.swift
//  Movil Realty
//
//  Created by BAPS on 4/5/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit

class ResidentFooterCell: UITableViewCell {

    @IBOutlet weak var btnPropertyDetail: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        btnPropertyDetail.layer.cornerRadius = 1.0
        btnPropertyDetail.layer.borderWidth = 5.0
        btnPropertyDetail.layer.borderColor = OrangeThemeColor.cgColor
        
        
        btnPropertyDetail.setTitle("See Full Property Detail", for: UIControlState.normal)
        // Configure the view for the selected state
    }

}
