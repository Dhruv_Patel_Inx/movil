//
//  AgentDetailsViewController.swift
//  Movil Realty
//
//  Created by Apple on 27/02/17.
//  Copyright © 2017 Sagar Trivedi. All rights reserved.
//

import UIKit
import GoogleMaps
import MessageUI

class AgentDetailsViewController: UIViewController, GMSMapViewDelegate, MFMailComposeViewControllerDelegate{

    
    @IBOutlet weak var lblAgents: UILabel!
    @IBOutlet var gmsmap: GMSMapView!
    @IBOutlet var agentnamelabel: UILabel!
    @IBOutlet var agentimageview: UIImageView!
    @IBOutlet var callbutton: UIButton!
    @IBOutlet var emailbutton: UIButton!
    @IBOutlet var chatbutton: UIButton!
    @IBOutlet var lineimage: UIImageView!
    @IBOutlet var weblabel: UILabel!
    @IBOutlet var companylabel: UILabel!
    @IBOutlet var addresslabel: UILabel!
    @IBOutlet var webnamelabel: UILabel!
    @IBOutlet var companynamelabel: UILabel!
    @IBOutlet var addressnamelabel: UILabel!
    
    var agentsDict: NSMutableDictionary!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        /*--------- GMSMAPVIEW IMPLEMENTATION ---------*/

        let agent_lat = (agentsDict.object(forKey: "lat") as! NSString).doubleValue
        let agent_long = (agentsDict.object(forKey: "long") as! NSString).doubleValue
        let agent_position = CLLocationCoordinate2DMake(agent_lat, agent_long)
        let agent_maker = GMSMarker(position: agent_position)
        agent_maker.map = gmsmap
        
        
        print("lat: \(agent_lat), long : \(agent_long)")
        let agent_camera
            = GMSCameraPosition.camera(withLatitude: agent_lat,                                                                longitude: agent_long , zoom: 6)
        gmsmap.camera = agent_camera
      
        
        /*--------- GMSMAPVIEW IMPLEMENTATION ---------*/

        
        agentimageview.layer.cornerRadius = 10.0
        agentimageview.clipsToBounds = true
        let agent_name = (agentsDict.object(forKey: "name"))
        let agent_web = (agentsDict.object(forKey: "website"))
        let agent_company = (agentsDict.object(forKey: "company_name"))
        let agent_address = (agentsDict.object(forKey: "address"))
        
        agentnamelabel.text = "\(agent_name!)"
        webnamelabel.text = "\(agent_web!)"
        companynamelabel.text = "\(agent_company!)"
        addressnamelabel.text = "\(agent_address!)"
        
        
        
        let imagUrlString = (agentsDict.object(forKey: "agent_pic"))
        
//        let url = URL(string: imagUrlString as! String)
//        agentimageview.sd_setImage(with: url)
        
        let url = URL(string: imagUrlString as! String)
        let placeHolderImage = "Profile"
        let placeimage = UIImage(named: placeHolderImage)
        agentimageview.sd_setImage(with: url, placeholderImage: placeimage)

        
        /*--------- SetUpValue Call ---------*/

        self.SetUpValue()
        
        
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
   
    
    @IBAction func sendemail(_ sender: Any) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    @IBAction func callphone(_ sender: Any) {
        let agent_phone = agentsDict.object(forKey: "contact_number")
        if let phoneCallURL = URL(string: "tel://\(agent_phone)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                }
                else {
                    // Fallback on earlier versions
                }
            }
        }
      }


    @IBAction func backtoagent(_ sender: Any) {
         self.navigationController!.popViewController(animated: true)
    }
    
        /*--------- FOR THE CONFIGURATION OF MAIL ---------*/
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        let recipient = agentsDict.object(forKey: "email")
        
        mailComposerVC.setToRecipients(["\(recipient)"])
        mailComposerVC.setSubject("Sending you an in-app e-mail...")
        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        
        return mailComposerVC
    }
    /*--------- FOR THE CONFIGURATION OF MAIL ---------*/
    
    
    /*--------- FOR THE ERROR ALERT ---------*/
    func showSendMailErrorAlert() {

        let mailerroralert = UIAlertController(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", preferredStyle: UIAlertControllerStyle.alert)
        mailerroralert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(mailerroralert, animated: true, completion: nil)
        
        
        //----- DEPRECATED -----//
        /*        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
                sendMailErrorAlert.show() */
        //----- DEPRECATED -----//
    }
    /*--------- FOR THE ERROR ALERT ---------*/
    
    
    
    /*--------- MFMailComposeViewControllerDelegate Method ---------*/
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    /*--------- MFMailComposeViewControllerDelegate Method ---------*/

    
    
    
    // MARK: - Navigation

    func SetUpValue()
    {
        weblabel.text = strWeb
        companylabel.text = strCompany
        addresslabel.text = strAddress
        lblAgents.text = strAgents
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
